package gofp

import "fmt"

type Option[T any] struct {
	value T
	none  bool
}

func Some[T any](value T) Option[T] {
	return Option[T]{value: value}
}

func None[T any]() Option[T] {
	return Option[T]{none: true}
}

func (o Option[T]) Get() (T, bool) {
	return o.value, o.IsDefined()
}

func (o Option[T]) Must() T {
	if o.none {
		panic("Optional value is None")
	}
	return o.value
}

func (o Option[T]) GetOrElse(other T) T {
	if o.none {
		return other
	}

	return o.value
}

func (o Option[T]) IsDefined() bool {
	return !o.none
}

func (o Option[T]) String() string {
	if o.none {
		return "None"
	}

	return fmt.Sprintf("Some(%v)", o.value)
}
