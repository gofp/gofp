ifneq (,$(wildcard ./.env))
	include .env
	export
endif

.DEFAULT_GOAL := dev-stack

DOCKER_REGISTRY ?= gitlab.com
REPO ?= gofp
PROJECT ?= gofp

SERVICE_DIR ?= /opt/service
SERVICE_PATH ?= ${SERVICE_DIR}/${PROJECT}

USER_ID ?= $(shell id -u)
USER_NAME ?= $(shell id -un)
MAINTAINER ?= "$(shell git config user.name) <$(shell git config user.email)>"

TAG ?= $(shell git describe --tags --always --dirty | sed 's/-g[a-z0-9]\{7\}//')
CONTAINER_NAME ?= ${DOCKER_REGISTRY}/${REPO}/${PROJECT}

ifeq ($(OS),Windows_NT)
	MSYS_NO_PATHCONV ?= 1
endif

dev-stack:
	docker-compose -p ${PROJECT} -f deployments/docker/docker-compose-dev.yaml up -d

dev-stack-down:
	docker-compose -p ${PROJECT} nexus -f deployments/docker/docker-compose-dev.yaml down

test-stack:
	docker-compose -p ${PROJECT} -f deployments/docker/docker-compose-test.yaml up -d --build

test-stack-down:
	docker-compose -p ${PROJECT} -f deployments/docker/docker-compose-test.yaml down

.PHONY: build
build:
	cp config/configuration.yaml build/docker/service/config
	docker build --build-arg SERVICE_PATH=${SERVICE_PATH} \
		--build-arg SERVICE_DIR=${SERVICE_DIR} \
		--build-arg MAINTAINER=${MAINTAINER} \
		-t "${CONTAINER_NAME}:latest" \
		-t "${CONTAINER_NAME}:${TAG}" \
		-f build/docker/service/Dockerfile .

push:
	docker push ${CONTAINER_NAME}:latest
	docker push ${CONTAINER_NAME}:${TAG}

clean:
	docker rmi -f ${CONTAINER_NAME}:latest ${CONTAINER_NAME}:${TAG}

bench-primes:
	go test -benchmem -run=^$$ -bench ^BenchmarkPrimes$$ gitlab.com/gofp/gofp/numbers -v -race -benchtime=10x
