package tuple_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gofp/gofp/tuple"
)

func TestTuple2(t *testing.T) {
	t2 := tuple.New2("a", 1)
	assert.Equal(t, "a", t2.E1())
	assert.Equal(t, 1, t2.E2())
	a, b := t2.Unbind()
	assert.Equal(t, "a", a)
	assert.Equal(t, 1, b)
}

func TestTuple3(t *testing.T) {
	t3 := tuple.New3(1, int8(2), 3.0)
	assert.Equal(t, 1, t3.E1())
	assert.Equal(t, int8(2), t3.E2())
	assert.Equal(t, 3.0, t3.E3())
	a, b, c := t3.Unbind()
	assert.Equal(t, 1, a)
	assert.Equal(t, int8(2), b)
	assert.Equal(t, 3.0, c)
}

func TestTuple4(t *testing.T) {
	t4 := tuple.New4(1, int8(2), 3.0, 'a')
	assert.Equal(t, 1, t4.E1())
	assert.Equal(t, int8(2), t4.E2())
	assert.Equal(t, 3.0, t4.E3())
	assert.Equal(t, 'a', t4.E4())
	a, b, c, d := t4.Unbind()
	assert.Equal(t, 1, a)
	assert.Equal(t, int8(2), b)
	assert.Equal(t, 3.0, c)
	assert.Equal(t, 'a', d)
}

func TestTuple5(t *testing.T) {
	t5 := tuple.New5(1, int8(2), 3.0, 'a', "bbb")
	assert.Equal(t, 1, t5.E1())
	assert.Equal(t, int8(2), t5.E2())
	assert.Equal(t, 3.0, t5.E3())
	assert.Equal(t, 'a', t5.E4())
	assert.Equal(t, "bbb", t5.E5())
	a, b, c, d, e := t5.Unbind()
	assert.Equal(t, 1, a)
	assert.Equal(t, int8(2), b)
	assert.Equal(t, 3.0, c)
	assert.Equal(t, 'a', d)
	assert.Equal(t, "bbb", e)
}

func TestTuple6(t *testing.T) {
	t6 := tuple.New6(1, int8(2), 3.0, 'a', "bbb", float32(6))
	assert.Equal(t, 1, t6.E1())
	assert.Equal(t, int8(2), t6.E2())
	assert.Equal(t, 3.0, t6.E3())
	assert.Equal(t, 'a', t6.E4())
	assert.Equal(t, "bbb", t6.E5())
	assert.Equal(t, float32(6), t6.E6())
	a, b, c, d, e, f := t6.Unbind()
	assert.Equal(t, 1, a)
	assert.Equal(t, int8(2), b)
	assert.Equal(t, 3.0, c)
	assert.Equal(t, 'a', d)
	assert.Equal(t, "bbb", e)
	assert.Equal(t, float32(6), f)
}

func TestTuple7(t *testing.T) {
	t7 := tuple.New7(1, int8(2), 3.0, 'a', "bbb", float32(6), uint16(7))
	assert.Equal(t, 1, t7.E1())
	assert.Equal(t, int8(2), t7.E2())
	assert.Equal(t, 3.0, t7.E3())
	assert.Equal(t, 'a', t7.E4())
	assert.Equal(t, "bbb", t7.E5())
	assert.Equal(t, float32(6), t7.E6())
	assert.Equal(t, uint16(7), t7.E7())
	a, b, c, d, e, f, g := t7.Unbind()
	assert.Equal(t, 1, a)
	assert.Equal(t, int8(2), b)
	assert.Equal(t, 3.0, c)
	assert.Equal(t, 'a', d)
	assert.Equal(t, "bbb", e)
	assert.Equal(t, float32(6), f)
	assert.Equal(t, uint16(7), g)
}

func TestTuple8(t *testing.T) {
	t8 := tuple.New8(1, int8(2), 3.0, 'a', "bbb", float32(6), uint16(7), []byte("ccc"))
	assert.Equal(t, 1, t8.E1())
	assert.Equal(t, int8(2), t8.E2())
	assert.Equal(t, 3.0, t8.E3())
	assert.Equal(t, 'a', t8.E4())
	assert.Equal(t, "bbb", t8.E5())
	assert.Equal(t, float32(6), t8.E6())
	assert.Equal(t, uint16(7), t8.E7())
	assert.Equal(t, []byte("ccc"), t8.E8())
	a, b, c, d, e, f, g, h := t8.Unbind()
	assert.Equal(t, 1, a)
	assert.Equal(t, int8(2), b)
	assert.Equal(t, 3.0, c)
	assert.Equal(t, 'a', d)
	assert.Equal(t, "bbb", e)
	assert.Equal(t, float32(6), f)
	assert.Equal(t, uint16(7), g)
	assert.Equal(t, []byte("ccc"), h)
}

func TestTuple9(t *testing.T) {
	t9 := tuple.New9(1, int8(2), 3.0, 'a', "bbb", float32(6), uint16(7), []byte("ccc"), time.Unix(100, 0))
	assert.Equal(t, 1, t9.E1())
	assert.Equal(t, int8(2), t9.E2())
	assert.Equal(t, 3.0, t9.E3())
	assert.Equal(t, 'a', t9.E4())
	assert.Equal(t, "bbb", t9.E5())
	assert.Equal(t, float32(6), t9.E6())
	assert.Equal(t, uint16(7), t9.E7())
	assert.Equal(t, []byte("ccc"), t9.E8())
	assert.Equal(t, time.Unix(100, 0), t9.E9())
	a, b, c, d, e, f, g, h, i := t9.Unbind()
	assert.Equal(t, 1, a)
	assert.Equal(t, int8(2), b)
	assert.Equal(t, 3.0, c)
	assert.Equal(t, 'a', d)
	assert.Equal(t, "bbb", e)
	assert.Equal(t, float32(6), f)
	assert.Equal(t, uint16(7), g)
	assert.Equal(t, []byte("ccc"), h)
	assert.Equal(t, time.Unix(100, 0), i)
}
