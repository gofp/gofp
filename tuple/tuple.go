package tuple

// Tuple2 is a 2-tuple of types A and B.
type Tuple2[A, B any] struct {
	_1 A
	_2 B
}

// Tuple3 is a 3-tuple of types A, B, and C.
type Tuple3[A, B, C any] struct {
	_1 A
	_2 B
	_3 C
}

// Tuple4 is a 4-tuple of types A, B, C, and D.
type Tuple4[A, B, C, D any] struct {
	_1 A
	_2 B
	_3 C
	_4 D
}

// Tuple5 is a 5-tuple of types A, B, C, D, and E.
type Tuple5[A, B, C, D, E any] struct {
	_1 A
	_2 B
	_3 C
	_4 D
	_5 E
}

// Tuple6 is a 6-tuple of types A, B, C, D, E, and F.
type Tuple6[A, B, C, D, E, F any] struct {
	_1 A
	_2 B
	_3 C
	_4 D
	_5 E
	_6 F
}

// Tuple7 is a 7-tuple of types A, B, C, D, E, F, and G.
type Tuple7[A, B, C, D, E, F, G any] struct {
	_1 A
	_2 B
	_3 C
	_4 D
	_5 E
	_6 F
	_7 G
}

// Tuple8 is a 8-tuple of types A, B, C, D, E, F, G, and H.
type Tuple8[A, B, C, D, E, F, G, H any] struct {
	_1 A
	_2 B
	_3 C
	_4 D
	_5 E
	_6 F
	_7 G
	_8 H
}

// Tuple9 is a 9-tuple of types A, B, C, D, E, F, G, H, and I.
type Tuple9[A, B, C, D, E, F, G, H, I any] struct {
	_1 A
	_2 B
	_3 C
	_4 D
	_5 E
	_6 F
	_7 G
	_8 H
	_9 I
}

// New2 creates a 2-tuple of the given values.
func New2[A, B any](a A, b B) Tuple2[A, B] {
	return Tuple2[A, B]{a, b}
}

// New3 creates a 3-tuple of the given values.
func New3[A, B, C any](a A, b B, c C) Tuple3[A, B, C] {
	return Tuple3[A, B, C]{a, b, c}
}

// New4 creates a 4-tuple of the given values.
func New4[A, B, C, D any](a A, b B, c C, d D) Tuple4[A, B, C, D] {
	return Tuple4[A, B, C, D]{a, b, c, d}
}

// New5 creates a 5-tuple of the given values.
func New5[A, B, C, D, E any](a A, b B, c C, d D, e E) Tuple5[A, B, C, D, E] {
	return Tuple5[A, B, C, D, E]{a, b, c, d, e}
}

// New6 creates a 6-tuple of the given values.
func New6[A, B, C, D, E, F any](a A, b B, c C, d D, e E, f F) Tuple6[A, B, C, D, E, F] {
	return Tuple6[A, B, C, D, E, F]{a, b, c, d, e, f}
}

// New7 creates a 7-tuple of the given values.
func New7[A, B, C, D, E, F, G any](a A, b B, c C, d D, e E, f F, g G) Tuple7[A, B, C, D, E, F, G] {
	return Tuple7[A, B, C, D, E, F, G]{a, b, c, d, e, f, g}
}

// New8 creates a 8-tuple of the given values.
func New8[A, B, C, D, E, F, G, H any](a A, b B, c C, d D, e E, f F, g G, h H) Tuple8[A, B, C, D, E, F, G, H] {
	return Tuple8[A, B, C, D, E, F, G, H]{a, b, c, d, e, f, g, h}
}

// New9 creates a 9-tuple of the given values.
func New9[A, B, C, D, E, F, G, H, I any](a A, b B, c C, d D, e E, f F, g G, h H, i I) Tuple9[A, B, C, D, E, F, G, H, I] {
	return Tuple9[A, B, C, D, E, F, G, H, I]{a, b, c, d, e, f, g, h, i}
}

// E1 returns the first element of the tuple.
func (t Tuple2[A, B]) E1() A {
	return t._1
}

// E2 returns the second element of the tuple.
func (t Tuple2[A, B]) E2() B {
	return t._2
}

// Unbind returns the values of the tuple
func (t Tuple2[A, B]) Unbind() (A, B) {
	return t._1, t._2
}

// E1 returns the first element of the tuple.
func (t Tuple3[A, B, C]) E1() A {
	return t._1
}

// E2 returns the second element of the tuple.
func (t Tuple3[A, B, C]) E2() B {
	return t._2
}

// E3 returns the third element of the tuple.
func (t Tuple3[A, B, C]) E3() C {
	return t._3
}

// Unbind returns the values of the tuple
func (t Tuple3[A, B, C]) Unbind() (A, B, C) {
	return t._1, t._2, t._3
}

// E1 returns the first element of the tuple.
func (t Tuple4[A, B, C, D]) E1() A {
	return t._1
}

// E2 returns the second element of the tuple.
func (t Tuple4[A, B, C, D]) E2() B {
	return t._2
}

// E3 returns the third element of the tuple.
func (t Tuple4[A, B, C, D]) E3() C {
	return t._3
}

// E4 returns the fourth element of the tuple.
func (t Tuple4[A, B, C, D]) E4() D {
	return t._4
}

// Unbind returns the values of the tuple
func (t Tuple4[A, B, C, D]) Unbind() (A, B, C, D) {
	return t._1, t._2, t._3, t._4
}

// E1 returns the first element of the tuple.
func (t Tuple5[A, B, C, D, E]) E1() A {
	return t._1
}

// E2 returns the second element of the tuple.
func (t Tuple5[A, B, C, D, E]) E2() B {
	return t._2
}

// E3 returns the third element of the tuple.
func (t Tuple5[A, B, C, D, E]) E3() C {
	return t._3
}

// E4 returns the fourth element of the tuple.
func (t Tuple5[A, B, C, D, E]) E4() D {
	return t._4
}

// E5 returns the fifth element of the tuple.
func (t Tuple5[A, B, C, D, E]) E5() E {
	return t._5
}

// Unbind returns the values of the tuple
func (t Tuple5[A, B, C, D, E]) Unbind() (A, B, C, D, E) {
	return t._1, t._2, t._3, t._4, t._5
}

// E1 returns the first element of the tuple.
func (t Tuple6[A, B, C, D, E, F]) E1() A {
	return t._1
}

// E2 returns the second element of the tuple.
func (t Tuple6[A, B, C, D, E, F]) E2() B {
	return t._2
}

// E3 returns the third element of the tuple.
func (t Tuple6[A, B, C, D, E, F]) E3() C {
	return t._3
}

// E4 returns the fourth element of the tuple.
func (t Tuple6[A, B, C, D, E, F]) E4() D {
	return t._4
}

// E5 returns the fifth element of the tuple.
func (t Tuple6[A, B, C, D, E, F]) E5() E {
	return t._5
}

// E6 returns the sixth element of the tuple.
func (t Tuple6[A, B, C, D, E, F]) E6() F {
	return t._6
}

// Unbind returns the values of the tuple
func (t Tuple6[A, B, C, D, E, F]) Unbind() (A, B, C, D, E, F) {
	return t._1, t._2, t._3, t._4, t._5, t._6
}

// E1 returns the first element of the tuple.
func (t Tuple7[A, B, C, D, E, F, G]) E1() A {
	return t._1
}

// E2 returns the second element of the tuple.
func (t Tuple7[A, B, C, D, E, F, G]) E2() B {
	return t._2
}

// E3 returns the third element of the tuple.
func (t Tuple7[A, B, C, D, E, F, G]) E3() C {
	return t._3
}

// E4 returns the fourth element of the tuple.
func (t Tuple7[A, B, C, D, E, F, G]) E4() D {
	return t._4
}

// E5 returns the fifth element of the tuple.
func (t Tuple7[A, B, C, D, E, F, G]) E5() E {
	return t._5
}

// E6 returns the sixth element of the tuple.
func (t Tuple7[A, B, C, D, E, F, G]) E6() F {
	return t._6
}

// E7 returns the seventh element of the tuple.
func (t Tuple7[A, B, C, D, E, F, G]) E7() G {
	return t._7
}

// Unbind returns the values of the tuple
func (t Tuple7[A, B, C, D, E, F, G]) Unbind() (A, B, C, D, E, F, G) {
	return t._1, t._2, t._3, t._4, t._5, t._6, t._7
}

// E1 returns the first element of the tuple.
func (t Tuple8[A, B, C, D, E, F, G, H]) E1() A {
	return t._1
}

// E2 returns the second element of the tuple.
func (t Tuple8[A, B, C, D, E, F, G, H]) E2() B {
	return t._2
}

// E3 returns the third element of the tuple.
func (t Tuple8[A, B, C, D, E, F, G, H]) E3() C {
	return t._3
}

// E4 returns the fourth element of the tuple.
func (t Tuple8[A, B, C, D, E, F, G, H]) E4() D {
	return t._4
}

// E5 returns the fifth element of the tuple.
func (t Tuple8[A, B, C, D, E, F, G, H]) E5() E {
	return t._5
}

// E6 returns the sixth element of the tuple.
func (t Tuple8[A, B, C, D, E, F, G, H]) E6() F {
	return t._6
}

// E7 returns the seventh element of the tuple.
func (t Tuple8[A, B, C, D, E, F, G, H]) E7() G {
	return t._7
}

// E8 returns the eighth element of the tuple.
func (t Tuple8[A, B, C, D, E, F, G, H]) E8() H {
	return t._8
}

// Unbind returns the values of the tuple
func (t Tuple8[A, B, C, D, E, F, G, H]) Unbind() (A, B, C, D, E, F, G, H) {
	return t._1, t._2, t._3, t._4, t._5, t._6, t._7, t._8
}

// E1 returns the first element of the tuple.
func (t Tuple9[A, B, C, D, E, F, G, H, I]) E1() A {
	return t._1
}

// E2 returns the second element of the tuple.
func (t Tuple9[A, B, C, D, E, F, G, H, I]) E2() B {
	return t._2
}

// E3 returns the third element of the tuple.
func (t Tuple9[A, B, C, D, E, F, G, H, I]) E3() C {
	return t._3
}

// E4 returns the fourth element of the tuple.
func (t Tuple9[A, B, C, D, E, F, G, H, I]) E4() D {
	return t._4
}

// E5 returns the fifth element of the tuple.
func (t Tuple9[A, B, C, D, E, F, G, H, I]) E5() E {
	return t._5
}

// E6 returns the sixth element of the tuple.
func (t Tuple9[A, B, C, D, E, F, G, H, I]) E6() F {
	return t._6
}

// E7 returns the seventh element of the tuple.
func (t Tuple9[A, B, C, D, E, F, G, H, I]) E7() G {
	return t._7
}

// E8 returns the eighth element of the tuple.
func (t Tuple9[A, B, C, D, E, F, G, H, I]) E8() H {
	return t._8
}

// E9 returns the ninth element of the tuple
func (t Tuple9[A, B, C, D, E, F, G, H, I]) E9() I {
	return t._9
}

// Unbind returns the values of the tuple
func (t Tuple9[A, B, C, D, E, F, G, H, I]) Unbind() (A, B, C, D, E, F, G, H, I) {
	return t._1, t._2, t._3, t._4, t._5, t._6, t._7, t._8, t._9
}
