package gofp

import (
	"fmt"
	"time"
)

// Map is a generic function that takes a slice of any type and a mapping function
// and returns a slice of the mapped values.
func Map[T any, U any](input []T, fn func(T) U) []U {
	result := make([]U, 0, len(input))

	for _, v := range input {
		result = append(result, fn(v))
	}

	return result
}

// MapStream is a generic function that takes a read-only channel of any type, a write-only
// channel of any type and a mapping function and applies the mapping function to each value
// in the read-only channel and writes the result to the write-only channel
func MapStream[T any, U any](in <-chan T, out chan<- U, fn func(T) U) {
	for v := range in {
		out <- fn(v)
	}
	close(out)
}

// Filter is a generic function that take a slice of any type and a filter function
// and returns a slice of the filtered values
func Filter[T any](input []T, fn func(T) bool, max int) []T {
	if max <= 0 {
		max = len(input)
	}

	result := make([]T, 0, max)

	count := 0
	for _, v := range input {
		if fn(v) {
			result = append(result, v)
			count++
		}

		if max > 0 && count >= max {
			break
		}
	}

	return result
}

// FilterStream is a generic function that take a read-only channel of any type, a write-only
// channel of the same type and a predicate and filters the read-only channel of values that
// match the predicate and writes the matching values to the write-only channel
func FilterStream[T any](in <-chan T, out chan<- T, fn func(T) bool, max int) {
	count := 0
	defer close(out)
	for v := range in {
		if fn(v) {
			// we don't control the input stream, so if we exit early, we end up blocking the sender
			// instead we need to continue consuming the stream until the sender has finished, but we
			// can stop processing and discard the data
			if max > 0 && count >= max {
				continue
			}

			out <- v
			count++
		}
	}
}

// FilterMap is a generic function that takes a slice of any type and a filter and mapping function
// then converts the filtered values into a slice of the mapped output
func FilterMap[T any, U any](input []T, filter func(T) bool, mapFn func(T) U, max int) []U {
	return Map(Filter(input, filter, max), mapFn)
}

func FilterMapStream[T any, U any](in <-chan T, out chan<- U, filter func(T) bool, mapFn func(T) U, max int) {
	count := 0
	defer close(out)
	for v := range in {
		if max > 0 && count >= max {
			continue
		}

		if filter(v) {
			out <- mapFn(v)
			count++
		}
	}
}

// FilterNot is a generic function that takes a slice of any type and a filter function
// and returns a slice of the values that do not match the filter
func FilterNot[T any](input []T, fn func(T) bool, max int) []T {
	if max <= 0 {
		max = len(input)
	}
	result := make([]T, 0, max)

	count := 0

	for _, v := range input {
		if !fn(v) {
			result = append(result, v)
			count++
		}

		if max > 0 && count >= max {
			break
		}
	}

	return result
}

// FilterNotStream is a generic function that takes a read-only channel of any type, a write-only
// channel of the same type and a predicate and filters the read-only channel of values that
// do not match the filter and writes the matching values to the write-only channel
func FilterNotStream[T any](in <-chan T, out chan<- T, pred func(T) bool, max int) {
	defer close(out)
	count := 0
	for v := range in {
		if max > 0 && count >= max {
			continue
		}

		if !pred(v) {
			out <- v
			count++
		}
	}
}

// Reverse is a generic function that takes a slice of any type and returns a slice of the same type
// but in reverse order
func Reverse[T any](input []T) []T {
	result := make([]T, len(input))
	for i, j := 0, len(input)-1; i <= j; i, j = i+1, j-1 {
		result[i], result[j] = input[j], input[i]
	}

	return result
}

// FoldLeft is a generic function that takes an initial value, a slice of any type and a function
// that performs an operation on each value in the slice and initial value, while feeding the result
// back into each iteration
func FoldLeft[T any, U any](acc U, l []T, fn func(U, T) U) U {
	for _, v := range l {
		acc = fn(acc, v)
	}

	return acc
}

// FoldRight is a generic function that takes an initial value, a slice of any type and a function
// that performs an operation on each value in the slice and initial value, while feeding the result
// back into each iteration
func FoldRight[T any, U any](acc U, l []T, fn func(U, T) U) U {
	for i := len(l) - 1; i >= 0; i-- {
		acc = fn(acc, l[i])
	}

	return acc
}

// FoldStream is a generic function that takes an initial value, a read-only channel of any types,
// a write-only channel of any types and an accumulating function, then applies the function to each
// incoming value. When the stream ends, the accumulated total is written to the write-only channel.
func FoldStream[T any, U any](acc U, in <-chan T, out chan<- U, fn func(U, T) U) {
	for v := range in {
		acc = fn(acc, v)
	}
	out <- acc
	close(out)
}

// ToStrings takes a slice of any type and results a slice of strings
func ToStrings[T any](input []T) []string {
	switch t := any(input).(type) {
	case []string:
		return t
	default:
		output := make([]string, len(input))
		for i, v := range input {
			output[i] = fmt.Sprintf("%v", v)
		}
		return output
	}
}

// ToStream takes a slice of any type and returns an unbuffered channel of values
func ToStream[T any](input []T) chan T {
	output := make(chan T)

	go func() {
		for _, v := range input {
			output <- v
		}
		close(output)
	}()

	return output
}

// ToBufferedStream takes a slice of any type and returns a buffered channel of values
func ToBufferedStream[T any](input []T, size int) chan T {
	output := make(chan T, size)

	go func() {
		for _, v := range input {
			output <- v
		}
		close(output)
	}()

	return output
}

// SplitAt is a generic function that takes a slice and splits it into 2 separate slices
// at the given index
func SplitAt[T any](input []T, index int) ([]T, []T) {
	if len(input) < index {
		return input, []T{}
	}

	if index <= 0 {
		return []T{}, input
	}

	return input[:index], input[index:]
}

// Partition is a generic function that takes a slice and a predicate and returns
// a list of items that match and a list of items that do not match the predicate
func Partition[T any](input []T, pred func(T) bool) (matched []T, unmatched []T) {
	for _, v := range input {
		if pred(v) {
			matched = append(matched, v)
		} else {
			unmatched = append(unmatched, v)
		}
	}

	return
}

// Foreach is a generic function that takes a slice and a function and applies the function to each item
func ForEach[T any](input []T, fn func(T)) []T {
	for _, v := range input {
		fn(v)
	}
	return input
}

// Sum is a generic function that takes a slice and an add function to calculate the sum of the numeric values
func Sum[T any](input []T, add func(T, T) T) Option[T] {
	if len(input) == 0 {
		return None[T]()
	}

	acc := input[0]

	if len(input) == 1 {
		return Some(acc)
	}

	for i := 1; i < len(input); i++ {
		acc = add(acc, input[i])
	}

	return Some(acc)
}

// Product is a generic function that takes a slice and a multiply function to calculate the product of the numeric values
func Product[T any](input []T, multiply func(T, T) T) Option[T] {
	if len(input) == 0 {
		return None[T]()
	}

	acc := input[0]

	for i := 1; i <= len(input)-1; i++ {
		acc = multiply(acc, input[i])
	}

	return Some(acc)
}

// Min is a generic function that takes a slice and a compare function to calculate the minimum value
func Min[T any](input []T, compare func(T, T) bool) Option[T] {
	if len(input) == 0 {
		return None[T]()
	}

	min := input[0]

	for i := 1; i < len(input); i++ {
		if compare(input[i], min) {
			min = input[i]
		}
	}

	return Some(min)
}

// Max is a generic function that takes a slice and a compare function to calculate the maximum value
func Max[T any](input []T, compare func(T, T) bool) Option[T] {
	if len(input) == 0 {
		return None[T]()
	}

	max := input[0]

	for i := 1; i < len(input); i++ {
		if compare(input[i], max) {
			max = input[i]
		}
	}

	return Some(max)
}

// Forall is a generic function that takes a slice and a predicate and returns true if all items in the slice match the predicate
func ForAll[T any](input []T, pred func(T) bool) bool {
	for _, v := range input {
		if !pred(v) {
			return false
		}
	}

	return true
}

// Contains is a generic function that takes a slice and a value and returns true if the value is in the slice
func Contains[T any](input []T, value T, compare func(T, T) bool) bool {
	for _, v := range input {
		if compare(value, v) {
			return true
		}
	}

	return false
}

// IndexOf is a generic function that takes a slice and a value and returns the index of the value in the slice
func IndexOf[T any](input []T, value T, compare func(T, T) bool) int {
	for i, v := range input {
		if compare(value, v) {
			return i
		}
	}

	return -1
}

// LastIndexOf is a generic function that takes a slice and a value and returns the index of the last occurrence of the value in the slice
func LastIndexOf[T any](input []T, value T, compare func(T, T) bool) int {
	for i := len(input) - 1; i >= 0; i-- {
		if compare(value, input[i]) {
			return i
		}
	}

	return -1
}

// Count is a generic function that takes a slice and a predicate and returns the number of items in the slice that match the predicate
func Count[T any](input []T, pred func(T) bool) int {
	count := 0
	for _, v := range input {
		if pred(v) {
			count++
		}
	}

	return count
}

// Zip is a generic function that takes 2 slices of different types and creates a map with the first list as keys and the second list as values
func Zip[T comparable, U any](keys []T, values []U) map[T]U {
	output := map[T]U{}
	max := len(values)

	for i, key := range keys {
		if i >= max {
			return output
		}
		output[key] = values[i]
	}

	return output
}

// Mappable is a generic interface for collection types that can be mapped over
type Mappable[T, U, V any] interface {
	Map(fn func(T) U) V
}

// Retrievable is an interface for collection types that should be implemented
// to retrieve elements from the collection
type Retrievable[T, U any] interface {
	Head() Option[T]
	Tail() U
	Last() Option[T]
}

// Reversable is an interface for collection types that should be implemented to
// reverse the order of the collection
type Reversable[T, U any] interface {
	Reverse() U
}

// Filterable is a generic interface for collection types that can be filtered
type Filterable[T any, U any] interface {
	Filter(fn func(T) bool, max int) U
	FilterNot(fn func(T) bool, max int) U
}

// Reducable is a generic interface for collection types that can be reduced
// to a single value
type Reduceable[T any] interface {
	FoldLeft(initial T, fn func(T, T) T) T
	FoldRight(initial T, fn func(T, T) T) T
}

// Streamable is a generic interface for collection types that can converted
// into a stream of values
type Streamable[T any] interface {
	ToStream() chan T
	ToBufferedStream(size int) chan T
}

// Sizeable is a generic interface for collection types that provide size information
type Sizeable[T any] interface {
	IsEmpty() bool
	NonEmpty() bool
	Size() int
	Cap() int
}

// Divisible is a generic interface for collection types that can be divided into
// one or more sub collections
type Divisible[T, U any] interface {
	SplitAt(n int) (U, U)
	Partition(fn func(T) bool) (U, U)
	Slice(int, int) U
}

// Stringable is an interface for collection types that should be implemented to covert
// the collection into a collection of strings
type Stringable[T any] interface {
	ToStrings() []string
}

// Traversable is a generic interface for collection types that can be traversed
type Traversable[T, U, V any] interface {
	Retrievable[T, V]
	Reversable[T, V]
	Mappable[T, U, V]
	Filterable[T, V]
	Reduceable[T]
	Sizeable[T]
	Divisible[T, V]
	Stringable[T]
}

func EQ[T comparable](a, b T) bool {
	return a == b
}

type Orderable interface {
	int | int8 | int16 | int32 | int64 | uint | uint8 | uint16 | uint32 | uint64 | uintptr | float32 | float64 | string | time.Duration
}

func GT[T Orderable](a, b T) bool {
	return a > b
}

func LT[T Orderable](a, b T) bool {
	return a < b
}

type Addable interface {
	int | int8 | int16 | int32 | int64 | uint | uint8 | uint16 | uint32 | uint64 | uintptr | float32 | float64 | string | time.Duration
}

func Add[T Addable](a, b T) T {
	return a + b
}

type Multipliable interface {
	int | int8 | int16 | int32 | int64 | uint | uint8 | uint16 | uint32 | uint64 | uintptr | float32 | float64
}

func Multiply[T Multipliable](a, b T) T {
	return a * b
}

// Pop returns an optional head of a given slice and the remaining tail slice
func Pop[T any](l []T) (Option[T], []T) {
	if len(l) == 0 {
		return None[T](), l
	}

	return Some(l[0]), l[1:]
}

// PopN returns a slice of N elements from the head of the list along with the remaining tail slice
func PopN[T any](l []T, n int) ([]T, []T) {
	return SplitAt(l, n)
}

// Take returns the an optional value from the end of a given list along with the remaining slice.
func Take[T any](l []T) (Option[T], []T) {
	if len(l) == 0 {
		return None[T](), l
	}
	last := len(l) - 1

	return Some(l[last]), l[:last]
}

// TakeN returns a slice of N elements from the end of a given list along with the remaining slice
func TakeN[T any](l []T, n int) (taken []T, remaining []T) {
	splitAt := len(l) - n
	remaining, taken = SplitAt(l, splitAt)
	return
}
