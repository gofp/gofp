package gofp_test

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"

	fp "gitlab.com/gofp/gofp"
)

type User struct {
	Name string
	Age  int
}

func (u User) String() string {
	return fmt.Sprintf(`"%s": %d`, u.Name, u.Age)
}

type Result struct {
	Sum     int
	Count   int
	Average float64
}

func TestMap(t *testing.T) {
	t.Run("Map any", func(t *testing.T) {
		type args struct {
			l  []any
			fn func(any) any
		}

		testCases := []struct {
			name string
			args args
			want []any
		}{
			{
				name: "Map any ints",
				args: args{
					l: []any{1, 2, 3},
					fn: func(i any) any {
						return i.(int) * 2
					},
				},
				want: []any{2, 4, 6},
			},
			{
				name: "Map any strings",
				args: args{
					l: []any{"a", "b", "c"},
					fn: func(i any) any {
						return strings.ToUpper(i.(string))
					},
				},
				want: []any{"A", "B", "C"},
			},
			{
				name: "Map any floats",
				args: args{
					l: []any{1.1, 2.2, 3.3},
					fn: func(i any) any {
						return i.(float64) * 2
					},
				},
				want: []any{2.2, 4.4, 6.6},
			},
		}

		for _, tc := range testCases {
			t.Run(tc.name, func(tt *testing.T) {
				got := fp.Map(tc.args.l, tc.args.fn)
				assert.Equal(tt, tc.want, got)
			})
		}
	})

	t.Run("Map Ints", func(t *testing.T) {
		got := fp.Map([]int{1, 2, 3}, func(i int) int {
			return i * 2
		})
		assert.Equal(t, []int{2, 4, 6}, got)
	})

	t.Run("Map String", func(t *testing.T) {
		got := fp.Map([]string{"a", "b", "c"}, func(s string) string {
			return strings.ToUpper(s)
		})
		assert.Equal(t, []string{"A", "B", "C"}, got)
	})

	t.Run("Map Floats", func(t *testing.T) {
		got := fp.Map([]float64{1.1, 2.2, 3.3}, func(f float64) float64 {
			return f * 2
		})
		assert.Equal(t, []float64{2.2, 4.4, 6.6}, got)
	})

	t.Run("Map Struct", func(t *testing.T) {
		got := fp.Map([]User{
			{Name: "Joe Bloggs", Age: 88},
			{Name: "Jane Bloggs", Age: 99},
		}, func(u User) User {
			return User{
				Name: strings.ToUpper(u.Name),
				Age:  u.Age + 1,
			}
		})
		assert.Equal(t, []User{
			{Name: "JOE BLOGGS", Age: 89},
			{Name: "JANE BLOGGS", Age: 100},
		}, got)
	})
}

func TestFilter(t *testing.T) {
	t.Run("Filter any", func(t *testing.T) {
		type args struct {
			l   []any
			fn  func(any) bool
			max int
		}

		testCases := []struct {
			name string
			args args
			want []any
		}{
			{
				name: "Filter any ints",
				args: args{
					l: []any{1, 2, 3},
					fn: func(i any) bool {
						return i.(int)%2 == 0
					},
				},
				want: []any{2},
			},
			{
				name: "Filter any floats",
				args: args{
					l: []any{1.0, 2.0, 3.0},
					fn: func(i any) bool {
						return int64(i.(float64))%2 == 0
					},
				},
				want: []any{2.0},
			},
			{
				name: "Filter any strings",
				args: args{
					l: []any{"a", "b", "c"},
					fn: func(i any) bool {
						return strings.HasPrefix(i.(string), "a")
					},
				},
				want: []any{"a"},
			},
			{
				name: "Filter any ints with max",
				args: args{
					l: []any{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
					fn: func(i any) bool {
						return i.(int)%2 == 0
					},
					max: 3,
				},
				want: []any{2, 4, 6},
			},
		}

		for _, tc := range testCases {
			t.Run(tc.name, func(tt *testing.T) {
				got := fp.Filter(tc.args.l, tc.args.fn, tc.args.max)
				assert.ElementsMatch(tt, got, tc.want, got)
			})
		}
	})

	t.Run("Filter Ints", func(t *testing.T) {
		got := fp.Filter([]int{1, 2, 3}, func(i int) bool {
			return i%2 == 0
		}, 0)
		assert.Equal(t, []int{2}, got)
		got = fp.Filter([]int{1, 2, 3, 4, 5}, func(i int) bool {
			return i%2 == 0
		}, 1)
		assert.Equal(t, []int{2}, got)
	})

	t.Run("Filter Float64s", func(t *testing.T) {
		got := fp.Filter([]float64{1.0, 2.0, 3.0}, func(f float64) bool {
			return int64(f)%2 == 0
		}, 0)
		assert.Equal(t, []float64{2.0}, got)
		got = fp.Filter([]float64{1.0, 2.0, 3.0, 4.0, 5.0}, func(f float64) bool {
			return int64(f)%2 == 0
		}, 1)
		assert.Equal(t, []float64{2.0}, got)
	})

	t.Run("Filter Strings", func(t *testing.T) {
		got := fp.Filter([]string{"a", "b", "c"}, func(s string) bool {
			return strings.HasPrefix(s, "a")
		}, 0)
		assert.Equal(t, []string{"a"}, got)
		got = fp.Filter([]string{"a", "b", "c", "ab", "ac"}, func(s string) bool {
			return strings.HasPrefix(s, "a")
		}, 2)
		assert.Equal(t, []string{"a", "ab"}, got)
	})

	t.Run("Filter Struct", func(t *testing.T) {
		got := fp.Filter([]User{
			{Name: "Joe Bloggs", Age: 88},
			{Name: "Jane Bloggs", Age: 99},
		}, func(u User) bool {
			return u.Age < 90
		}, 0)
		assert.Equal(t, []User{
			{Name: "Joe Bloggs", Age: 88},
		}, got)
	})
}

func TestFilterNot(t *testing.T) {
	t.Run("FilterNot any", func(t *testing.T) {
		type args struct {
			l   []any
			fn  func(any) bool
			max int
		}

		testCases := []struct {
			name string
			args args
			want []any
		}{
			{
				name: "FilterNot any ints",
				args: args{
					l: []any{1, 2, 3},
					fn: func(i any) bool {
						return i.(int)%2 == 0
					},
				},
				want: []any{1, 3},
			},
			{
				name: "FilterNot any floats",
				args: args{
					l: []any{1.0, 2.0, 3.0},
					fn: func(i any) bool {
						return int64(i.(float64))%2 == 0
					},
				},
				want: []any{1.0, 3.0},
			},
			{
				name: "FilterNot any strings",
				args: args{
					l: []any{"a", "b", "c"},
					fn: func(i any) bool {
						return strings.HasPrefix(i.(string), "a")
					},
				},
				want: []any{"b", "c"},
			},
			{
				name: "FilterNot any ints with max",
				args: args{
					l: []any{1, 2, 3},
					fn: func(i any) bool {
						return i.(int)%2 == 0
					},
					max: 1,
				},
				want: []any{1},
			},
		}

		for _, tc := range testCases {
			t.Run(tc.name, func(tt *testing.T) {
				got := fp.FilterNot(tc.args.l, tc.args.fn, tc.args.max)
				assert.ElementsMatch(tt, got, tc.want, got)
			})
		}
	})

	t.Run("FilterNot Ints", func(t *testing.T) {
		got := fp.FilterNot([]int{1, 2, 3}, func(i int) bool {
			return i%2 == 0
		}, 0)
		assert.Equal(t, []int{1, 3}, got)
		got = fp.FilterNot([]int{1, 2, 3}, func(i int) bool {
			return i%2 == 0
		}, 1)
		assert.Equal(t, []int{1}, got)
	})

	t.Run("FilterNot Floats", func(t *testing.T) {
		got := fp.FilterNot([]float64{1.0, 2.0, 3.0}, func(f float64) bool {
			return int64(f)%2 == 0
		}, 0)
		assert.Equal(t, []float64{1.0, 3.0}, got)
		got = fp.FilterNot([]float64{1.0, 2.0, 3.0}, func(f float64) bool {
			return int64(f)%2 == 0
		}, 1)
		assert.Equal(t, []float64{1.0}, got)
	})

	t.Run("FilterNot Strings", func(t *testing.T) {
		got := fp.FilterNot([]string{"a", "b", "c"}, func(s string) bool {
			return strings.HasPrefix(s, "a")
		}, 0)
		assert.Equal(t, []string{"b", "c"}, got)
		got = fp.FilterNot([]string{"a", "b", "c"}, func(s string) bool {
			return strings.HasPrefix(s, "a")
		}, 1)
		assert.Equal(t, []string{"b"}, got)
	})

	t.Run("FilterNot Struct", func(t *testing.T) {
		got := fp.FilterNot([]User{
			{Name: "Joe Bloggs", Age: 88},
			{Name: "Jane Bloggs", Age: 99},
		}, func(u User) bool {
			return u.Age < 90
		}, 0)
		assert.Equal(t, []User{
			{Name: "Jane Bloggs", Age: 99},
		}, got)
	})
}

func TestFoldLeft(t *testing.T) {
	t.Run("FoldLeft any", func(t *testing.T) {
		type args struct {
			l  []any
			fn func(any, any) any
			z  any
		}

		testCases := []struct {
			name string
			args args
			want any
		}{
			{
				name: "FoldLeft any ints",
				args: args{
					l: []any{1, 2, 3},
					fn: func(a, b any) any {
						return a.(int) + b.(int)
					},
					z: 0,
				},
				want: 6,
			},
			{
				name: "FoldLeft any floats",
				args: args{
					l: []any{1.0, 2.0, 3.0},
					fn: func(a, b any) any {
						return a.(float64) + b.(float64)
					},
					z: 0.0,
				},
				want: 6.0,
			},
			{
				name: "FoldLeft any strings",
				args: args{
					l: []any{"a", "b", "c"},
					fn: func(a, b any) any {
						return a.(string) + b.(string)
					},
					z: "",
				},
				want: "abc",
			},
		}

		for _, tc := range testCases {
			t.Run(tc.name, func(tt *testing.T) {
				got := fp.FoldLeft(tc.args.z, tc.args.l, tc.args.fn)
				assert.Equal(tt, tc.want, got)
			})
		}
	})

	t.Run("FoldLeft Ints", func(t *testing.T) {
		got := fp.FoldLeft(0, []int{1, 2, 3}, func(a, b int) int {
			return a + b
		})
		assert.Equal(t, 6, got)
	})

	t.Run("FoldLeft Floats", func(t *testing.T) {
		got := fp.FoldLeft(0.0, []float64{1.0, 2.0, 3.0}, func(a, b float64) float64 {
			return a + b
		})
		assert.Equal(t, 6.0, got)
	})

	t.Run("FoldLeft Strings", func(t *testing.T) {
		got := fp.FoldLeft("", []string{"a", "b", "c"}, func(a, b string) string {
			return a + b
		})
		assert.Equal(t, "abc", got)
	})

	t.Run("FoldLeft Struct", func(t *testing.T) {
		got := fp.FoldLeft(Result{}, []User{
			{Name: "Joe Bloggs", Age: 88},
			{Name: "Jane Bloggs", Age: 99},
		}, func(a Result, b User) Result {
			return Result{
				Sum:     a.Sum + b.Age,
				Count:   a.Count + 1,
				Average: float64(a.Sum+b.Age) / float64(a.Count+1),
			}
		})
		assert.Equal(t, Result{Sum: 187, Count: 2, Average: 93.5}, got)
	})
}

func TestFoldRight(t *testing.T) {
	t.Run("FoldLeft any", func(t *testing.T) {
		type args struct {
			l  []any
			fn func(any, any) any
			z  any
		}

		testCases := []struct {
			name string
			args args
			want any
		}{
			{
				name: "FoldRight any ints",
				args: args{
					l: []any{1, 2, 3},
					fn: func(a, b any) any {
						return a.(int) + b.(int)
					},
					z: 0,
				},
				want: 6,
			},
			{
				name: "FoldRight any floats",
				args: args{
					l: []any{1.0, 2.0, 3.0},
					fn: func(a, b any) any {
						return a.(float64) + b.(float64)
					},
					z: 0.0,
				},
				want: 6.0,
			},
			{
				name: "FoldRight any strings",
				args: args{
					l: []any{"a", "b", "c"},
					fn: func(a, b any) any {
						return a.(string) + b.(string)
					},
					z: "",
				},
				want: "cba",
			},
		}

		for _, tc := range testCases {
			t.Run(tc.name, func(tt *testing.T) {
				got := fp.FoldRight(tc.args.z, tc.args.l, tc.args.fn)
				assert.Equal(tt, tc.want, got)
			})
		}
	})

	t.Run("FoldRight Ints", func(t *testing.T) {
		got := fp.FoldRight(0, []int{1, 2, 3}, func(a, b int) int {
			return a + b
		})
		assert.Equal(t, 6, got)
	})

	t.Run("FoldRight Floats", func(t *testing.T) {
		got := fp.FoldRight(0.0, []float64{1.0, 2.0, 3.0}, func(a, b float64) float64 {
			return a + b
		})
		assert.Equal(t, 6.0, got)
	})

	t.Run("FoldRight Strings", func(t *testing.T) {
		got := fp.FoldRight("", []string{"a", "b", "c"}, func(a, b string) string {
			return a + b
		})
		assert.Equal(t, "cba", got)
	})

	t.Run("FoldRight Struct", func(t *testing.T) {
		got := fp.FoldRight(Result{}, []User{
			{Name: "Joe Bloggs", Age: 88},
			{Name: "Jane Bloggs", Age: 99},
		}, func(a Result, b User) Result {
			return Result{
				Sum:     a.Sum + b.Age,
				Count:   a.Count + 1,
				Average: float64(a.Sum+b.Age) / float64(a.Count+1),
			}
		})
		assert.Equal(t, Result{Sum: 187, Count: 2, Average: 93.5}, got)
	})
}

func TestReverse(t *testing.T) {
	t.Run("Reverse any", func(t *testing.T) {
		type args struct {
			l []any
		}

		testCases := []struct {
			name string
			args args
			want []any
		}{
			{
				name: "Reverse any ints",
				args: args{
					l: []any{1, 2, 3},
				},
				want: []any{3, 2, 1},
			},
			{
				name: "Reverse any floats",
				args: args{
					l: []any{1.0, 2.0, 3.0},
				},
				want: []any{3.0, 2.0, 1.0},
			},
			{
				name: "Reverse any strings",
				args: args{
					l: []any{"a", "b", "c"},
				},
				want: []any{"c", "b", "a"},
			},
			{
				name: "Reverse even length slice",
				args: args{
					l: []any{1, 2, 3, 4},
				},
				want: []any{4, 3, 2, 1},
			},
		}

		for _, tc := range testCases {
			t.Run(tc.name, func(tt *testing.T) {
				got := fp.Reverse(tc.args.l)
				assert.Equal(tt, tc.want, got)
			})
		}
	})

	t.Run("Reverse Ints", func(t *testing.T) {
		got := fp.Reverse([]int{1, 2, 3})
		assert.Equal(t, []int{3, 2, 1}, got)
	})

	t.Run("Reverse Floats", func(t *testing.T) {
		got := fp.Reverse([]float64{1.0, 2.0, 3.0})
		assert.Equal(t, []float64{3.0, 2.0, 1.0}, got)
	})

	t.Run("Reverse Strings", func(t *testing.T) {
		got := fp.Reverse([]string{"a", "b", "c"})
		assert.Equal(t, []string{"c", "b", "a"}, got)
	})

	t.Run("Reverse Struct", func(t *testing.T) {
		got := fp.Reverse([]User{
			{Name: "Joe Bloggs", Age: 88},
			{Name: "Jane Bloggs", Age: 99},
		})
		assert.Equal(t, []User{
			{Name: "Jane Bloggs", Age: 99},
			{Name: "Joe Bloggs", Age: 88},
		}, got)
	})
}

func TestToString(t *testing.T) {
	t.Run("ToString any", func(t *testing.T) {
		type args struct {
			l []any
		}

		testCases := []struct {
			name string
			args args
			want []string
		}{
			{
				name: "ToString any ints",
				args: args{
					l: []any{1, 2, 3},
				},
				want: []string{"1", "2", "3"},
			},
			{
				name: "ToString any floats",
				args: args{
					l: []any{1.0, 2.0, 3.0},
				},
				want: []string{"1", "2", "3"},
			},
			{
				name: "ToString any strings",
				args: args{
					l: []any{"a", "b", "c"},
				},
				want: []string{"a", "b", "c"},
			},
		}

		for _, tc := range testCases {
			t.Run(tc.name, func(tt *testing.T) {
				got := fp.ToStrings(tc.args.l)
				assert.Equal(tt, tc.want, got)
			})
		}
	})

	t.Run("ToString Ints", func(t *testing.T) {
		got := fp.ToStrings([]int{1, 2, 3})
		assert.Equal(t, []string{"1", "2", "3"}, got)
	})

	t.Run("ToString Floats", func(t *testing.T) {
		got := fp.ToStrings([]float64{1.0, 2.0, 3.0})
		assert.Equal(t, []string{"1", "2", "3"}, got)
	})

	t.Run("ToString Strings", func(t *testing.T) {
		got := fp.ToStrings([]string{"a", "b", "c"})
		assert.Equal(t, []string{"a", "b", "c"}, got)
	})

	t.Run("ToString Struct", func(t *testing.T) {
		got := fp.ToStrings([]User{
			{Name: "Joe Bloggs", Age: 88},
			{Name: "Jane Bloggs", Age: 99},
		})
		assert.Equal(t, []string{`"Joe Bloggs": 88`, `"Jane Bloggs": 99`}, got)
	})
}

func TestToStream(t *testing.T) {
	t.Run("ToStream Ints", func(t *testing.T) {
		gotCh := fp.ToStream([]int{1, 2, 3})
		got := make([]int, 0)
		for v := range gotCh {
			got = append(got, v)
		}
		assert.Equal(t, []int{1, 2, 3}, got)
	})
	t.Run("ToStream Floats", func(t *testing.T) {
		gotCh := fp.ToStream([]float64{1, 2, 3})
		got := make([]float64, 0)
		for v := range gotCh {
			got = append(got, v)
		}
		assert.Equal(t, []float64{1, 2, 3}, got)
	})
	t.Run("ToStream String", func(t *testing.T) {
		gotCh := fp.ToStream([]string{"a", "b", "c"})
		got := make([]string, 0)
		for v := range gotCh {
			got = append(got, v)
		}
		assert.Equal(t, []string{"a", "b", "c"}, got)
	})
}

func TestBufferedSstream(t *testing.T) {
	t.Run("ToBufferedStream Ints, buffer < length", func(t *testing.T) {
		gotCh := fp.ToBufferedStream([]int{1, 2, 3}, 2)
		got := make([]int, 0)
		for v := range gotCh {
			got = append(got, v)
		}
		assert.Equal(t, []int{1, 2, 3}, got)
	})
	t.Run("ToBufferedStream Floats, buffer < length", func(t *testing.T) {
		gotCh := fp.ToBufferedStream([]float64{1, 2, 3}, 2)
		got := make([]float64, 0)
		for v := range gotCh {
			got = append(got, v)
		}
		assert.Equal(t, []float64{1, 2, 3}, got)
	})
	t.Run("ToBufferedStream String, buffer < length", func(t *testing.T) {
		gotCh := fp.ToBufferedStream([]string{"a", "b", "c"}, 2)
		got := make([]string, 0)
		for v := range gotCh {
			got = append(got, v)
		}
		assert.Equal(t, []string{"a", "b", "c"}, got)
	})

	t.Run("ToBufferedStream Ints, buffer > length", func(t *testing.T) {
		gotCh := fp.ToBufferedStream([]int{1, 2, 3}, 5)
		got := make([]int, 0)
		for v := range gotCh {
			got = append(got, v)
		}
		assert.Equal(t, []int{1, 2, 3}, got)
	})
	t.Run("ToBufferedStream Floats, buffer > length", func(t *testing.T) {
		gotCh := fp.ToBufferedStream([]float64{1, 2, 3}, 5)
		got := make([]float64, 0)
		for v := range gotCh {
			got = append(got, v)
		}
		assert.Equal(t, []float64{1, 2, 3}, got)
	})
	t.Run("ToBufferedStream String, buffer > length", func(t *testing.T) {
		gotCh := fp.ToBufferedStream([]string{"a", "b", "c"}, 5)
		got := make([]string, 0)
		for v := range gotCh {
			got = append(got, v)
		}
		assert.Equal(t, []string{"a", "b", "c"}, got)
	})
}

func TestSplitAt(t *testing.T) {
	t.Run("SplitAt Ints", func(t *testing.T) {
		got, got2 := fp.SplitAt([]int{1, 2, 3}, 1)
		assert.Equal(t, []int{1}, got)
		assert.Equal(t, []int{2, 3}, got2)
	})
	t.Run("SplitAt Floats", func(t *testing.T) {
		got, got2 := fp.SplitAt([]float64{1, 2, 3}, 1)
		assert.Equal(t, []float64{1}, got)
		assert.Equal(t, []float64{2, 3}, got2)
	})
	t.Run("SplitAt Strings", func(t *testing.T) {
		got, got2 := fp.SplitAt([]string{"a", "b", "c"}, 1)
		assert.Equal(t, []string{"a"}, got)
		assert.Equal(t, []string{"b", "c"}, got2)
	})
	t.Run("SplitAt index greater than length", func(t *testing.T) {
		got, got2 := fp.SplitAt([]int{1, 2, 3}, 4)
		assert.Equal(t, []int{1, 2, 3}, got)
		assert.Equal(t, []int{}, got2)
	})
	t.Run("SplitAt index less than 0", func(t *testing.T) {
		got, got2 := fp.SplitAt([]int{1, 2, 3}, -1)
		assert.Equal(t, []int{}, got)
		assert.Equal(t, []int{1, 2, 3}, got2)
	})
}

func TestPartition(t *testing.T) {
	t.Run("Partition Ints", func(t *testing.T) {
		odds, evens := fp.Partition([]int{1, 2, 3}, func(a int) bool {
			return a%2 != 0
		})
		assert.Equal(t, []int{1, 3}, odds)
		assert.Equal(t, []int{2}, evens)
	})
	t.Run("Partition Floats", func(t *testing.T) {
		odds, evens := fp.Partition([]float64{1, 2, 3}, func(a float64) bool {
			return int64(a)%2 != 0
		})
		assert.Equal(t, []float64{1, 3}, odds)
		assert.Equal(t, []float64{2}, evens)
	})
}

func TestForEach(t *testing.T) {
	t.Run("ForEach Ints", func(t *testing.T) {
		var got []int
		fp.ForEach([]int{1, 2, 3}, func(a int) {
			got = append(got, a)
		})
		assert.Equal(t, []int{1, 2, 3}, got)
	})
	t.Run("ForEach Floats", func(t *testing.T) {
		var got []float64
		fp.ForEach([]float64{1, 2, 3}, func(a float64) {
			got = append(got, a)
		})
		assert.Equal(t, []float64{1, 2, 3}, got)
	})
	t.Run("ForEach Strings", func(t *testing.T) {
		var got []string
		fp.ForEach([]string{"a", "b", "c"}, func(a string) {
			got = append(got, a)
		})
		assert.Equal(t, []string{"a", "b", "c"}, got)
	})
}

func TestSum(t *testing.T) {
	t.Run("Sum Ints", func(t *testing.T) {
		got := fp.Sum([]int{1, 2, 3}, fp.Add[int])
		assert.Equal(t, fp.Some(6), got)
	})
	t.Run("Sum Floats", func(t *testing.T) {
		got := fp.Sum([]float64{1, 2, 3}, fp.Add[float64])
		assert.Equal(t, fp.Some(6.0), got)
	})
	t.Run("Sum decimals", func(t *testing.T) {
		got := fp.Sum([]decimal.Decimal{decimal.NewFromInt(1), decimal.NewFromInt(2), decimal.NewFromInt(3)}, func(a, b decimal.Decimal) decimal.Decimal {
			return a.Add(b)
		})
		assert.Equal(t, fp.Some(decimal.NewFromInt(6)), got)
	})
	t.Run("Sum durations", func(t *testing.T) {
		got := fp.Sum([]time.Duration{time.Second, time.Second * 2, time.Second * 3}, fp.Add[time.Duration])
		assert.Equal(t, fp.Some(time.Second*6), got)
	})
	t.Run("Sum empty ints", func(t *testing.T) {
		got := fp.Sum([]int{}, fp.Add[int])
		assert.Equal(t, fp.None[int](), got)
	})
	t.Run("Sum 1 int", func(t *testing.T) {
		got := fp.Sum([]int{1}, fp.Add[int])
		assert.Equal(t, fp.Some(1), got)
	})
}

func TestProduct(t *testing.T) {
	t.Run("Product Ints", func(t *testing.T) {
		got := fp.Product([]int{1, 2, 3}, fp.Multiply[int])
		assert.Equal(t, fp.Some(6), got)
	})
	t.Run("Product Floats", func(t *testing.T) {
		got := fp.Product([]float64{1, 2, 3}, fp.Multiply[float64])
		assert.Equal(t, fp.Some(6.0), got)
	})
	t.Run("Product decimals", func(t *testing.T) {
		got := fp.Product([]decimal.Decimal{decimal.NewFromInt(1), decimal.NewFromInt(2), decimal.NewFromInt(3)}, func(a, b decimal.Decimal) decimal.Decimal {
			return a.Mul(b)
		})
		assert.Equal(t, fp.Some(decimal.NewFromInt(6)), got)
	})
	t.Run("Product empty ints", func(t *testing.T) {
		got := fp.Product([]int{}, fp.Multiply[int])
		assert.Equal(t, fp.None[int](), got)
	})
	t.Run("Product 1 int", func(t *testing.T) {
		got := fp.Product([]int{1}, fp.Multiply[int])
		assert.Equal(t, fp.Some(1), got)
	})
}

func TestMin(t *testing.T) {
	t.Run("Min Ints", func(t *testing.T) {
		got := fp.Min([]int{2, 3, 1}, fp.LT[int])
		assert.Equal(t, fp.Some(1), got)
	})
	t.Run("Min Floats", func(t *testing.T) {
		got := fp.Min([]float64{1, 2, 3}, fp.LT[float64])
		assert.Equal(t, fp.Some(1.0), got)
	})
	t.Run("Min decimals", func(t *testing.T) {
		got := fp.Min([]decimal.Decimal{decimal.NewFromInt(1), decimal.NewFromInt(2), decimal.NewFromInt(3)}, func(a, b decimal.Decimal) bool {
			return a.LessThan(b)
		})
		assert.Equal(t, fp.Some(decimal.NewFromInt(1)), got)
	})
	t.Run("Min Date", func(t *testing.T) {
		got := fp.Min([]time.Time{time.Unix(1, 0), time.Unix(2, 0), time.Unix(3, 0)}, func(a, b time.Time) bool {
			return a.Before(b)
		})
		assert.Equal(t, fp.Some(time.Unix(1, 0)), got)
	})
	t.Run("Min empty ints", func(t *testing.T) {
		got := fp.Min([]int{}, fp.LT[int])
		assert.Equal(t, fp.None[int](), got)
	})
	t.Run("Min 1 int", func(t *testing.T) {
		got := fp.Min([]int{1}, fp.LT[int])
		assert.Equal(t, fp.Some(1), got)
	})
}

func TestMax(t *testing.T) {
	t.Run("Max Ints", func(t *testing.T) {
		got := fp.Max([]int{1, 2, 3}, fp.GT[int])
		assert.Equal(t, fp.Some(3), got)
	})
	t.Run("Max Floats", func(t *testing.T) {
		got := fp.Max([]float64{1, 2, 3}, fp.GT[float64])
		assert.Equal(t, fp.Some(3.0), got)
	})
	t.Run("Max decimals", func(t *testing.T) {
		got := fp.Max([]decimal.Decimal{decimal.NewFromInt(1), decimal.NewFromInt(2), decimal.NewFromInt(3)}, func(a, b decimal.Decimal) bool {
			return a.GreaterThan(b)
		})
		assert.Equal(t, fp.Some(decimal.NewFromInt(3)), got)
	})
	t.Run("Max Date", func(t *testing.T) {
		got := fp.Max([]time.Time{time.Unix(1, 0), time.Unix(2, 0), time.Unix(3, 0)}, func(a, b time.Time) bool {
			return a.After(b)
		})
		assert.Equal(t, fp.Some(time.Unix(3, 0)), got)
	})
	t.Run("Max empty ints", func(t *testing.T) {
		got := fp.Max([]int{}, fp.GT[int])
		assert.Equal(t, fp.None[int](), got)
	})
	t.Run("Max 1 int", func(t *testing.T) {
		got := fp.Max([]int{1}, fp.GT[int])
		assert.Equal(t, fp.Some(1), got)
	})
}

func TestForAll(t *testing.T) {
	t.Run("ForAll Ints", func(t *testing.T) {
		got := fp.ForAll([]int{1, 2, 3}, func(a int) bool {
			return a > 0
		})
		assert.Equal(t, true, got)
	})
	t.Run("ForAll Floats", func(t *testing.T) {
		got := fp.ForAll([]float64{1, 2, 3}, func(a float64) bool {
			return a > 0
		})
		assert.Equal(t, true, got)
	})
	t.Run("ForAll decimals", func(t *testing.T) {
		got := fp.ForAll([]decimal.Decimal{decimal.NewFromInt(1), decimal.NewFromInt(2), decimal.NewFromInt(3)}, func(a decimal.Decimal) bool {
			return a.GreaterThan(decimal.NewFromInt(0))
		})
		assert.Equal(t, true, got)
	})
	t.Run("ForAll Date", func(t *testing.T) {
		got := fp.ForAll([]time.Time{time.Unix(1, 0), time.Unix(2, 0), time.Unix(3, 0)}, func(a time.Time) bool {
			return a.After(time.Unix(0, 0))
		})
		assert.Equal(t, true, got)
	})
	t.Run("ForAll returns false", func(t *testing.T) {
		got := fp.ForAll([]int{1, 2, 3}, func(a int) bool {
			return a%2 == 1
		})
		assert.Equal(t, false, got)
	})
}

func TestContains(t *testing.T) {
	t.Run("Contains Ints", func(t *testing.T) {
		got := fp.Contains([]int{1, 2, 3}, 2, fp.EQ[int])
		assert.Equal(t, true, got)
	})
	t.Run("Contains Floats", func(t *testing.T) {
		got := fp.Contains([]float64{1, 2, 3}, 2.0, fp.EQ[float64])
		assert.Equal(t, true, got)
	})
	t.Run("Contains decimals", func(t *testing.T) {
		got := fp.Contains([]decimal.Decimal{decimal.NewFromInt(1), decimal.NewFromInt(2), decimal.NewFromInt(3)}, decimal.NewFromInt(2),
			func(a, b decimal.Decimal) bool {
				return a.Equal(b)
			})
		assert.Equal(t, true, got)
	})
	t.Run("Contains Date", func(t *testing.T) {
		got := fp.Contains([]time.Time{time.Unix(1, 0), time.Unix(2, 0), time.Unix(3, 0)}, time.Unix(2, 0), func(a, b time.Time) bool {
			return a.Equal(b)
		})
		assert.Equal(t, true, got)
	})
	t.Run("Contains returns false", func(t *testing.T) {
		got := fp.Contains([]int{1, 2, 3}, 4, fp.EQ[int])
		assert.Equal(t, false, got)
	})
}

func TestIndexOf(t *testing.T) {
	t.Run("IndexOf Ints", func(t *testing.T) {
		got := fp.IndexOf([]int{1, 2, 3}, 2, fp.EQ[int])
		assert.Equal(t, 1, got)
	})
	t.Run("IndexOf Floats", func(t *testing.T) {
		got := fp.IndexOf([]float64{1, 2, 3}, 2.0, fp.EQ[float64])
		assert.Equal(t, 1, got)
	})
	t.Run("IndexOf decimals", func(t *testing.T) {
		got := fp.IndexOf([]decimal.Decimal{decimal.NewFromInt(1), decimal.NewFromInt(2), decimal.NewFromInt(3)}, decimal.NewFromInt(2),
			func(a, b decimal.Decimal) bool {
				return a.Equal(b)
			})
		assert.Equal(t, 1, got)
	})
	t.Run("IndexOf Date", func(t *testing.T) {
		got := fp.IndexOf([]time.Time{time.Unix(1, 0), time.Unix(2, 0), time.Unix(3, 0)}, time.Unix(2, 0), func(a, b time.Time) bool {
			return a.Equal(b)
		})
		assert.Equal(t, 1, got)
	})
	t.Run("IndexOf returns none", func(t *testing.T) {
		got := fp.IndexOf([]int{1, 2, 3}, 4, fp.EQ[int])
		assert.Equal(t, -1, got)
	})
}

func TestLastIndexOf(t *testing.T) {
	t.Run("LastIndexOf Ints", func(t *testing.T) {
		got := fp.LastIndexOf([]int{1, 2, 3, 2}, 2, fp.EQ[int])
		assert.Equal(t, 3, got)
	})
	t.Run("LastIndexOf Floats", func(t *testing.T) {
		got := fp.LastIndexOf([]float64{1, 2, 3, 2}, 2.0, fp.EQ[float64])
		assert.Equal(t, 3, got)
	})
	t.Run("LastIndexOf decimals", func(t *testing.T) {
		got := fp.LastIndexOf([]decimal.Decimal{decimal.NewFromInt(1), decimal.NewFromInt(2), decimal.NewFromInt(3), decimal.NewFromInt(2)},
			decimal.NewFromInt(2),
			func(a, b decimal.Decimal) bool {
				return a.Equal(b)
			})
		assert.Equal(t, 3, got)
	})
	t.Run("LastIndexOf Date", func(t *testing.T) {
		got := fp.LastIndexOf([]time.Time{time.Unix(1, 0), time.Unix(2, 0), time.Unix(3, 0), time.Unix(2, 0)},
			time.Unix(2, 0),
			func(a, b time.Time) bool {
				return a.Equal(b)
			})
		assert.Equal(t, 3, got)
	})
	t.Run("LastIndexOf returns none", func(t *testing.T) {
		got := fp.LastIndexOf([]int{1, 2, 3}, 4, fp.EQ[int])
		assert.Equal(t, -1, got)
	})
}

func TestCount(t *testing.T) {
	t.Run("Count Ints", func(t *testing.T) {
		got := fp.Count([]int{1, 2, 3, 2}, func(a int) bool {
			return a == 2
		})
		assert.Equal(t, 2, got)
	})
	t.Run("Count Floats", func(t *testing.T) {
		got := fp.Count([]float64{1, 2, 3, 2}, func(a float64) bool {
			return a == 2.0
		})
		assert.Equal(t, 2, got)
	})
	t.Run("Count decimals", func(t *testing.T) {
		got := fp.Count([]decimal.Decimal{decimal.NewFromInt(1), decimal.NewFromInt(2), decimal.NewFromInt(3), decimal.NewFromInt(2)},
			func(a decimal.Decimal) bool {
				return a.Equal(decimal.NewFromInt(2))
			})
		assert.Equal(t, 2, got)
	})
	t.Run("Count Date", func(t *testing.T) {
		got := fp.Count([]time.Time{time.Unix(1, 0), time.Unix(2, 0), time.Unix(3, 0), time.Unix(2, 0)}, func(a time.Time) bool {
			return a.Equal(time.Unix(2, 0))
		})
		assert.Equal(t, 2, got)
	})
}

func TestMapStream(t *testing.T) {
	in := make(chan int)
	out := make(chan string)

	go fp.MapStream(in, out, func(i int) string {
		return strconv.Itoa(i)
	})

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			in <- i
		}
		close(in)
	}()

	got := make([]string, 0)
	for v := range out {
		got = append(got, v)
	}

	wg.Wait()
	assert.Equal(t, 10, len(got))
	assert.Equal(t, []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}, got)
}

func TestFilterStream(t *testing.T) {
	in := make(chan int)
	out := make(chan int)

	go fp.FilterStream(in, out, func(i int) bool {
		return i%2 == 0
	}, 0)

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			in <- i
		}
		close(in)
	}()

	got := make([]int, 0)
	for v := range out {
		got = append(got, v)
	}

	wg.Wait()
	assert.Equal(t, 5, len(got))
	assert.Equal(t, []int{0, 2, 4, 6, 8}, got)
}

func TestFilterStreamWithMax(t *testing.T) {
	in := make(chan int)
	out := make(chan int)

	go fp.FilterStream(in, out, func(i int) bool {
		return i%2 == 0
	}, 3)

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			in <- i
		}
		close(in)
	}()

	got := make([]int, 0)
	for v := range out {
		got = append(got, v)
	}

	wg.Wait()
	assert.Equal(t, 3, len(got))
	assert.Equal(t, []int{0, 2, 4}, got)
}

func TestFilterNotStream(t *testing.T) {
	in := make(chan int)
	out := make(chan int)

	go fp.FilterNotStream(in, out, func(i int) bool {
		return i%2 == 0
	}, 0)

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			in <- i
		}
		close(in)
	}()

	got := make([]int, 0)
	for v := range out {
		got = append(got, v)
	}

	wg.Wait()
	assert.Equal(t, 5, len(got))
	assert.Equal(t, []int{1, 3, 5, 7, 9}, got)
}

func TestFilterNotStreamWithMax(t *testing.T) {
	in := make(chan int)
	out := make(chan int)

	go fp.FilterNotStream(in, out, func(i int) bool {
		return i%2 == 0
	}, 3)

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			in <- i
		}
		close(in)
	}()

	got := make([]int, 0)
	for v := range out {
		got = append(got, v)
	}

	wg.Wait()
	assert.Equal(t, 3, len(got))
	assert.Equal(t, []int{1, 3, 5}, got)
}

func TestFoldStream(t *testing.T) {
	in := make(chan int)
	out := make(chan int)

	go fp.FoldStream(0, in, out, func(acc, i int) int {
		return acc + i
	})

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			in <- i
		}
		close(in)
	}()

	var got int
	for v := range out {
		got = v
	}

	wg.Wait()
	assert.Equal(t, 45, got)
}

func TestZip(t *testing.T) {
	t.Run("Zip should return a map with all elements from both slices if they are equal size", func(t *testing.T) {
		got := fp.Zip([]int{1, 2, 3}, []string{"a", "b", "c"})
		assert.Equal(t, map[int]string{1: "a", 2: "b", 3: "c"}, got)
	})

	t.Run("Zip should return a map with all the keys when the values slice is longer than keys slice", func(t *testing.T) {
		got := fp.Zip([]int{1, 2, 3}, []string{"a", "b", "c", "d"})
		assert.Equal(t, map[int]string{1: "a", 2: "b", 3: "c"}, got)
	})

	t.Run("Zip should return a map with all the values and only the keys with the corresponding indexes when there are more keys than values", func(t *testing.T) {
		got := fp.Zip([]int{1, 2, 3, 4}, []string{"a", "b", "c"})
		assert.Equal(t, map[int]string{1: "a", 2: "b", 3: "c"}, got)
	})
}
