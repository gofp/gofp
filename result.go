package gofp

import "fmt"

// Result is a struct holding a returned value or an error. It is useful way to handle errors
// in a functional way rather than the idiomatic Go way, so that we can create a pipeline using
// functional programming constructs and methods, such as map, filter, reduce etc.
type Result[T any] struct {
	value Option[T]
	err   error
}

// OK creates a Result with the given value and no error
func OK[T any](value ...T) Result[T] {
	r := Result[T]{
		value: None[T](),
	}

	if len(value) > 0 {
		r.value = Some(value[0])
	}

	return r
}

// Err creates a result with an error and no value
func Err[T any](err error) Result[T] {
	return Result[T]{
		value: None[T](),
		err:   err,
	}
}

// Result returns both the result and the error value
func (r Result[T]) Result() (Option[T], error) {
	return r.value, r.err
}

// Err returns the error value if there is one
func (r Result[T]) Err() error {
	return r.err
}

// Get returns the optional value of the result
func (r Result[T]) Get() Option[T] {
	return r.value
}

// GetOrElse returns the value of the result if there is no error, otherwise it returns the other value
func (r Result[T]) GetOrElse(other T) T {
	if error(r.err) == nil {
		return r.value.Must()
	}

	return other
}

// Must returns the value of the result if there is no error, otherwise it panics with the error
func (r Result[T]) Must() T {
	if error(r.err) != nil {
		panic(r.err)
	}
	return r.value.Must()
}

// IsErr returns true if the result has an error
func (r Result[T]) IsErr() bool {
	return error(r.err) != nil
}

// HasValue returns true if the result has a value and the value is not None
func (r Result[T]) HasValue() bool {
	err := error(r.err)
	hasErr := err == nil
	return hasErr && r.value.IsDefined()
}

// String returns a string representation of the result
func (r Result[T]) String() string {
	if error(r.err) == nil {
		if !r.value.IsDefined() {
			return "None"
		}
		return fmt.Sprintf("%v", r.value.Must())
	}
	return fmt.Sprintf("%v", r.err)
}
