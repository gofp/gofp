package gofp

// If is a ternary operator that returns the first value if the condition is true, otherwise the second value
func If[T any](condition bool, trueVal, falseVal T) T {
	if condition {
		return trueVal
	}
	return falseVal
}
