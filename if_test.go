package gofp_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	fp "gitlab.com/gofp/gofp"
)

func Test_If(t *testing.T) {
	t.Run("If evaluates boolean and returns the appropriate branch", testIfBool)
	t.Run("If branches return int value", testIfReturnsInt)
	t.Run("If branches return float value", testIfReturnsFloat)
	t.Run("If branches return string value", testIfReturnsString)
	t.Run("If branches return function", testIfReturnsFunction)
}

func testIfBool(t *testing.T) {
	t.Run("if boolean value true", func(t *testing.T) {
		got := fp.If(true, true, false)
		assert.Equal(t, true, got)
	})
	t.Run("If boolean value false", func(t *testing.T) {
		got := fp.If(false, true, false)
		assert.Equal(t, false, got)
	})

	a := 1
	b := 2

	t.Run("If boolean expression is true", func(t *testing.T) {
		got := fp.If(a < b, true, false)
		assert.Equal(t, true, got)
	})
	t.Run("If boolean expression is false", func(t *testing.T) {
		got := fp.If(a > b, true, false)
		assert.Equal(t, false, got)
	})
	t.Run("If boolean function returns true", func(t *testing.T) {
		got := fp.If(func() bool { return true }(), true, false)
		assert.Equal(t, true, got)
	})
	t.Run("If boolean function returns false", func(t *testing.T) {
		got := fp.If(func() bool { return false }(), true, false)
		assert.Equal(t, false, got)
	})
}

func testIfReturnsInt(t *testing.T) {
	t.Run("If true returns int", func(t *testing.T) {
		got := fp.If(true, 1, 2)
		assert.Equal(t, 1, got)
	})
	t.Run("If false returns int", func(t *testing.T) {
		got := fp.If(false, 1, 2)
		assert.Equal(t, 2, got)
	})
}

func testIfReturnsFloat(t *testing.T) {
	t.Run("If true returns float64", func(t *testing.T) {
		got := fp.If(true, 1.0, 2.0)
		assert.Equal(t, 1.0, got)
	})
	t.Run("If false returns float64", func(t *testing.T) {
		got := fp.If(false, 1.0, 2.0)
		assert.Equal(t, 2.0, got)
	})
}

func testIfReturnsString(t *testing.T) {
	t.Run("If true returns string", func(t *testing.T) {
		got := fp.If(true, "1", "2")
		assert.Equal(t, "1", got)
	})
	t.Run("If false returns string", func(t *testing.T) {
		got := fp.If(false, "1", "2")
		assert.Equal(t, "2", got)
	})
}

func testIfReturnsFunction(t *testing.T) {
	t.Run("If true returns function", func(t *testing.T) {
		got := fp.If(true, func() int { return 1 }, func() int { return 2 })
		assert.Equal(t, 1, got())
	})
	t.Run("If false returns function", func(t *testing.T) {
		got := fp.If(false, func() int { return 1 }, func() int { return 2 })
		assert.Equal(t, 2, got())
	})
}
