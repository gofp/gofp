package ranger_test

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/collection"
	"gitlab.com/gofp/gofp/ranger"
)

func TestRanger_While(t *testing.T) {
	t.Run("Range without step", func(t *testing.T) {
		w := ranger.While(0, func(i int) bool { return i == 100 })
		assert.Equal(t, 0, w.GetStart())
		assert.Equal(t, false, w.GetEnd())
		assert.Equal(t, 1, w.GetStep())
		w, n, ok := w.Next()
		assert.True(t, ok)
		assert.Equal(t, 0, n)
		w, n, ok = w.Next()
		assert.True(t, ok)
		assert.Equal(t, 1, n)
		w = w.Reset()
		w, n, ok = w.Next()
		assert.True(t, ok)
		assert.Equal(t, 0, n)
	})

	t.Run("Range with step", func(t *testing.T) {
		w := ranger.While(0, func(i int) bool { return i == 100 }, 2)
		assert.Equal(t, 0, w.GetStart())
		assert.Equal(t, false, w.GetEnd())
		assert.Equal(t, 2, w.GetStep())
		w, n, ok := w.Next()
		assert.True(t, ok)
		assert.Equal(t, 0, n)
		w, n, ok = w.Next()
		assert.True(t, ok)
		assert.Equal(t, 2, n)
		w = w.Reset()
		w, n, ok = w.Next()
		assert.True(t, ok)
		assert.Equal(t, 0, n)
	})

	t.Run("Range uint", func(t *testing.T) {
		w := ranger.While(uint(0), func(i uint) bool { return i == 100 })
		assert.Equal(t, uint(0), w.GetStart())
		assert.Equal(t, false, w.GetEnd())
		assert.Equal(t, uint(1), w.GetStep())
		w, n, ok := w.Next()
		assert.True(t, ok)
		assert.Equal(t, uint(0), n)
		w, n, ok = w.Next()
		assert.True(t, ok)
		assert.Equal(t, uint(1), n)
		w = w.Reset()
		w, n, ok = w.Next()
		assert.True(t, ok)
		assert.Equal(t, uint(0), n)
	})

	t.Run("Range float64", func(t *testing.T) {
		end := func(f float64) bool { return f >= float64(math.MaxUint64)+1.0 }
		w := ranger.While(float64(0), end)
		assert.Equal(t, float64(0), w.GetStart())
		assert.Equal(t, false, w.GetEnd())
		assert.Equal(t, float64(1), w.GetStep())
		w, n, ok := w.Next()
		assert.True(t, ok)
		assert.Equal(t, float64(0), n)
		w, n, ok = w.Next()
		assert.True(t, ok)
		assert.Equal(t, float64(1), n)
		w = w.Reset()
		w, n, ok = w.Next()
		assert.True(t, ok)
		assert.Equal(t, float64(0), n)
	})

	t.Run("Range float32", func(t *testing.T) {
		w := ranger.While(float32(0), func(f float32) bool { return f >= float32(100) })
		assert.Equal(t, float32(0), w.GetStart())
		assert.Equal(t, false, w.GetEnd())
		assert.Equal(t, float32(1), w.GetStep())
		w, n, ok := w.Next()
		assert.True(t, ok)
		assert.Equal(t, float32(0), n)
		w, n, ok = w.Next()
		assert.True(t, ok)
		assert.Equal(t, float32(1), n)
		w = w.Reset()
		w, n, ok = w.Next()
		assert.True(t, ok)
		assert.Equal(t, float32(0), n)
	})
}

func TestRanger_WhileWithStart(t *testing.T) {
	w := ranger.While(0, func(i int) bool { return i == 100 })
	assert.Equal(t, 0, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
	assert.Equal(t, 1, w.GetStep())
	for i := 0; i < 10; i++ {
		w, _, _ = w.Next()
	}
	w, n, ok := w.Next()
	assert.True(t, ok)
	assert.Equal(t, 10, n)
	w = w.WithStart(50)
	assert.Equal(t, 50, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
	assert.Equal(t, 1, w.GetStep())
	w, n, ok = w.Next()
	assert.True(t, ok)
	assert.Equal(t, 50, n)
	w, n, ok = w.Next()
	assert.True(t, ok)
	assert.Equal(t, 51, n)
	w = w.Reset()
	w, n, ok = w.Next()
	assert.True(t, ok)
	assert.Equal(t, 50, n)
}

func TestRanger_WhileWithEnd(t *testing.T) {
	w := ranger.While(0, func(i int) bool { return i == 100 })
	assert.Equal(t, 0, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
	assert.Equal(t, 1, w.GetStep())
	for i := 0; i < 10; i++ {
		w, _, _ = w.Next()
	}
	w, n, ok := w.Next()
	assert.True(t, ok)
	assert.Equal(t, 10, n)
	w = w.WithEnd(func(i int) bool { return i == 50 })
	assert.Equal(t, 0, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
	assert.Equal(t, 1, w.GetStep())
	w, n, ok = w.Next()
	assert.True(t, ok)
	assert.Equal(t, 0, n)
	w, n, ok = w.Next()
	assert.True(t, ok)
	assert.Equal(t, 1, n)
	w = w.Reset()
	assert.Equal(t, 0, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
}

func TestRanger_WhileWithStep(t *testing.T) {
	w := ranger.While(0, func(i int) bool { return i == 100 })
	assert.Equal(t, 0, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
	assert.Equal(t, 1, w.GetStep())
	for i := 0; i < 10; i++ {
		w, _, _ = w.Next()
	}
	w, n, ok := w.Next()
	assert.True(t, ok)
	assert.Equal(t, 10, n)
	w = w.WithStep(2)
	assert.Equal(t, 0, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
	assert.Equal(t, 2, w.GetStep())
	w, n, ok = w.Next()
	assert.True(t, ok)
	assert.Equal(t, 0, n)
	w, n, ok = w.Next()
	assert.True(t, ok)
	assert.Equal(t, 2, n)
}

func TestRanger_WhileWithStartAndEnd(t *testing.T) {
	w := ranger.While(0, func(i int) bool { return i == 100 })
	assert.Equal(t, 0, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
	assert.Equal(t, 1, w.GetStep())
	for i := 0; i < 10; i++ {
		w, _, _ = w.Next()
	}
	w, n, ok := w.Next()
	assert.True(t, ok)
	assert.Equal(t, 10, n)
	w = w.WithStartAndEnd(50, func(i int) bool { return i == 200 })
	assert.Equal(t, 50, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
	assert.Equal(t, 1, w.GetStep())
	w, n, ok = w.Next()
	assert.True(t, ok)
	assert.Equal(t, 50, n)
	w, n, ok = w.Next()
	assert.True(t, ok)
	assert.Equal(t, 51, n)
	w = w.Reset()
	assert.Equal(t, 50, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
	assert.Equal(t, 1, w.GetStep())
}

func TestRanger_WhileChainableWith(t *testing.T) {
	w := ranger.While(0, func(i int) bool { return i == 100 })
	assert.Equal(t, 0, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
	assert.Equal(t, 1, w.GetStep())
	for i := 0; i < 10; i++ {
		w, _, _ = w.Next()
	}
	w, n, ok := w.Next()
	assert.True(t, ok)
	assert.Equal(t, 10, n)
	w = w.WithStart(50).WithEnd(func(i int) bool { return i == 200 }).WithStep(2)
	assert.Equal(t, 50, w.GetStart())
	assert.Equal(t, false, w.GetEnd())
	assert.Equal(t, 2, w.GetStep())
}

func TestRanger_WhileForEach(t *testing.T) {
	v := 0
	ranger.While(0, func(i int) bool { return i == 100 }).ForEach(func(n int) {
		v = n
	})
	assert.Equal(t, 99, v)
}

func TestRanger_WhileForEachResult(t *testing.T) {
	rs := ranger.While(0, func(i int) bool { return i == 3 }).ForEachResult(func(n int) fp.Result[int] {
		return fp.OK(n)
	})
	assert.Equal(t, collection.List[fp.Result[int]]{fp.OK(0), fp.OK(1), fp.OK(2)}, rs)
}

func TestRanger_WhileMap(t *testing.T) {
	rs := ranger.While(0, func(i int) bool { return i == 3 }).Map(func(n int) int {
		return n * 2
	})
	assert.Equal(t, collection.List[int]{0, 2, 4}, rs)
}

func TestRanger_WhileFilter(t *testing.T) {
	t.Run("Filter all values", func(t *testing.T) {
		rs := ranger.While(0, func(i int) bool { return i == 10 }).Filter(func(n int) bool {
			return n%2 == 0
		}, 0)
		assert.Equal(t, collection.List[int]{0, 2, 4, 6, 8}, rs)
	})
	t.Run("Filter max 3 results", func(t *testing.T) {
		rs := ranger.While(0, func(i int) bool { return i == 10 }).Filter(func(n int) bool {
			return n%2 == 0
		}, 3)
		assert.Equal(t, collection.List[int]{0, 2, 4}, rs)
	})
}

func TestRanger_WhileFilterNot(t *testing.T) {
	t.Run("FilterNot all values", func(t *testing.T) {
		rs := ranger.While(0, func(i int) bool { return i == 10 }).FilterNot(func(n int) bool {
			return n%2 == 0
		}, 0)
		assert.Equal(t, collection.List[int]{1, 3, 5, 7, 9}, rs)
	})
	t.Run("FilterNot max 3 results", func(t *testing.T) {
		rs := ranger.While(0, func(i int) bool { return i == 10 }).FilterNot(func(n int) bool {
			return n%2 == 0
		}, 3)
		assert.Equal(t, collection.List[int]{1, 3, 5}, rs)
	})
}

func TestRanger_WhilePartition(t *testing.T) {
	matched, unmatched := ranger.While(0, func(i int) bool { return i == 10 }).Partition(func(n int) bool {
		return n%2 == 0
	})
	assert.Equal(t, collection.List[int]{0, 2, 4, 6, 8}, matched)
	assert.Equal(t, collection.List[int]{1, 3, 5, 7, 9}, unmatched)
}

func TestRanger_WhileFold(t *testing.T) {
	sum := ranger.While(0, func(i int) bool { return i == 10 }).Fold(0, func(acc, n int) int {
		return acc + n
	})
	assert.Equal(t, 45, sum)
}

func TestRanger_WhileFoldWhile(t *testing.T) {
	max := 10
	v := 0
	sum := ranger.While(0, func(i int) bool { return i == 10 }).FoldWhile(0, func(acc, n int) int {
		v = acc + n
		return v
	}, func() bool { return v < max })
	assert.Equal(t, 10, sum)
}

func TestRanger_WhileFoldUntil(t *testing.T) {
	max := 10
	v := 0
	sum := ranger.While(0, func(i int) bool { return i == 10 }).FoldUntil(0, func(acc, n int) int {
		v = acc + n
		return v
	}, func() bool { return v == max })
	assert.Equal(t, 10, sum)
}

func TestRanger_WhileCollect(t *testing.T) {
	rs := ranger.While(0, func(i int) bool { return i == 10 }).Collect()
	assert.Equal(t, collection.List[int]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, rs)

	rs = ranger.While(0, func(i int) bool { return i == 10 }, 2).Collect()
	assert.Equal(t, collection.List[int]{0, 2, 4, 6, 8}, rs)
}

func TestRanger_WhileCollectN(t *testing.T) {
	rs := ranger.While(0, func(i int) bool { return i == 10 }).CollectN(3)
	assert.Equal(t, collection.List[int]{0, 1, 2}, rs)
	rs = ranger.While(0, func(i int) bool { return i == 10 }, 2).CollectN(10)
	assert.Equal(t, collection.List[int]{0, 2, 4, 6, 8}, rs)
}

func BenchmarkRangerWhileCollect(b *testing.B) {
	b.Run("Ranger collect", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			ranger.While(1, func(i int) bool { return i == 100000 }).Collect()
		}
	})

	b.Run("For loop append list", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			l := make([]int, 100000)
			for i := 0; i < 100000; i++ {
				l[i] = i + 1
			}
		}
	})
}

func BenchmarkRangerWhileIteration(b *testing.B) {
	b.Run("While iteration", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			ranger.While(0, func(i int) bool { return i == 100000 }).ForEach(func(i int) {
				_ = i
			})
		}
	})

	b.Run("While loop", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			i := 0
			for i < 100000 {
				_ = i
				i++
			}
		}
	})
}

func BenchmarkRangerWhile(b *testing.B) {
	b.Run("Calculate the sum of the first 10 natural numbers whose squares are divisible by 5 using ranger", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			ranger.While(1, func(i int) bool { return i == 100 }).
				Filter(func(a int) bool { return (a*a)%5 == 0 }, 10).
				Sum(fp.Add[int])
		}
	})

	b.Run("Calculate the sum of the first 10 natural numbers whose squares are divisible by 5 using range fold", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			count := 0
			ranger.While(1, func(i int) bool { return i == 100 }).
				FoldWhile(0, func(a, b int) int {
					if (b*b)%5 == 0 {
						count++
						return a + b
					}
					return a
				}, func() bool { return count < 10 })
		}
	})

	b.Run("Calculate the sum of the first 10 natural numbers whose squares are divisible by 5 the imperative way", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			sumOfSquaresDivisibleBy5While(100, 10)
		}
	})

	b.Run("Calculate the sum of the first 1000 natural numbers whose squares are divisible by 5 using ranger", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			ranger.While(1, func(i int) bool { return i == 10000 }).
				Filter(func(a int) bool { return (a*a)%5 == 0 }, 1000).
				Sum(fp.Add[int])
		}
	})

	b.Run("Calculate the sum of the first 1000 natural numbers whose squares are divisible by 5 using range fold", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			count := 0
			ranger.While(1, func(i int) bool { return i == 10000 }).
				FoldWhile(0, func(a, b int) int {
					if (b*b)%5 == 0 {
						count++
						return a + b
					}
					return a
				}, func() bool { return count < 1000 })
		}
	})

	b.Run("Calculate the sum of the first 1000 natural numbers whose squares are divisible by 5 the imperative way", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			sumOfSquaresDivisibleBy5While(10000, 1000)
		}
	})
}

func sumOfSquaresDivisibleBy5While(n, max int) int {
	var sum int
	count := 0
	for i := 1; i <= n; i++ {
		if i*i%5 == 0 {
			sum += i
			count++
		}

		if count == max {
			break
		}
	}
	return sum
}
