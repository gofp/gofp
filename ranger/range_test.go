package ranger_test

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/collection"
	"gitlab.com/gofp/gofp/ranger"
)

func TestRanger_Range(t *testing.T) {
	t.Run("Range without step", func(t *testing.T) {
		r := ranger.Range(0, 100)
		assert.Equal(t, 0, r.GetStart())
		assert.Equal(t, 100, r.GetEnd())
		assert.Equal(t, 1, r.GetStep())
		r, n, ok := r.Next()
		assert.True(t, ok)
		assert.Equal(t, 0, n)
		r, n, ok = r.Next()
		assert.True(t, ok)
		assert.Equal(t, 1, n)
		r = r.Reset()
		r, n, ok = r.Next()
		assert.True(t, ok)
		assert.Equal(t, 0, n)
	})

	t.Run("Range with step", func(t *testing.T) {
		r := ranger.Range(0, 100, 2)
		assert.Equal(t, 0, r.GetStart())
		assert.Equal(t, 100, r.GetEnd())
		assert.Equal(t, 2, r.GetStep())
		r, n, ok := r.Next()
		assert.True(t, ok)
		assert.Equal(t, 0, n)
		r, n, ok = r.Next()
		assert.True(t, ok)
		assert.Equal(t, 2, n)
		r = r.Reset()
		r, n, ok = r.Next()
		assert.True(t, ok)
		assert.Equal(t, 0, n)
	})

	t.Run("Range uint", func(t *testing.T) {
		r := ranger.Range(uint(0), uint(100))
		assert.Equal(t, uint(0), r.GetStart())
		assert.Equal(t, uint(100), r.GetEnd())
		assert.Equal(t, uint(1), r.GetStep())
		r, n, ok := r.Next()
		assert.True(t, ok)
		assert.Equal(t, uint(0), n)
		r, n, ok = r.Next()
		assert.True(t, ok)
		assert.Equal(t, uint(1), n)
		r = r.Reset()
		r, n, ok = r.Next()
		assert.True(t, ok)
		assert.Equal(t, uint(0), n)
	})

	t.Run("Range float64", func(t *testing.T) {
		end := float64(math.MaxUint64) + 1.0
		r := ranger.Range(float64(0), end)
		assert.Equal(t, float64(0), r.GetStart())
		assert.Equal(t, end, r.GetEnd())
		assert.Equal(t, float64(1), r.GetStep())
		r, n, ok := r.Next()
		assert.True(t, ok)
		assert.Equal(t, float64(0), n)
		r, n, ok = r.Next()
		assert.True(t, ok)
		assert.Equal(t, float64(1), n)
		r = r.Reset()
		r, n, ok = r.Next()
		assert.True(t, ok)
		assert.Equal(t, float64(0), n)
	})

	t.Run("Range float32", func(t *testing.T) {
		r := ranger.Range(float32(0), float32(100))
		assert.Equal(t, float32(0), r.GetStart())
		assert.Equal(t, float32(100), r.GetEnd())
		assert.Equal(t, float32(1), r.GetStep())
		r, n, ok := r.Next()
		assert.True(t, ok)
		assert.Equal(t, float32(0), n)
		r, n, ok = r.Next()
		assert.True(t, ok)
		assert.Equal(t, float32(1), n)
		r = r.Reset()
		r, n, ok = r.Next()
		assert.True(t, ok)
		assert.Equal(t, float32(0), n)
	})
}

func TestRanger_WithStart(t *testing.T) {
	r := ranger.Range(0, 100)
	assert.Equal(t, 0, r.GetStart())
	assert.Equal(t, 100, r.GetEnd())
	assert.Equal(t, 1, r.GetStep())
	for i := 0; i < 10; i++ {
		r, _, _ = r.Next()
	}
	r, n, ok := r.Next()
	assert.True(t, ok)
	assert.Equal(t, 10, n)
	r = r.WithStart(50)
	assert.Equal(t, 50, r.GetStart())
	assert.Equal(t, 100, r.GetEnd())
	assert.Equal(t, 1, r.GetStep())
	r, n, ok = r.Next()
	assert.True(t, ok)
	assert.Equal(t, 50, n)
	r, n, ok = r.Next()
	assert.True(t, ok)
	assert.Equal(t, 51, n)
	r = r.Reset()
	r, n, ok = r.Next()
	assert.True(t, ok)
	assert.Equal(t, 50, n)
}

func TestRanger_WithEnd(t *testing.T) {
	r := ranger.Range(0, 100)
	assert.Equal(t, 0, r.GetStart())
	assert.Equal(t, 100, r.GetEnd())
	assert.Equal(t, 1, r.GetStep())
	for i := 0; i < 10; i++ {
		r, _, _ = r.Next()
	}
	r, n, ok := r.Next()
	assert.True(t, ok)
	assert.Equal(t, 10, n)
	r = r.WithEnd(50)
	assert.Equal(t, 0, r.GetStart())
	assert.Equal(t, 50, r.GetEnd())
	assert.Equal(t, 1, r.GetStep())
	r, n, ok = r.Next()
	assert.True(t, ok)
	assert.Equal(t, 0, n)
	r, n, ok = r.Next()
	assert.True(t, ok)
	assert.Equal(t, 1, n)
	r = r.Reset()
	assert.Equal(t, 0, r.GetStart())
	assert.Equal(t, 50, r.GetEnd())
}

func TestRanger_WithStep(t *testing.T) {
	r := ranger.Range(0, 100)
	assert.Equal(t, 0, r.GetStart())
	assert.Equal(t, 100, r.GetEnd())
	assert.Equal(t, 1, r.GetStep())
	for i := 0; i < 10; i++ {
		r, _, _ = r.Next()
	}
	r, n, ok := r.Next()
	assert.True(t, ok)
	assert.Equal(t, 10, n)
	r = r.WithStep(2)
	assert.Equal(t, 0, r.GetStart())
	assert.Equal(t, 100, r.GetEnd())
	assert.Equal(t, 2, r.GetStep())
	r, n, ok = r.Next()
	assert.True(t, ok)
	assert.Equal(t, 0, n)
	r, n, ok = r.Next()
	assert.True(t, ok)
	assert.Equal(t, 2, n)
}

func TestRanger_WithStartAndEnd(t *testing.T) {
	r := ranger.Range(0, 100)
	assert.Equal(t, 0, r.GetStart())
	assert.Equal(t, 100, r.GetEnd())
	assert.Equal(t, 1, r.GetStep())
	for i := 0; i < 10; i++ {
		r, _, _ = r.Next()
	}
	r, n, ok := r.Next()
	assert.True(t, ok)
	assert.Equal(t, 10, n)
	r = r.WithStartAndEnd(50, 200)
	assert.Equal(t, 50, r.GetStart())
	assert.Equal(t, 200, r.GetEnd())
	assert.Equal(t, 1, r.GetStep())
	r, n, ok = r.Next()
	assert.True(t, ok)
	assert.Equal(t, 50, n)
	r, n, ok = r.Next()
	assert.True(t, ok)
	assert.Equal(t, 51, n)
	r = r.Reset()
	assert.Equal(t, 50, r.GetStart())
	assert.Equal(t, 200, r.GetEnd())
	assert.Equal(t, 1, r.GetStep())
}

func TestRanger_ChainableWith(t *testing.T) {
	r := ranger.Range(0, 100)
	assert.Equal(t, 0, r.GetStart())
	assert.Equal(t, 100, r.GetEnd())
	assert.Equal(t, 1, r.GetStep())
	for i := 0; i < 10; i++ {
		r, _, _ = r.Next()
	}
	r, n, ok := r.Next()
	assert.True(t, ok)
	assert.Equal(t, 10, n)
	r = r.WithStart(50).WithEnd(200).WithStep(2)
	assert.Equal(t, 50, r.GetStart())
	assert.Equal(t, 200, r.GetEnd())
	assert.Equal(t, 2, r.GetStep())
}

func TestRanger_ForEach(t *testing.T) {
	v := 0
	ranger.Range(0, 100).ForEach(func(n int) {
		v = n
	})
	assert.Equal(t, 99, v)
}

func TestRanger_ForEachResult(t *testing.T) {
	rs := ranger.Range(0, 3).ForEachResult(func(n int) fp.Result[int] {
		return fp.OK(n)
	})
	assert.Equal(t, collection.List[fp.Result[int]]{fp.OK(0), fp.OK(1), fp.OK(2)}, rs)
}

func TestRanger_Map(t *testing.T) {
	rs := ranger.Range(0, 3).Map(func(n int) int {
		return n * 2
	})
	assert.Equal(t, collection.List[int]{0, 2, 4}, rs)
}

func TestRanger_Filter(t *testing.T) {
	t.Run("Filter all values", func(t *testing.T) {
		rs := ranger.Range(0, 10).Filter(func(n int) bool {
			return n%2 == 0
		}, 0)
		assert.Equal(t, collection.List[int]{0, 2, 4, 6, 8}, rs)
	})
	t.Run("Filter max 3 results", func(t *testing.T) {
		rs := ranger.Range(0, 10).Filter(func(n int) bool {
			return n%2 == 0
		}, 3)
		assert.Equal(t, collection.List[int]{0, 2, 4}, rs)
	})
}

func TestRanger_FilterNot(t *testing.T) {
	t.Run("FilterNot all values", func(t *testing.T) {
		rs := ranger.Range(0, 10).FilterNot(func(n int) bool {
			return n%2 == 0
		}, 0)
		assert.Equal(t, collection.List[int]{1, 3, 5, 7, 9}, rs)
	})
	t.Run("FilterNot max 3 results", func(t *testing.T) {
		rs := ranger.Range(0, 10).FilterNot(func(n int) bool {
			return n%2 == 0
		}, 3)
		assert.Equal(t, collection.List[int]{1, 3, 5}, rs)
	})
}

func TestRanger_Partition(t *testing.T) {
	matched, unmatched := ranger.Range(0, 10).Partition(func(n int) bool {
		return n%2 == 0
	})
	assert.Equal(t, collection.List[int]{0, 2, 4, 6, 8}, matched)
	assert.Equal(t, collection.List[int]{1, 3, 5, 7, 9}, unmatched)
}

func TestRanger_Fold(t *testing.T) {
	sum := ranger.Range(0, 10).Fold(0, func(acc, n int) int {
		return acc + n
	})
	assert.Equal(t, 45, sum)
}

func TestRanger_FoldWhile(t *testing.T) {
	max := 10
	v := 0
	sum := ranger.Range(0, 10).FoldWhile(0, func(acc, n int) int {
		v = acc + n
		return v
	}, func() bool { return v < max })
	assert.Equal(t, 10, sum)
}

func TestRanger_FoldUntil(t *testing.T) {
	max := 10
	v := 0
	sum := ranger.Range(0, 10).FoldUntil(0, func(acc, n int) int {
		v = acc + n
		return v
	}, func() bool { return v == max })
	assert.Equal(t, 10, sum)
}

func TestRanger_Collect(t *testing.T) {
	rs := ranger.Range(0, 10).Collect()
	assert.Equal(t, collection.List[int]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, rs)

	rs = ranger.Range(0, 10, 2).Collect()
	assert.Equal(t, collection.List[int]{0, 2, 4, 6, 8}, rs)
}

func TestRanger_CollectN(t *testing.T) {
	rs := ranger.Range(0, 10).CollectN(3)
	assert.Equal(t, collection.List[int]{0, 1, 2}, rs)
	rs = ranger.Range(0, 10, 2).CollectN(10)
	assert.Equal(t, collection.List[int]{0, 2, 4, 6, 8}, rs)
}

func BenchmarkRangerCollect(b *testing.B) {
	b.Run("Ranger collect", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			ranger.Range(1, 100000).Collect()
		}
	})

	b.Run("For loop append list", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			l := make([]int, 100000)
			for i := 0; i < 100000; i++ {
				l[i] = i + 1
			}
		}
	})
}

func BenchmarkRangerIteration(b *testing.B) {
	b.Run("Ranger iteration", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			ranger.Range(0, 100000).ForEach(func(i int) {
				_ = i
			})
		}
	})

	b.Run("For loop", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			for i := 0; i < 100000; i++ {
				_ = i
			}
		}
	})
}

func BenchmarkRanger(b *testing.B) {
	b.Run("Calculate the sum of the first 10 natural numbers whose squares are divisible by 5 using range filter sum", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			ranger.Range(1, 100).
				Filter(func(a int) bool { return (a*a)%5 == 0 }, 10).
				Sum(fp.Add[int])
		}
	})

	b.Run("Calculate the sum of the first 10 natural numbers whose squares are divisible by 5 using range fold", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			count := 0
			ranger.Range(1, 100).
				FoldWhile(0, func(a, b int) int {
					if (b*b)%5 == 0 {
						count++
						return a + b
					}
					return a
				}, func() bool { return count < 10 })
		}
	})

	b.Run("Calculate the sum of the first 10 natural numbers whose squares are divisible by 5 the imperative way", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			sumOfSquaresDivisibleBy5(100, 10)
		}
	})

	b.Run("Calculate the sum of the first 1000 natural numbers whose squares are divisible by 5 using ranger", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			ranger.Range(1, 10000).
				Filter(func(a int) bool { return (a*a)%5 == 0 }, 1000).
				Sum(fp.Add[int])
		}
	})

	b.Run("Calculate the sum of the first 1000 natural numbers whose squares are divisible by 5 using range fold", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			count := 0
			ranger.Range(1, 10000).
				FoldWhile(0, func(a, b int) int {
					if (b*b)%5 == 0 {
						count++
						return a + b
					}
					return a
				}, func() bool { return count < 1000 })
		}
	})

	b.Run("Calculate the sum of the first 1000 natural numbers whose squares are divisible by 5 the imperative way", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			sumOfSquaresDivisibleBy5(10000, 1000)
		}
	})
}

func sumOfSquaresDivisibleBy5(n, max int) int {
	var sum int
	count := 0
	for i := 1; i <= n; i++ {
		if i*i%5 == 0 {
			sum += i
			count++
		}

		if count == max {
			break
		}
	}
	return sum
}
