package ranger

import (
	"math"

	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/collection"
)

// Ranger is a reusable functional range generator that can be iterated over and used to perform
// functional programming type operations such as map, fold, filter, partition, etc.
// The next number in the range is only generated when it's needed and is not preallocated
// just like a regular for loop, but with functional bells and whistles
type Ranger[T Number] struct {
	start   T
	end     T
	step    T
	cap     uint64
	current T
}

// Range creates a range of numbers that can be used to perform calculations with.
func Range[T Number](start T, end T, step ...T) Ranger[T] {
	var cap uint64
	switch v := interface{}(end).(type) {
	case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64:
		cap = uint64(v.(T))
	case float32, float64:
		vf := float64(v.(T))
		if vf >= float64(math.MaxUint64) {
			cap = math.MaxUint64
		} else {
			cap = uint64(vf)
		}
	}

	var ss T

	if len(step) > 0 {
		ss = step[0]
	} else {
		ss = 1
	}

	r := Ranger[T]{
		start:   start,
		end:     end,
		step:    ss,
		cap:     cap,
		current: start,
	}

	return r
}

// Next returns the next value in the range
// we can use Next to consume the values in the range one-by-one
func (r Ranger[T]) Next() (Ranger[T], T, bool) {
	t := r.current
	ok := true

	r.current = r.current + r.step

	if r.current > r.end {
		r.current = r.end
		ok = false
	}

	return r, t, ok
}

// Reset resets the ranger to the start of the range
func (r Ranger[T]) Reset() Ranger[T] {
	r.current = r.start
	return r
}

// WithStart returns a new ranger with the given start value
// and the ranger will be restarted.
func (r Ranger[T]) WithStart(s T) Ranger[T] {
	r.start = s
	return r.Reset()
}

// WithEnd returns a new ranger with the given end value
// and the ranger will be restarted.
func (r Ranger[T]) WithEnd(e T) Ranger[T] {
	r.end = e
	return r.Reset()
}

// WithStep returns a new ranger with the given step value
// and the ranger will be restarted.
func (r Ranger[T]) WithStep(st T) Ranger[T] {
	r.step = st
	return r.Reset()
}

// WithStartAndEnd returns a new ranger with the given start and end values
// and the ranger will be restarted.
func (r Ranger[T]) WithStartAndEnd(s T, e T) Ranger[T] {
	r.start = s
	r.end = e

	return r.Reset()
}

// GetStart returns the start value of the ranger
func (r Ranger[T]) GetStart() T {
	return r.start
}

// GetEnd returns the end value of the ranger
func (r Ranger[T]) GetEnd() T {
	return r.end
}

// GetStep returns the step value of the ranger
func (r Ranger[T]) GetStep() T {
	return r.step
}

// ForEach iterates over the ranger and calls the given function for each value in the ranger
func (r Ranger[T]) ForEach(pred func(T)) {
	rr, v, ok := r.Next()
	for ok {
		pred(v)
		rr, v, ok = rr.Next()
	}
}

// ForEachResult iterates over the values from the ranger and executes the predicate and collects the results
func (r Ranger[T]) ForEachResult(pred func(T) fp.Result[T]) collection.List[fp.Result[T]] {
	results := make(collection.List[fp.Result[T]], 0, r.cap)

	rr, v, ok := r.Next()
	for ok {
		results = append(results, pred(v))
		rr, v, ok = rr.Next()
	}

	return results
}

// Map iterates over the values of the ranger mapping the values and collects the results into a new collection.List
func (r Ranger[T]) Map(pred func(T) T) collection.List[T] {
	results := make(collection.List[T], 0, r.cap)

	rr, v, ok := r.Next()
	for ok {
		results = append(results, pred(v))
		rr, v, ok = rr.Next()
	}

	return results
}

// Filter iterates over the values of the ranger filtering the values and collects the results into a new collection.List
func (r Ranger[T]) Filter(pred func(T) bool, max int) collection.List[T] {
	cap := r.cap

	if max > 0 {
		cap = uint64(max)
	}

	results := make(collection.List[T], cap)

	count := 0
	rr, v, ok := r.Next()
	for ok {
		if pred(v) {
			results[count] = v
			count++
		}

		if max > 0 && count >= max {
			break
		}
		rr, v, ok = rr.Next()
	}

	if uint64(count) >= cap {
		return results
	}

	return results[:count]
}

// FilterNot iterates over the values of the ranger filtering the values that don't match and collects the results into a new collection.List
func (r Ranger[T]) FilterNot(pred func(T) bool, max int) collection.List[T] {
	cap := r.cap

	if max > 0 {
		cap = uint64(max)
	}

	results := make(collection.List[T], cap)

	count := 0
	rr, v, ok := r.Next()
	for ok {
		if !pred(v) {
			results[count] = v
			count++
		}

		if max > 0 && count >= max {
			break
		}
		rr, v, ok = rr.Next()
	}

	if uint64(count) >= cap {
		return results
	}

	return results[:count]
}

// Partition iterates over the values of the ranger partitioning the values and collects the results into a new collection.List
func (r Ranger[T]) Partition(pred func(T) bool) (collection.List[T], collection.List[T]) {
	results := make(collection.List[T], 0, r.cap)
	results2 := make(collection.List[T], 0, r.cap)

	rr, v, ok := r.Next()
	for ok {
		if pred(v) {
			results = append(results, v)
		} else {
			results2 = append(results2, v)
		}
		rr, v, ok = rr.Next()
	}

	return results, results2
}

// Fold iterates over the values of the ranger folding the values and collects the results into a new collection.List
func (r Ranger[T]) Fold(init T, pred func(T, T) T) T {
	result := init

	rr, v, ok := r.Next()
	for ok {
		result = pred(result, v)
		rr, v, ok = rr.Next()
	}

	return result
}

// FoldWhile iterates the values of the ranger folding the values until any provided while predicate is false then returns the result
func (r Ranger[T]) FoldWhile(init T, pred func(T, T) T, while ...func() bool) T {
	result := init
	rr, v, ok := r.Next()
	for ok {
		result = pred(result, v)
		for _, fn := range while {
			if !fn() {
				return result
			}
		}

		rr, v, ok = rr.Next()
	}

	return result
}

// FoldUntil iterates the values of the ranger folding the values until any provided until predicate is true then returns the result
func (r Ranger[T]) FoldUntil(init T, pred func(T, T) T, until ...func() bool) T {
	result := init
	rr, v, ok := r.Next()
	for ok {
		result = pred(result, v)
		for _, fn := range until {
			if fn() {
				return result
			}
		}

		rr, v, ok = rr.Next()
	}

	return result
}

// Collect iterates over the values in the range and collects the values into a new collection.List
// This is a slow operation as it will continue to append new values to a collection until the range is exhausted
// For a faster collect operation, use the CollectN function and specify the maximum number of values to collect
func (r Ranger[T]) Collect() collection.List[T] {
	nums := make([]T, r.cap)
	count := 0
	rr, v, ok := r.Next()
	for ok {
		nums[count] = v
		count++
		rr, v, ok = rr.Next()
	}

	if uint64(count) >= r.cap {
		return nums
	}

	return nums[:count]
}

// CollectN iterates over the values in the range and collects the values into a new collection.List up to the
// given maximum number of values.
func (r Ranger[T]) CollectN(max uint64) collection.List[T] {
	nums := make([]T, max)
	var i uint64

	rr, v, ok := r.Next()
	for ok {
		nums[i] = v
		i++

		if i >= max {
			return nums
		}
		rr, v, ok = rr.Next()
	}

	// If we get here, we didn't collect enough values so we need to return a slice of the collected values only
	// i is already pointing to the index after the last valid input so we can just take a slice up to that index
	return nums[:i]
}
