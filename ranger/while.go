package ranger

import (
	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/collection"
)

// Whiler is a reusable functional range generator that can be iterated over and used to perform
// functional programming type operations such as map, fold, filter, partition, etc.
// The next number in the range is only generated when it's needed and is not preallocated
// just like a regular for loop, but with functional bells and whistles
type Whiler[T Number] struct {
	start   T
	end     func(T) bool
	step    T
	current T
}

// New creates a range of numbers that can be used to perform calculations with.
func While[T Number](start T, end func(T) bool, step ...T) Whiler[T] {
	var ss T

	if len(step) > 0 {
		ss = step[0]
	} else {
		ss = 1
	}

	w := Whiler[T]{
		start:   start,
		end:     end,
		step:    ss,
		current: start,
	}

	return w
}

// Next returns the next value in the range
// we can use Next to consume the values in the range one-by-one
func (w Whiler[T]) Next() (Whiler[T], T, bool) {
	if w.end(w.current) {
		return w, w.current, false
	}

	t := w.current

	w.current = w.current + w.step

	return w, t, true
}

// Reset resets the ranger to the start of the range
func (w Whiler[T]) Reset() Whiler[T] {
	w.current = w.start
	return w
}

// WithStart returns a new ranger with the given start value
// and the ranger will be restarted.
func (w Whiler[T]) WithStart(s T) Whiler[T] {
	w.start = s
	return w.Reset()
}

// WithEnd returns a new ranger with the given end value
// and the ranger will be restarted.
func (w Whiler[T]) WithEnd(e func(T) bool) Whiler[T] {
	w.end = e
	return w.Reset()
}

// WithStep returns a new ranger with the given step value
// and the ranger will be restarted.
func (w Whiler[T]) WithStep(st T) Whiler[T] {
	w.step = st
	return w.Reset()
}

// WithStartAndEnd returns a new ranger with the given start and end values
// and the ranger will be restarted.
func (w Whiler[T]) WithStartAndEnd(s T, e func(T) bool) Whiler[T] {
	w.start = s
	w.end = e
	return w.Reset()
}

// GetStart returns the start value of the ranger
func (w Whiler[T]) GetStart() T {
	return w.start
}

// GetEnd returns true if we have reached the end of the while loop
func (w Whiler[T]) GetEnd() bool {
	return w.end(w.current)
}

// GetStep returns the step value of the ranger
func (w Whiler[T]) GetStep() T {
	return w.step
}

// ForEach iterates over the ranger and calls the given function for each value in the ranger
func (w Whiler[T]) ForEach(pred func(T)) {
	ww, v, ok := w.Next()
	for ok {
		pred(v)
		ww, v, ok = ww.Next()
	}
}

// ForEachResult iterates over the values from the ranger and executes the predicate and collects the results
func (w Whiler[T]) ForEachResult(pred func(T) fp.Result[T]) collection.List[fp.Result[T]] {
	results := make(collection.List[fp.Result[T]], 0)

	ww, v, ok := w.Next()
	for ok {
		results = append(results, pred(v))
		ww, v, ok = ww.Next()
	}

	return results
}

// Map iterates over the values of the ranger mapping the values and collects the results into a new collection.List
func (w Whiler[T]) Map(pred func(T) T) collection.List[T] {
	results := make(collection.List[T], 0)

	ww, v, ok := w.Next()
	for ok {
		results = append(results, pred(v))
		ww, v, ok = ww.Next()
	}

	return results
}

// Filter iterates over the values of the ranger filtering the values and collects the results that satisfy the predicate into a new collection.List
func (w Whiler[T]) Filter(pred func(T) bool, max int) collection.List[T] {
	results := make(collection.List[T], 0)

	count := 0
	ww, v, ok := w.Next()
	for ok {
		if pred(v) {
			results = append(results, v)
			count++
		}

		if max > 0 && count >= max {
			break
		}
		ww, v, ok = ww.Next()
	}

	return results
}

// FilterNot iterates over the values of the ranger filtering the values and collects the results that do not satisfy the predicate into a new collection.List
func (w Whiler[T]) FilterNot(pred func(T) bool, max int) collection.List[T] {
	results := make(collection.List[T], 0)

	count := 0
	ww, v, ok := w.Next()
	for ok {
		if !pred(v) {
			results = append(results, v)
			count++
		}

		if max > 0 && count >= max {
			break
		}
		ww, v, ok = ww.Next()
	}

	return results
}

// Partition iterates over the values of the ranger partitioning the values and collects the results into a new collection.List
func (w Whiler[T]) Partition(pred func(T) bool) (collection.List[T], collection.List[T]) {
	results := make(collection.List[T], 0)
	results2 := make(collection.List[T], 0)

	ww, v, ok := w.Next()
	for ok {
		if pred(v) {
			results = append(results, v)
		} else {
			results2 = append(results2, v)
		}
		ww, v, ok = ww.Next()
	}

	return results, results2
}

// Fold iterates over the values of the ranger folding the values and collects the results into a new collection.List
func (w Whiler[T]) Fold(init T, pred func(T, T) T) T {
	var result T

	ww, v, ok := w.Next()
	for ok {
		result = pred(result, v)
		ww, v, ok = ww.Next()
	}

	return result
}

// FoldWhile iterates the values of the ranger folding the values until any provided while predicate is false then returns the result
func (w Whiler[T]) FoldWhile(init T, pred func(T, T) T, while ...func() bool) T {
	result := init
	ww, v, ok := w.Next()
	for ok {
		result = pred(result, v)
		for _, fn := range while {
			if !fn() {
				return result
			}
		}

		ww, v, ok = ww.Next()
	}

	return result
}

// FoldUntil iterates the values of the ranger folding the values until any provided until predicate is true then returns the result
func (w Whiler[T]) FoldUntil(init T, pred func(T, T) T, until ...func() bool) T {
	result := init
	ww, v, ok := w.Next()
	for ok {
		result = pred(result, v)
		for _, fn := range until {
			if fn() {
				return result
			}
		}

		ww, v, ok = ww.Next()
	}

	return result
}

// Collect iterates over the values in the range and collects the values into a new collection.List
// This is a slow operation as it will continue to append new values to a collection until the range is exhausted
// For a faster collect operation, use the CollectN function and specify the maximum number of values to collect
func (w Whiler[T]) Collect() collection.List[T] {
	nums := make([]T, 0)
	count := 0
	ww, v, ok := w.Next()
	for ok {
		nums = append(nums, v)
		count++
		ww, v, ok = ww.Next()
	}

	return nums
}

// CollectN iterates over the values in the range and collects the values into a new collection.List up to the
// given maximum number of values.
func (w Whiler[T]) CollectN(max uint64) collection.List[T] {
	nums := make([]T, max)
	var i uint64

	ww, v, ok := w.Next()
	for ok {
		nums[i] = v
		i++

		if i >= max {
			return nums
		}
		ww, v, ok = ww.Next()
	}

	// If we get here, we didn't collect enough values so we need to return a slice of the collected values only
	// i is already pointing to the index after the last valid input so we can just take a slice up to that index
	return nums[:i]
}
