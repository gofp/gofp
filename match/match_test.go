package match_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/match"
)

func TestMatch(t *testing.T) {
	assert.Equal(t, "a", matchValue(3, getMatcher()).Must())
	assert.Equal(t, "a", matchValue(5, getMatcher()).Must())
	assert.Equal(t, "b", matchValue(8, getMatcher()).Must())
	assert.Equal(t, "b", matchValue(10, getMatcher()).Must())
	assert.Equal(t, "c", matchValue(18, getMatcher()).Must())
	assert.Equal(t, "c", matchValue(20, getMatcher()).Must())
	assert.Error(t, matchValue(23, getMatcher()).Err())
	assert.Panics(t, func() { matchValue(23, getMatcher()).Must() })
}

func matchValue(a int, m match.Matcher[int, string]) fp.Result[string] {
	return m.Match(a)
}

func getMatcher() match.Match[int, string] {
	return match.New[int, string]().Add(
		match.NewBranch(
			func(i int) bool {
				return i <= 5
			},
			func(i int) fp.Result[string] {
				return fp.OK("a")
			},
		),
	).Add(
		match.NewBranch(
			func(i int) bool {
				return i <= 10
			},
			func(i int) fp.Result[string] {
				return fp.OK("b")
			},
		),
	).Add(
		match.NewBranch(
			func(i int) bool {
				return i <= 20
			},
			func(i int) fp.Result[string] {
				return fp.OK("c")
			},
		),
	)
}
