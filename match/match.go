package match

import (
	"errors"

	fp "gitlab.com/gofp/gofp"
)

// Matcher is the interface to implement a functional programming matching pattern.
type Matcher[T any, U any] interface {
	Match(T) fp.Result[U]
}

// Branch represents a single match branch for a matcher
type Branch[T any, U any] struct {
	pred func(T) bool
	op   func(T) fp.Result[U]
}

// NewBranch created a new branch for a matcher with a predicate that performs the matching, and an operation to execute
// when if the match is successful.
func NewBranch[T any, U any](pred func(T) bool, op func(T) fp.Result[U]) Branch[T, U] {
	return Branch[T, U]{
		pred: pred,
		op:   op,
	}
}

// Match is an implementation of the matcher interface
type Match[T any, U any] struct {
	branches []Branch[T, U]
}

// New creates a new Match instance
// You can add all or none of the branches of the matcher on instantiation
func New[T any, U any](branches ...Branch[T, U]) Match[T, U] {
	return Match[T, U]{
		branches: branches,
	}
}

// Add adds a new match branch to the matcher
func (m Match[T, U]) Add(branches ...Branch[T, U]) Match[T, U] {
	m.branches = append(m.branches, branches...)
	return m
}

// Match iterates over all the branches of the matcher and executes the operation of the first branch that matches the input
// The branches are checked in the order they were added
func (m Match[T, U]) Match(value T) fp.Result[U] {
	for _, branch := range m.branches {
		if branch.pred(value) {
			return branch.op(value)
		}
	}

	return ErrNoMatch[U]()
}

// ErrNoMatch returns a no match result of the given type
func ErrNoMatch[U any]() fp.Result[U] {
	return fp.Err[U](errors.New("no match"))
}
