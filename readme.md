# Go Functional Programming

## Description

Now that Generics are available with Go, it has become much easier to create functional programming constructs while maintaining type safety. This library is an experimental attempt at creating a usable functional programming library in Go that leverages Generics while attempting to learn a little bit more about Go Generics. This library has been inspired by my (admittedly limited) experience with [Scala](https://scala-lang.org) and [Rust](https://rust-lang.org).

> NOTE: This library is still in development and is subject to change. Also, I cannot make any guarantees about performance as it is not the primary focus of this library.
>
> This library is more of a thought exercise to see how we can use Go Generics to implement functional programming in Go. If it is of use to you, feel free to use it or fork it.
>
> I hope it can inspire you to make your own, and better library. If you do, please let me know I would love to use it!

## installation

To install this library, use go get from your project directly.

```bash
go get gitlab.com/gofp/gofp
```

## Documentation

### If

One of the annoying things about Go is that there is no ternary conditional expression. For example in C# we can use the `?:` statement to evaluate a boolean expression and return a one of two values depending on whether the expression evaluates to true or false.

Before Generics, we would need to create a separate `If` function for each type we need want to create a ternary conditional function for:

```go
func IfInt(condition bool, a, b int) int {
    if condition {
        return a
    }
    return b
}

func IfFloat64(condition bool, a, b float64) float64 {
    if condition {
        return a
    }
    return b
}

// Ad nauseum
```

With Generics, this is simplified to:

```go
func If[T any](condition bool, a, b T) T {
    if condition {
        return a
    }
    return b
}
```

### Option

Sometimes the zero value of a variable is a valid value and you need to be able to represent it in such a way that you can determine whether or not the variable has been set. Traditionally in Go we would create the variable as a pointer type then set the value to `nil` if the value hasn't been set, and not nil if it has. This in turn leads to a lot of additional checking to make sure the value is not nil, or face nil pointer panics when attempting to use the value before it has been set.

In other programming languages such as `Scala` and `Rust`, we have Optional data types that can return `Some` value, or `None`. Before the introduction of Generics in Go, to create an Optional type, we would need to define something for every type we needed to represent as an Optional value, for example `OptionalInt`, `OptionalFloat64`, `OptionalString` etc. [This optional library by Mark Phelps](https://github.com/markphelps/optional) is a great example of how to create Optional types in Go without Generics.

With Generics however, the definition of an Optional data type has become much simpler:

```go
type Option[T any] struct {
    value T
    none bool
}

func Some[T any](value T) Option[T] {
    return Option[T]{value: value}
}

func None[T any]() Option[T] {
    return Option[T]{none: true}
}
```

#### Option - Get

Get will return both the value and a boolean to indicate whether or not the value has been set

```go
val := gofp.Some(1)
v, ok := val.Get()      // v = 1, ok = true

noVal := gofp.None[int]()
n, ok := noVal.Get()    // n = 0, ok = false
```

#### Option - GetOrElse

GetOrElse will return the value if it has been set, or return the other value if it hasn't

```go
val := gofp.Some(1)
v := val.GetOrElse(2)      // v = 1

noVal := gofp.None[int]()
n := noVal.GetOrElse(2)    // n = 2
```

#### Option - IsDefined

IsDefined tells us if the value of the Option has been set.

```go
val := gofp.Some(1)
v := val.IsDefined()      // v = true

noVal := gofp.None[int]()
n := noVal.IsDefined()    // n = false
```

#### Option - Must

Must will return the value if it has been set, or panics if it hasn't

```go
val := gofp.Some(1)
v := val.Must()      // v = 1

noVal := gofp.None[int]()
n := noVal.Must()    // panics!!!
```

### Result

Idiomatic Go suggests that if a function can error then the function should return that error, and if the function needs to return a value, then both the value and the error is returned.

```go
func MyFunc(a int) (int, error) {
    // some operation that will return an int, but might also fail
}

func main() {
    // other initialisation...

    a, err := MyFunc(100)
    if err != nil {
        // handle error
    }

    // continue application
}
```

Unfortunately, this doesn't lend itself very well to chaining operations as one might in a functional programming way. In other languages we have a Result type that wraps the result of the function and any error it could throw. Once again, this has become simplified with the introduction of Generics in Go.

```go
type Result[T any] struct {
    value Option[T]
    err error
}

func OK[T any](value ...T) Result[T] {
    r := Result[T] { value: None[T]() }
    if len(value) > 0 {
        r.value = Some(value[0])
    }
    return r
}

func Err[T any](err error) Result[T] {
    return Result[T]{
        value: None[T](),
        err: err,
    }
}
```

You can see and example of how to use the Result in the [List and Result Example](#example-use-of-list-and-result) below.

### Ranger

Traditionally functional programming does have loops and uses recursion when it needs execute repetitive tasks or iterate a specific number of times like a `for` loop or `while` loop. While it's possible to do recursion in Go, we are limited to how many times we can recurse due to the size of the `stack`.

The ranger package provides a functional construct to iterate over a range of numbers. The ranger.Range has the following signature:

```go
func Range[T Number](start T, end T, step ...T) *ranger.ranger
```

You can initialize a range like this:

```go
r := ranger.Range(0, 10) // generates a range of 0 to 10 exclusive e.g. [0, 1, 2, 3 ...]
r2 := ranger.Range(0, 10, 2) // generates a range of 0 to 10 inclusive, with steps of 2, e.g. [0, 2, 4 ...]
```

From here we can use apply `Filter`, `Map`, `Fold`, `Collect` etc., to operate on a the range of numbers.

Range can be used to generate ranges for the following data types:

- `int`
- `int8`
- `int16`
- `int32`
- `int64`
- `uint`
- `uint8`
- `uint16`
- `uint32`
- `uint64`
- `float32`
- `float64`

To create a `while`-like loop, we can use `ranger.While`. The `ranger.While` has the following signature:

```go
func While[T Number](start T, end func(T) bool, step ...T) *ranger.whiler
```

#### Example

To calculate the sum of the first 10 natural numbers whose squares are divisible by 5

```go
sum := ranger.Range(1, 100).     // Range[int]
    Filter(func(a int) bool { return (a*a)%5 == 0 }, 10).   // List[int]
    Sum(fp.Add[int])    // int
```

### List and List2

A generic list structure that allow you to push to and pop from the head, or append to and take from the end. List2 is a variant of List that allows you to map the items in the list from one type to another. The methods are the same on both the List and List2 type, the only differences are the type definitions that are needed to define the lists.

For simplicity List and List2 are just generic type aliases for Go slices:

```go
type List[T any] []T
type List2[T any, U any] []T
```

Methods for these lists types do not change the original list, but return a new list instead, allowing us to treat them like immutable lists. For example:

```go
l := collection.NewList[int](1, 2, 3)
h, t := l.Pop() // h = 1, t = [2, 3]
fmt.Println(l) // [1, 2, 3] - l is not changed, t is a new list that has the head removed.
```

However, there's nothing to stop you from taking a reference to the list and modifying it directly.

#### List - NewList and NewList2

```go
intList := collection.NewList[int](1, 2, 3, 4, 5, 6)
stringList := collection.NewList[string]("a", "b", "c", "d", "e", "f")
timeList := collection.NewList[time.Time](time.Unix(1000, 0), time.Unix(2000, 0), time.Unix(3000, 0))
```

```go
// Create a list of ints that we are going to perform an operations on and we need to transform into a list of strings.
intList := collection.NewList2[int, string](1, 2, 3, 4, 5, 6)
// Create a list of strings that we will attempt to transform into a list of ints.
stringList := collection.NewList2[string, fp.Result[int]]("1", "2", "3", "4", "5", "6", "error")
// Create a list of structs that we will serialize to json
type MyStruct struct {
    Name string `json:"name"`
    Age int `json:"age"`
}
structList := collection.NewList2[MyStruct, []byte](MyStruct{Name: "John", Age: 30}, MyStruct{Name: "Jane", Age: 25})
```

#### List - Get

Given the List types are just type aliases for Go slices, it's possible to access items in the list using the index directly, e.g. `myList[index]`. This however is not very safe if the index value is out of bounds. Instead we can use the Get method, which will return the item as an Optional value at the index if it exists, or `None` if it doesn't.

```go
l := collection.NewList[int](1, 2, 3, 4, 5)
v := l.Get(2) // v = Some(3)
v = l.Get(6) // v = None
```

#### List - Push and Append

Push adds an item to the head of the list.

```go
l := collection.NewList[int](2, 3, 4, 5)
l = l.Push(1) // l = [1, 2, 3, 4, 5]
```

Append adds items to the end of the list.

```go
l := collection.NewList[int](1, 2, 3, 4)
l = l.Append(5) // l = [1, 2, 3, 4, 5]
```

#### List - Pop and PopN

Pop returns the head and tail of the list. The head of the list is wrapped in an option struct in case the list is empty, in which case the head value is None.

PopN allows you to pop the first N items from the head of the list and return both the items popped and the remaining items.

```go
l := collection.NewList[int](1, 2, 3, 4, 5)
head, tail := l.Pop()
fmt.Printf("Head: %v", head)    // Some(1)
fmt.Printf("Tail: %v", tail)    // [2, 3, 4, 5]

l2 := collection.NewList[int]()
head, tail := l2.Pop()
fmt.Printf("Head: %v", head)    // None
fmt.Printf("Tail: %v", tail)    // []

l3 := collection.NewList[int](1, 2, 3, 4, 5)
head, tail := l3.PopN(2)
fmt.Printf("Head: %v", head)    // [1, 2]
fmt.Printf("Tail: %v", tail)    // [3, 4, 5]
```

#### List - Take and TakeN

Take returns the value at the end of the list and a copy of the remaining items in the list. The value is wrapped in an option struct in case the list is empty, in which case the last value is None.

Like PopN, TakeN allows you to take the last N items from the end of the list and return both the items taken and the remaining items.

```go
l := collection.NewList[int](1, 2, 3, 4, 5)
last, remaining := l.Take()
fmt.Printf("Last: %v", last)    // Some(5)
fmt.Printf("Remaining: %v", remaining)    // [1, 2, 3, 4]

l2 := collection.NewList[int]()
last, remaining := l2.Take()
fmt.Printf("Head: %v", last)    // None
fmt.Printf("Tail: %v", remaining)    // []

l3 := collection.NewList[int](1, 2, 3, 4, 5)
last, remaining := l3.TakeN(2)
fmt.Printf("Last: %v", last)    // [4, 5]
fmt.Printf("Remaining: %v", remaining)    // [1, 2, 3]
```

#### List - Map

Map takes a predicate function that is applied to every item in the list and transforms it to a new list. For Lists, the predicate function takes the form `func(T) T`, while for Lists2 the predicate function takes the form `func(T) U`. The List2 type allows the items in the list to be transformed into the second type specified when creating a `List2[T, U]` object.

```go
l := collection.NewList[int](1, 2, 3, 4, 5)
l = l.Map(func(i int) int {
    return i * 2
} // [2, 4, 6, 8, 10]

l2 := collection.NewList2[int, string](1, 2, 3, 4, 5)
l2 = l2.Map(func(i int) string {
    return strconv.Itoa(i * 2)
} // ["2", "4", "6", "8", "10"]
```

#### List - Additional list operations available

Below is a table listing a sample of available list operations currently implemented.

| Function Name | Descrition | Signature |
| ------------- | ---------- | --------- |
| Head | Returns the value at the head of the list | `Head() fp.Option[T]` |
| Tail | Returns the tail of the list | `Tail() List[T]` |
| Last | Returns the value at the end of the list | `Last() fp.Option[T]` |
| ToString | Converts all elements in the list into string slice | `ToString() []string` |
| ToStream | Converts the list into a stream of data | `ToStream() chan T` |
| ToBufferedStream | Converts the list into a buffered stream of data | `ToBufferedStream(size int) chan T` |
| SplitAt | Splits the list into 2 separate lists at the given index | `SplitAt(int) (List[T], List[T])` |
| Partition | Splits the list into 2 separate lists that match a given predicate | `Partition(func(T) bool) (List[T], List[T])` |
| Filter | Filters the list to only include items that match a given predicate | `Filter(func(T) bool) List[T]` |
| FilterNot | Filters the list to only include items that do not match a given predicate | `FilterNot(func(T) bool) List[T]` |
| FoldLeft | Performs a left fold on the list | `FoldLeft(T, func(T, T) T) T` |
| FoldRight | Performs a right fold on the list | `FoldRight(T, func(T, T) T) T` |
| Slice | Returns a slice of the list between the given start and end (exclusive) indices | `Slice(int, int) List[T]` |
| Cap | Returns the current capacity of the list | `Cap() int` |
| Reverse | Reverses the order of the list | `Reverse() List[T]` |
| ForEach | Performs a given action on every element in the list | `ForEach(func(T))` |
| Sum | Given an addition predicate function, Sum will return the sum of all elements in the list | `Sum(func(T, T) T) fp.Option[T]` |
| Product | Given a multiplication predicate function, Product will return the product of all elements in the list | `Product(func(T, T) T) fp.Option[T]` |
| Min | Given a comparison predicate function, Min will return the minimum element in the list | `Min(func(T, T) bool) fp.Option[T]` |
| Max | Given a comparison predicate function, Max will return the maximum element in the list | `Max(func(T, T) bool) fp.Option[T]` |
| Contains | Given a predicate function, Contains will return true if the list contains an element that matches the predicate | `Contains(func(T) bool) bool` |
| IndexOf | Given a predicate function, IndexOf will return the index of the first element that matches the predicate | `IndexOf(func(T, T) bool) int` |
| LastIndexOf | Given a predicate function, LastIndexOf will return the index of the last element that matches the predicate | `LastIndexOf(func(T, T) bool) int` |
| Count | Given a predicate function, Count will return the number of elements that match the predicate | `Count(func(T) bool) int` |
| Sort | Given a comparison predicate function, Sort will sort the list in ascending order | `Sort(func(T, T) bool) List[T]` |

#### Example use of List and Result

```go
type ErrDivisionByZero struct {}
func (ErrDivisionByZero) Error() string {
    return "Division by zero"
}

func Divide(a, b int) gofp.Result[int] {
    if b == 0 {
        return gofp.Err[int](ErrDivisionByZero{})
    }
    return gofp.OK[int, ErrDivisionByZero](a / b)
}

type args struct {
    nom int
    denom int
}

func main() {
    // We have to use List2 because our Map function maps from one type to another
    // If we were mapping the same type we could simply use a List
    sum := collection.NewList2[args, fp.Result[int]](args{10, 2}, args{10, 0}, args{10, 1}).
        Map(func(args args) fp.Result[int] {
            return divide(args.nom, args.denom)
        }).
        Filter(func(r fp.Result[int]) bool {
            return r.HasValue()
        }).
        FoldLeft(fp.OK(0), func(a, b fp.Result[int]) fp.Result[int] {
            return fp.OK(a.Must() + b.Must())
        }).Must()


    fmt.Printf("sum = %d\n", sum) // sum = 15

    sum2 := collection.NewList2[args, fp.Result[int]](args{10, 2}, args{10, 0}, args{10, 1}).
        Map(func(args args) fp.Result[int] {
            return divide(args.nom, args.denom)
        }).
        FoldLeft(fp.OK(0), func(a, b fp.Result[int]) fp.Result[int] {
            return fp.OK(a.GetOrElse(0) + b.GetOrElse(0))
        }).Must()

    fmt.Printf("sum = %d\n", sum2) // sum = 15

    // If we were to only use List without mapping from args to fp.Result[int], you will see it gets
    // quite noisy and complicated.

    sum3 := collection.NewList(fp.OK(args{10, 2}), fp.OK(args{10, 0}), fp.OK(args{10, 1})).
        Map(func(x fp.Result[args]) fp.Result[args] {
            if x.GetOrElse(args{0, 0}).denom == 0 {
                return fp.Err[args](ErrDivideByZero{})
            }
            return fp.OK(args{x.Must().nom / x.Must().denom, 1})
        }).
        Filter(func(r fp.Result[args]) bool {
            return r.HasValue()
        }).
        FoldLeft(fp.OK(args{0, 1}), func(a, b fp.Result[args]) fp.Result[args] {
            return fp.OK(args{(a.Must().nom / a.Must().denom) + (b.Must().nom / b.Must().denom), 1})
        }).Must()

    fmt.Printf("sum = %d\n", sum3.nom/sum3.denom) // sum = 15
}
```

### ResultList

ResultList is a helper type alias for `List[fp.Result[T]]`. When performing list operations, you may encounter situations where the calculation can fail and want to wrap the output of the calculation in a `Result`. The ResultList provides two helper functions, `Collect` and `CollectErrors`.

#### Collect

Collect returns all valid results from a ResultList[T] into a List[T].

```go
func main() {
    var l = collection.NewResultList(fp.OK(1), fp.OK(2), fp.OK(3), fp.Err(errors.New("error")), fp.OK(4))
    var l2 = l.Collect()
    fmt.Printf("%v\n", l2) // [1, 2, 3, 4]
}
```

#### CollectErrors

CollectErrors returns all errors from a ResultList[T] into a List[error].

```go
func main() {
    var l = collection.NewResultList(fp.OK(1), fp.OK(2), fp.OK(3), fp.Err(errors.New("error")), fp.OK(4))
    var l2 = l.CollectErrors()
    fmt.Printf("%v\n", l2) // [error]
}
```

### Maps

Map is a generic type alias defined as `type Map[K comparable, V any] map[K]V`.

The type alias allows us to define functional methods on the type so that we can compose operations on the values the map contains in a functional way.

```go
    sum := collection.NewMap[string, float64]().
        Add("aaa", 1.0).
        Add("bbb", 2.0).
        Add("ccc", 3.0).
        MapValues(func(v float64) float64 {
            return v * 2
        }).Fold(0, func(acc float64, key string, value float64) float64 {
        return acc + value
    }, func(left, right string) bool {
        return left < right
    })
    fmt.Printf("%v\n", sum) // 12.0
```

In the example above, we created a new map with type `Map[string, float64]`, called the `Add(k K, v V) Map[K, V]` method, which adds a key-value pair to the map. We then called the `MapValues(func(V) V) Map[K, V]` method, which applies a function to all values in the map. Finally, we called the `Fold(V, func(V, K, V) V, func(K, K) bool) V` method, which folds the map into a single value.

If we had written this the Go way instead, it would have looked something like this:

```go
    myMap := make(map[string]float64)
    myMap["aaa"] = 1.0
    myMap["bbb"] = 2.0
    myMap["ccc"] = 3.0
    for k, v := range myMap {
        myMap[k] = v * 2
    }
    var sum float64
    for _, v := range myMap {
        sum += v
    }
    fmt.Printf("%v\n", sum) // 12.0
```

You'll have to decide for yourself which you prefer. As with any programming paradigm, there are arguments for and against either way. What becomes interesting is when we have a map of optional values that we need to map and aggregate as an example:

```go
    sum := collection.NewMap[string, fp.Option[float64]]().
        Add("aaa", fp.Some(1.0)).
        Add("bbb", fp.Some(2.0)).
        Add("ccc", fp.None[float64]()).
        Add("ddd", fp.Some(3.0)).
        Add("eee", fp.None[float64]()).
        Filter(func(_ string, result fp.Option[float64]) bool {
            return result.IsDefined()
        }).
        MapValues(func(v fp.Option[float64]) fp.Option[float64] {
            return fp.Some(v.Must() * 2)
        }).Fold(
            fp.Some[float64](0),
            func(acc fp.Option[float64], key string, value fp.Option[float64]) fp.Option[float64] {
                return fp.Some(acc.Must() + value.GetOrElse(0))
            },
            func(left, right string) bool {
                return left < right
            },
        ).Must()

    fmt.Printf("%v\n", sum) // 12.0
```

If we had to write this the Go way instead, it could look something like this:

```go
    myMap := make(map[string]*float64)
    v1 := 1.0
    v2 := 2.0
    v3 := 3.0

    myMap["aaa"] = &v1
    myMap["bbb"] = &v2
    myMap["ccc"] = nil
    myMap["ddd"] = &v3
    myMap["eee"] = nil

    var sum float64
    for k, v := range myMap {
        if v == nil {
            continue
        }
        sum += *v * 2
    }

    fmt.Printf("%v\n", sum) // 12.0
```

If we had performed an operation that could result in an error, this can be handled similarly:

```go
    type args struct {
        nom    int
        denom  int
        result int
    }

    errs := make([]error, 0)

    errDivisionByZero := errors.New("division by zero")

    divide := func(a args) fp.Result[args] {
        if a.denom == 0 {
            return fp.Err[args](errDivisionByZero)
        }

        return fp.OK(args{nom: a.nom, denom: a.denom, result: a.nom / a.denom})
    }

    // Instead of using Add, we pass the KV pairs directly to the NewMap constructor
    // While this method has been created for completeness, it's particularly verbose
    // and ugly, however if you have a function somewhere else that constructs a slice
    // of KV pairs, you can simply pass the results of that function to the `collection.NewMap`
    // constructor. E.g. `collection.NewMap(getKVPairs()...)`
    res := collection.NewMap(
        collection.KV[string, fp.Result[args]]{K: "aaa", V: fp.OK(args{nom: 10, denom: 2, result: 0})},
        collection.KV[string, fp.Result[args]]{K: "bbb", V: fp.OK(args{nom: 10, denom: 0, result: 0})},
        collection.KV[string, fp.Result[args]]{K: "notOK", V: fp.Err[args](errors.New("some error"))},
        collection.KV[string, fp.Result[args]]{K: "ccc", V: fp.OK(args{nom: 10, denom: 1, result: 0})},
    ).Filter(
        // first we filter out any bad inputs from our map so that our mapping function
        // doesn't need to handle bad inputs
        func(_ string, result fp.Result[args]) bool {
            return result.HasValue()
        },
    ).MapValues(func(v fp.Result[args]) fp.Result[args] {
        return divide(v.Must()) // We can use Must() here because we already filtered out errors
    }).ForEach(func(k string, v fp.Result[args]) {
        // Here we've defined a function that will handle each of the results that were errors
        // inline as a parameter to the `ForEach` function, but we could easily have passed a
        // predefined error handling function
        if v.IsErr() {
            errs = append(errs, v.Err())
        }
    }).Fold(
        fp.OK(args{result: 0}),
        func(acc fp.Result[args], key string, value fp.Result[args]) fp.Result[args] {
            if value.IsErr() {
                return acc
            }
            sum := acc.Must().result + value.Must().result
            return fp.OK(args{result: sum})
        },
        func(left, right string) bool {
            return left < right
        },
    ).Must() // res = args{0, 0, 15}
```

Using idomatic Go, we range over the map of results, and we have to nest multiple if statements to handle each error case. In our example, there are only two, but in more complex scenarios, it's easy to see how this can become more complicated, and how more complicated testing becomes as we need to test for each error branch.

```go
    // Now doing it the Go way
    errs = make([]error, 0)

    type args2 struct {
        nom   int
        denom int
        err   error
    }

    myMap := make(map[string]args2)

    myMap["aaa"] = args2{nom: 10, denom: 2}
    myMap["bbb"] = args2{nom: 10, denom: 0}
    myMap["notOK"] = args2{err: errors.New("some error")}
    myMap["ccc"] = args2{nom: 10, denom: 1}

    var sum int
    for _, v := range myMap {
        if v.err != nil {
            continue
        }

        // we cam use Must() because we already checked for error
        if v.denom == 0 {
            // this can cause a divide by zero error so we have two choices
            // - Skip it and lose the error
            // - Skip it and log the error
            // - Have our function report the error back up so it can be dealt with
            //   by the relevant party
            // - Panic because we're not expecting to encounter this error and
            //   something has gone wrong
            errs = append(errs, errDivisionByZero)
            continue
        }

        sum += (v.nom / v.denom)
    }

    assert.Equal(t, 15, sum)
```

Also there is also little opportunity for re-use of code with the code above. If we had to process the map several times to perform different operations on the same data, we would need to write multiple `for .. := range` loops with little re-use of code possible.

Our functional programming Map example above provides inline anonymous functions to demonstrate the use of the `Map[K, V]` methods, however, this makes our code look particularly verbose and difficult to understand. We can easily replace these anonymous functions with named functions to simplify our code.

```go
package main

import (
    "fmt"
    fp "gitlab.com/gofp/gofp"
)

func GetResults() []collection.KV[string, fp.Result[args]] {
    ...
}

func FilterBadResults(_ string, result fp.Result[args]) bool {
    ...
}

func Divide(a args) fp.Result[args] {
    ...
}

func MapValues(v fp.Result[args]) fp.Result[args] {
    ...
}

func HandleErrors(k string, v fp.Result[args]) {
    ...
}

func SumArgs(acc fp.Result[args], key string, value fp.Result[args]) fp.Result[args] {
    ...
}

func OrderString(left, right string) bool {
    ...
}

func main() {
    res := collection.NewMap(getResults()...).
        Filter(FilterBadInputs).
        MapValues(MapValues).
        ForEach(HandleErrors).
        Fold(fp.OK(args{result: 0}, SumArgs, OrderString).
        Must()

    fmt.Printf("Result: %d\n", res)
}
```

The body of the functions `filterBadInputs`, `mapValues`, `handleErrors`, etc. is not important for this example. They could be defined in another package or another library. They could have been written by ourselves, or written by others. They may be generic functions, or not. What is important is that it allow us to foster reuse of code and allow us to test each of their functionality separately to ensure correctness.

I think we can all agree that the above is easier to read and more concise.

### Queues and Sets

Other collection types included in this library are the Queue and Set collections. Sets allow us to create a generic collection of unique values, confined by type. Sets are defined as:

```go
type Set[K comparable] map[K]struct{}
```

Queue on the other hand is a generic struct that wraps a generic list and adds a capacity restriction. This allows us to create a generic queue that can be used to store a limited number of items. New items are pushed to the end of the queue until it is full. When the queue is full, the oldest items at the head of the queue are removed as new items are pushed on to it. Queue is defined as:

```go
type Queue[T any] struct {
    q List[T]
    cap int
}
```

Queue and Set have the same functional programming constructs such as `Map`, `Filter`, `ForEach` and `Fold`.

### Match

Another aspect of functional programming that is the `match` statement. While similar to `switch` is some ways, the `match` statement is much more powerful. While Go doesn't provide a match statement, with Generics we can implement our own.

```go
type Matcher[T any, U any] interface {
    Match(T) fp.Result[U]
}

type Branch[T any, U any] struct {
    pred func(T) bool
    op   func(T) fp.Result[U]
}

type Match[T any, U any] struct {
    branches []Branch[T, U]
}
```

#### Match Example

```go
matcher := match.New[int, string]().Add(
        match.NewBranch(
            func(i int) bool {
                return i <= 5
            },
            func(i int) fp.Result[string] {
                return fp.OK("a")
            },
        ),
    ).Add(
        match.NewBranch(
            func(i int) bool {
                return i <= 10
            },
            func(i int) fp.Result[string] {
                return fp.OK("b")
            },
        ),
    ).Add(
        match.NewBranch(
            func(i int) bool {
                return i <= 20
            },
            func(i int) fp.Result[string] {
                return fp.OK("c")
            },
        ),
    )

matched := matcher.Match(3).Must() // matched = "a"
matched := matcher.Match(5).Must() // matched = "a"
matched := matcher.Match(8).Must() // matched = "b"
matched := matcher.Match(10).Must() // matched = "b"
matched := matcher.Match(13).Must() // matched = "c"
matched := matcher.Match(20).Must() // matched = "c"
matched := matcher.Match(23).Must() // panics
```

The example above only matches a value and returns a value, however we could execute any function that returns a `Result[U]` value, meaning we can match a `T` value, execute an operation and return the result of that operation using the match construct defined above.

### Tuples

Go doesn't provide a tuple type, but we can easily implement them using Generics. The `tuple` package provides generic tuple types for tuples of 2 to 9 values.

```go
t2 := tuple.New2("some string", 42)
t9 := tuple.New9("some string", 42, true, false, 1.0, errors.New("some error"), time.Now(), "another string", 'X')


// We can access each element
t2e1 := t2.E1()
t2e3 := t2.E2()

t9e1 := t9.E1()
t9e2 := t9.E2()
t9e9 := t9.E9()

// Or we can unbind all elements from the tuple with Unbind()

a, b := t2.Unbind() // a = "some string", b = 42
```
