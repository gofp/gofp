package gofp

// Sort is an implementation of the quick sort algorithm written as a generic function
func Sort[T any](a []T, lt func(a, b T) bool) []T {
	b := make([]T, len(a))
	copy(b, a)
	sort(b, 0, len(b)-1, lt)
	return b
}

func sort[T any](a []T, lo, hi int, lt func(a, b T) bool) {
	if lo >= hi {
		return
	}

	p := partition(a, lo, hi, lt)
	sort(a, lo, p, lt)
	sort(a, p+1, hi, lt)
}

func partition[T any](a []T, lo, hi int, lt func(a, b T) bool) int {
	pivot := a[lo]
	i := lo + 1
	j := hi

	for {
		for i <= j && lt(a[i], pivot) {
			i++
		}
		for j >= i && !(lt(a[j], pivot)) {
			j--
		}
		if i >= j {
			break
		}
		a[i], a[j] = a[j], a[i]
		i++
		j--
	}
	a[lo], a[j] = a[j], a[lo]
	return j
}
