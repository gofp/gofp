package gofp_test

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"

	fp "gitlab.com/gofp/gofp"
)

func TestOptionInt(t *testing.T) {
	optInt := fp.Some(10)
	val, ok := optInt.Get()
	assert.True(t, ok)
	assert.Equal(t, 10, val)

	optNoInt := fp.None[int]()
	val, ok = optNoInt.Get()
	assert.False(t, ok)
	assert.Equal(t, 0, val)
}

func TestOptionFloat64(t *testing.T) {
	optFloat := fp.Some(10.0)
	val, ok := optFloat.Get()
	assert.True(t, ok)
	assert.Equal(t, 10.0, val)

	optNoFloat := fp.None[float64]()
	val, ok = optNoFloat.Get()
	assert.False(t, ok)
	assert.Equal(t, 0.0, val)
}

func TestOptionBool(t *testing.T) {
	optBool := fp.Some(true)
	val, ok := optBool.Get()
	assert.True(t, ok)
	assert.Equal(t, true, val)

	optNoBool := fp.None[bool]()
	val, ok = optNoBool.Get()
	assert.False(t, ok)
	assert.Equal(t, false, val)
}

func TestOptionTime(t *testing.T) {
	want := time.Date(2022, time.January, 1, 0, 0, 0, 0, time.UTC)
	optTime := fp.Some(want)
	val, ok := optTime.Get()
	assert.True(t, ok)
	assert.Equal(t, want, val)

	optNoTime := fp.None[time.Time]()
	val, ok = optNoTime.Get()
	assert.False(t, ok)
	want = time.Time{}
	assert.Equal(t, want, val)
}

func TestOptionMust(t *testing.T) {
	optInt := fp.Some(10)
	assert.NotPanics(t, func() {
		val := optInt.Must()
		assert.Equal(t, 10, val)
	})

	optNoInt := fp.None[int]()
	assert.Panics(t, func() {
		val := optNoInt.Must()
		assert.Equal(t, 0, val)
	})
}

func TestOptionStruct(t *testing.T) {
	type TestStruct struct {
		A int
		B string
	}

	optStruct := fp.Some(TestStruct{A: 10, B: "test"})
	val, ok := optStruct.Get()
	assert.True(t, ok)
	assert.Equal(t, TestStruct{A: 10, B: "test"}, val)

	optNoStruct := fp.None[TestStruct]()
	val, ok = optNoStruct.Get()
	assert.False(t, ok)
	assert.Equal(t, TestStruct{}, val)
}

func TestOptionIsDefined(t *testing.T) {
	optInt := fp.Some(10)
	assert.True(t, optInt.IsDefined())

	optNoInt := fp.None[int]()
	assert.False(t, optNoInt.IsDefined())
}

type testStruct struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func (t testStruct) String() string {
	bs, _ := json.Marshal(t)
	return string(bs)
}

func TestOptionString(t *testing.T) {
	type args struct {
		value any
	}

	testCases := []struct {
		name string
		args args
		want string
	}{
		{
			name: "fp.String() string value",
			args: args{
				value: "test",
			},
			want: "Some(test)",
		},
		{
			name: "fp.String() int value",
			args: args{
				value: 10,
			},
			want: "Some(10)",
		},
		{
			name: "fp.String() float64 value",
			args: args{
				value: 12.3,
			},
			want: "Some(12.3)",
		},
		{
			name: "fp.String() bool value",
			args: args{
				value: true,
			},
			want: "Some(true)",
		},
		{
			name: "fp.String() time.Time value",
			args: args{
				value: time.Unix(0, 0).UTC(),
			},
			want: "Some(1970-01-01 00:00:00 +0000 UTC)",
		},
		{
			name: "fp.String() decimal.Decimal value",
			args: args{
				value: decimal.NewFromFloat(123.45),
			},
			want: "Some(123.45)",
		},
		{
			name: "fp.String() testStruct value",
			args: args{
				value: testStruct{Name: "Joe Bloggs", Age: 88},
			},
			want: `Some({"name":"Joe Bloggs","age":88})`,
		},
		{
			name: "fp.String() none",
			args: args{
				value: nil,
			},
			want: "None",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(tt *testing.T) {
			if tc.args.value == nil {
				assert.Equal(tt, tc.want, fp.None[string]().String())
				return
			}
			assert.Equal(tt, tc.want, fp.Some(tc.args.value).String())
		})
	}
}

func TestOptionGetOrElse(t *testing.T) {
	type args struct {
		value any
		other any
	}

	testCases := []struct {
		name string
		args args
		want any
	}{
		{
			name: "GetOrElse returns other string value when value is none",
			args: args{
				value: nil,
				other: "other",
			},
			want: "other",
		},
		{
			name: "GetOrElse returns other int value when value is none",
			args: args{
				value: nil,
				other: 10,
			},
			want: 10,
		},
		{
			name: "GetOrElse returns other float value when value is none",
			args: args{
				value: nil,
				other: 10.5,
			},
			want: 10.5,
		},
		{
			name: "GetOrElse returns float value when value is not none",
			args: args{
				value: 10.9,
				other: 10.5,
			},
			want: 10.9,
		},
		{
			name: "GetOrElse returns string value when value is not none",
			args: args{
				value: "AAA",
				other: "BBB",
			},
			want: "AAA",
		},
		{
			name: "GetOrElse returns bool value when value is not none",
			args: args{
				value: true,
				other: false,
			},
			want: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(tt *testing.T) {
			if tc.args.value == nil {
				assert.Equal(tt, tc.want, fp.None[any]().GetOrElse(tc.args.other))
				return
			}
			assert.Equal(tt, tc.want, fp.Some(tc.args.value).GetOrElse(tc.args.other))
		})
	}
}
