package gofp_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	fp "gitlab.com/gofp/gofp"
)

func TestResult(t *testing.T) {
	t.Run("OK should create a result with no error", testOK)
	t.Run("Err should create a result with an error and no value", testErr)
	t.Run("Get should return an optional value", testGet)
	t.Run("GetOrElse should return the value if it exists, or other value if it doesn't", testGetOrElse)
	t.Run("HasValue should return true if the Result is not an error", testHasValue)
	t.Run("Result should return both the value and the error", testResult)
	t.Run("IsErr should return true if the result has an error", testIsErr)
	t.Run("Must should panic if the result is an error", testMust)
	t.Run("String should return a string representation of the result", testString)
}

func testOK(t *testing.T) {
	r := fp.OK("foo")
	assert.Equal(t, "foo", r.Must())
	assert.False(t, r.IsErr())

	none := fp.OK[string]()
	assert.False(t, none.HasValue())
	assert.Equal(t, fp.None[string](), none.Get())
	assert.False(t, none.IsErr())
}

func testErr(t *testing.T) {
	r := fp.Err[string](errors.New("some error"))
	assert.Error(t, r.Err())
	assert.False(t, r.HasValue())
}

func testGet(t *testing.T) {
	ok := fp.OK(1)
	assert.Equal(t, fp.Some(1), ok.Get())
	err := fp.Err[int](errors.New("some error"))
	assert.Equal(t, fp.None[int](), err.Get())
}

func testGetOrElse(t *testing.T) {
	ok := fp.OK(1)
	assert.Equal(t, 1, ok.GetOrElse(2))
	err := fp.Err[int](errors.New("some error"))
	assert.Equal(t, 2, err.GetOrElse(2))
}

func testHasValue(t *testing.T) {
	ok := fp.OK(1)
	assert.True(t, ok.HasValue())
	err := fp.Err[int](errors.New("some error"))
	assert.False(t, err.HasValue())
}

func testResult(t *testing.T) {
	ok := fp.OK(1)
	got, err := ok.Result()
	assert.Equal(t, fp.Some(1), got)
	assert.NoError(t, err)
	e := fp.Err[int](errors.New("some error"))
	got, err = e.Result()
	assert.Equal(t, fp.None[int](), got)
	assert.Error(t, err)
}

func testIsErr(t *testing.T) {
	ok := fp.OK(1)
	assert.False(t, ok.IsErr())
	err := fp.Err[int](errors.New("some error"))
	assert.True(t, err.IsErr())
}

func testMust(t *testing.T) {
	ok := fp.OK(1)
	assert.NotPanics(t, func() { ok.Must() })
	err := fp.Err[int](errors.New("some error"))
	assert.Panics(t, func() { err.Must() })
}

func testString(t *testing.T) {
	ok := fp.OK(1)
	assert.Equal(t, "1", ok.String())
	err := fp.Err[int](errors.New("some error"))
	assert.Equal(t, "some error", err.String())
	none := fp.OK[int]()
	assert.Equal(t, "None", none.String())
}
