package numbers_test

import (
	"fmt"
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gofp/gofp/collection"
	"gitlab.com/gofp/gofp/numbers"
)

func TestIsPrime(t *testing.T) {
	t.Run("IsPrime is true when the number is a prime number", func(t *testing.T) {
		collection.NewList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
			73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163).
			ForEach(func(v int) {
				assert.True(t, numbers.IsPrime(v))
			})
	})
	t.Run("IsPrime is false when the number is not a prime number", func(t *testing.T) {
		collection.NewList(1, 4, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 30,
			32, 33, 34, 35, 36, 38, 39, 40, 42, 44, 45, 46, 48, 49, 50, 51, 52, 54, 55, 56, 57, 58).
			ForEach(func(v int) {
				assert.False(t, numbers.IsPrime(v))
			})
	})
}

func TestPrimes(t *testing.T) {
	t.Run("Primes should generate prime numbers between the start and end values (inclusive)", func(t *testing.T) {
		primes := numbers.Primes(1, 11)
		assert.Equal(t, []int{2, 3, 5, 7, 11}, primes)
		primes = numbers.Primes(1, 11, 3)
		assert.Equal(t, []int{2, 3, 5}, primes)
		primes = numbers.Primes(100, 150, 8)
		assert.Equal(t, []int{101, 103, 107, 109, 113, 127, 131, 137}, primes)
	})
}

func BenchmarkIsPrime(b *testing.B) {
	testCases := []struct {
		name  string
		input int
	}{
		{
			name:  "Benchmark 100",
			input: 100,
		},
		{
			name:  "Benchmark 123",
			input: 123,
		},
		{
			name:  "Benchmark 5334",
			input: 5334,
		},
		{
			name:  "Benchmark 72345",
			input: 72345,
		},
		{
			name:  "Benchmark 493456",
			input: 493456,
		},
		{
			name:  "Benchmark 654721",
			input: 654721,
		},
		{
			name:  "Benchmark 54367",
			input: 54367,
		},
		{
			name:  "Benchmark 9572839",
			input: 9572839,
		},
	}
	for _, tc := range testCases {
		b.Run(tc.name, func(b *testing.B) {
			for n := 0; n < b.N; n++ {
				numbers.IsPrime(tc.input)
			}
		})
	}
}

func BenchmarkPrimes(b *testing.B) {
	testCases := []struct {
		name  string
		input int
	}{
		{
			name:  "Benchmark 100",
			input: 100,
		},
		{
			name:  "Benchmark 123",
			input: 123,
		},
		{
			name:  "Benchmark 5334",
			input: 5334,
		},
		{
			name:  "Benchmark 54367",
			input: 54367,
		},
		{
			name:  "Benchmark 72345",
			input: 72345,
		},
		{
			name:  "Benchmark 493456",
			input: 493456,
		},
		{
			name:  "Benchmark 654721",
			input: 654721,
		},
		{
			name:  "Benchmark 9572839",
			input: 9572839,
		},
	}

	for _, tc := range testCases {
		b.Run(fmt.Sprintf("%s-Functional", tc.name), func(bb *testing.B) {
			b.Logf("N = %d", bb.N)
			for n := 0; n < bb.N; n++ {
				numbers.Primes(1, n)
			}
		})

		b.Run(fmt.Sprintf("%s-Imperative", tc.name), func(bb *testing.B) {
			b.Logf("N = %d", bb.N)
			for n := 0; n < bb.N; n++ {
				primeNumbers(tc.input)
			}
		})
	}
}

func primeNumbers(max int) []int {
	var primes []int

	for i := 2; i < max; i++ {
		isPrime := true

		for j := 2; j <= int(math.Sqrt(float64(i))); j++ {
			if i%j == 0 {
				isPrime = false
				break
			}
		}

		if isPrime {
			primes = append(primes, i)
		}
	}

	return primes
}
