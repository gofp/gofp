package numbers_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gofp/gofp/numbers"
)

//nolint:funlen
func TestFibN(t *testing.T) {
	type args struct {
		n int
	}

	testCases := []struct {
		name string
		args args
		want uint64
	}{
		{
			name: "FibN(0) should return 0",
			args: args{
				n: 0,
			},
			want: 0,
		},
		{
			name: "FibN(1) should return 1",
			args: args{
				n: 1,
			},
			want: 1,
		},
		{
			name: "FibN(2) should return 1",
			args: args{
				n: 2,
			},
			want: 1,
		},
		{
			name: "FibN(3) should return 2",
			args: args{
				n: 3,
			},
			want: 2,
		},
		{
			name: "FibN(4) should return 3",
			args: args{
				n: 4,
			},
			want: 3,
		},
		{
			name: "FibN(5) should return 5",
			args: args{
				n: 5,
			},
			want: 5,
		},
		{
			name: "FibN(6) should return 8",
			args: args{
				n: 6,
			},
			want: 8,
		},
		{
			name: "FibN(7) should return 13",
			args: args{
				n: 7,
			},
			want: 13,
		},
		{
			name: "FibN(8) should return 21",
			args: args{
				n: 8,
			},
			want: 21,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			got := numbers.FibN(tt.args.n)
			assert.Equal(t, tt.want, got)
		})
	}
}

//nolint:funlen
func TestFibs(t *testing.T) {
	type args struct {
		n int
	}

	testCases := []struct {
		name string
		args args
		want []uint64
	}{
		{
			name: "Fibs(0) should return []",
			args: args{
				n: 0,
			},
			want: []uint64{},
		},
		{
			name: "Fibs(1) should return [1]",
			args: args{
				n: 1,
			},
			want: []uint64{1},
		},
		{
			name: "Fibs(2) should return [1, 1]",
			args: args{
				n: 2,
			},
			want: []uint64{1, 1},
		},
		{
			name: "Fibs(3) should return [1, 1, 2]",
			args: args{
				n: 3,
			},
			want: []uint64{1, 1, 2},
		},
		{
			name: "Fibs(4) should return [1, 1, 2, 3]",
			args: args{
				n: 4,
			},
			want: []uint64{1, 1, 2, 3},
		},
		{
			name: "Fibs(5) should return [1, 1, 2, 3, 5]",
			args: args{
				n: 5,
			},
			want: []uint64{1, 1, 2, 3, 5},
		},
		{
			name: "Fibs(6) should return [1, 1, 2, 3, 5, 8]",
			args: args{
				n: 6,
			},
			want: []uint64{1, 1, 2, 3, 5, 8},
		},
		{
			name: "Fibs(7) should return [1, 1, 2, 3, 5, 8, 13]",
			args: args{
				n: 7,
			},
			want: []uint64{1, 1, 2, 3, 5, 8, 13},
		},
		{
			name: "Fibs(8) should return [1, 1, 2, 3, 5, 8, 13, 31]",
			args: args{
				n: 8,
			},
			want: []uint64{1, 1, 2, 3, 5, 8, 13, 21},
		},
		{
			name: "Fibs(9) should return [1, 1, 2, 3, 5, 8, 13, 21, 34]",
			args: args{
				n: 9,
			},
			want: []uint64{1, 1, 2, 3, 5, 8, 13, 21, 34},
		},
	}

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			got := numbers.Fibs(tt.args.n)
			assert.Equal(t, tt.want, got)
		})
	}
}

func BenchmarkFibs(b *testing.B) {
	for n := 0; n < b.N; n++ {
		numbers.Fibs(n)
	}
}

func BenchmarkFibN(b *testing.B) {
	for n := 0; n < b.N; n++ {
		numbers.FibN(n)
	}
}
