package numbers

// FibN returns the nth Fibonacci number
//
//nolint:mnd
func FibN(n int) uint64 {
	var a, b uint64 = 0, 1
	for i := 0; i < n; i++ {
		a, b = b, a+b
		// in case of integer overflow
		if a > b {
			break
		}
	}

	return a
}

// Fibs returns a slice of the first n numbers in the Fibonacci sequence
func Fibs(n int) []uint64 {
	fibs := make([]uint64, n)

	for i := 1; i <= n; i++ {
		fibs[i-1] = FibN(i)
	}

	return fibs
}
