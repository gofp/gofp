package numbers

import (
	"math"

	"gitlab.com/gofp/gofp/ranger"
)

const (
	firstPrime = 2
)

// IsPrime evaluates a number and returns true if it is a prime
func IsPrime(i int) bool {
	if i < firstPrime {
		return false
	}

	last := int(math.Sqrt(float64(i)))

	for j := firstPrime; j <= last; j++ {
		if i%j == 0 && j != i {
			return false
		}
	}

	return true
}

// Primes will generate a list of prime numbers
func Primes(start, end int, count ...int) []int {
	var max int

	if len(count) > 0 {
		max = count[0]
	}

	primes := ranger.Range(start, end+1).
		Filter(IsPrime, max)

	return primes
}
