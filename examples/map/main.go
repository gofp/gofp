package main

import (
	"errors"
	"fmt"

	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/collection"
)

type args struct {
	nom    int
	denom  int
	result int
}

var errDivisionByZero = errors.New("division by zero")
var errs []error

func GetResults() []collection.KV[string, fp.Result[args]] {
	return []collection.KV[string, fp.Result[args]]{
		{K: "aaa", V: fp.OK(args{nom: 10, denom: 2, result: 0})},
		{K: "bbb", V: fp.OK(args{nom: 10, denom: 0, result: 0})},
		{K: "notOK", V: fp.Err[args](errors.New("some error"))},
		{K: "ccc", V: fp.OK(args{nom: 10, denom: 1, result: 0})},
	}
}

func FilterBadResults(_ string, result fp.Result[args]) bool {
	return result.HasValue()
}

func Divide(a args) fp.Result[args] {
	if a.denom == 0 {
		return fp.Err[args](errDivisionByZero)
	}

	return fp.OK(args{nom: a.nom, denom: a.denom, result: a.nom / a.denom})
}

func MapValues(v fp.Result[args]) fp.Result[args] {
	if v.IsErr() {
		return v
	}
	return Divide(v.Must())
}

func HandleErrors(k string, v fp.Result[args]) {
	if v.IsErr() {
		errs = append(errs, v.Err())
	}
}

func SumArgs(acc fp.Result[args], key string, value fp.Result[args]) fp.Result[args] {
	if value.IsErr() {
		return acc
	}
	sum := acc.Must().result + value.Must().result
	return fp.OK(args{result: sum})
}

func OrderString(left, right string) bool {
	return left < right
}

func main() {
	res := collection.NewMap(GetResults()...).
		Filter(FilterBadResults).
		MapValues(MapValues).
		ForEach(HandleErrors).
		Fold(fp.OK(args{result: 0}), SumArgs, OrderString).
		Must()

	fmt.Printf("%d\n", res.result)
}
