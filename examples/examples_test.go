package examples

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/collection"
	"gitlab.com/gofp/gofp/numbers"
	"gitlab.com/gofp/gofp/ranger"
)

func TestFP(t *testing.T) {
	t.Run("Find the sum of the square of the first 5 prime numbers", testSumOfPrimes)
	t.Run("A function that returns a result", testDivide)
	t.Run("Using maps", testMapType)
}

func testSumOfPrimes(t *testing.T) {
	sumOfPrimes := ranger.Range(1, 100).
		Filter(numbers.IsPrime, 5).
		Map(func(i int) int {
			return i * i
		}).
		FoldLeft(0, fp.Add[int])

	assert.Equal(t, 208, sumOfPrimes)

	square := func(i int) int { return i * i }

	sumOfPrimes = collection.NewList(numbers.Primes(1, 100, 5)...).
		Map(square).
		FoldLeft(0, fp.Add[int])
	assert.Equal(t, 208, sumOfPrimes)
}

type ErrDivideByZero struct{}

func (ErrDivideByZero) Error() string {
	return "division by zero"
}

func divide(a, b int) fp.Result[int] {
	if b == 0 {
		return fp.Err[int](ErrDivideByZero{})
	}
	return fp.OK(a / b)
}

func testDivide(t *testing.T) {
	result := divide(10, 0)
	assert.Equal(t, fp.Err[int](ErrDivideByZero{}), result)

	result = divide(10, 2)
	assert.Equal(t, fp.OK(5), result)

	type args struct {
		nom   int
		denom int
	}

	t.Run("Divide a list of numerator and denominator pairs and aggregate the totals using List", func(t *testing.T) {
		r := collection.NewList(fp.OK(args{10, 2}), fp.OK(args{10, 0}), fp.OK(args{10, 1})).
			Map(func(x fp.Result[args]) fp.Result[args] {
				if x.GetOrElse(args{0, 0}).denom == 0 {
					return fp.Err[args](ErrDivideByZero{})
				}
				return fp.OK(args{x.Must().nom / x.Must().denom, 1})
			}).
			Filter(func(r fp.Result[args]) bool {
				return r.HasValue()
			}, 0).
			FoldLeft(fp.OK(args{0, 1}), func(a, b fp.Result[args]) fp.Result[args] {
				return fp.OK(args{(a.Must().nom / a.Must().denom) + (b.Must().nom / b.Must().denom), 1})
			}).Must()

		assert.Equal(t, 15, r.nom/r.denom)
	})

	t.Run("Divide a list of numerator and denominator pairs and aggregate the totals using List2.Map.Filter.FoldLeft", func(t *testing.T) {
		r := collection.NewList2[args, fp.Result[int]](args{10, 2}, args{10, 0}, args{10, 1}).
			Map(func(args args) fp.Result[int] {
				return divide(args.nom, args.denom)
			}).
			Filter(func(r fp.Result[int]) bool {
				return r.HasValue()
			}, 0).
			FoldLeft(fp.OK(0), func(a, b fp.Result[int]) fp.Result[int] {
				return fp.OK(a.Must() + b.Must())
			}).Must()

		assert.Equal(t, 15, r)
	})

	t.Run("Divide a list of numerator and denominator pairs and aggregate the totals using List2.Map.FoldLeft", func(t *testing.T) {
		r := collection.NewList2[args, fp.Result[int]](args{10, 2}, args{10, 0}, args{10, 1}).
			Map(func(args args) fp.Result[int] {
				return divide(args.nom, args.denom)
			}).
			FoldLeft(fp.OK(0), func(a, b fp.Result[int]) fp.Result[int] {
				return fp.OK(a.GetOrElse(0) + b.GetOrElse(0))
			}).Must()

		assert.Equal(t, 15, r)
	})

	t.Run("Divide a list of numerator and denominator pairs and aggregate the totals by mapping a list of ints to Results then collect and fold", func(t *testing.T) {
		r := collection.NewResultList(
			collection.NewList2[args, fp.Result[int]](
				args{10, 2},
				args{10, 0},
				args{10, 1},
			).Map(func(args args) fp.Result[int] {
				return divide(args.nom, args.denom)
			})...,
		).
			Collect().
			FoldLeft(0, func(a, b int) int {
				return a + b
			})

		assert.Equal(t, 15, r)
	})

	t.Run("Divide a list of numerator and denominator pairs and aggregate the totals by filtering a list of ints, mapping to Results then collect and fold", func(t *testing.T) {
		r := collection.NewResultList(collection.NewList2[args, fp.Result[int]](args{10, 2}, args{10, 0}, args{10, 1}).
			Filter(func(a args) bool {
				return a.denom != 0
			}, 0).Map(func(args args) fp.Result[int] {
			return divide(args.nom, args.denom)
		})...).Collect().FoldLeft(0, fp.Add[int])

		assert.Equal(t, 15, r)
	})
}

func testMapType(t *testing.T) {
	t.Run("The Map type can chain operations to generate a result", func(t *testing.T) {
		res := collection.NewMap[string, float64]().
			Add("aaa", 1.0).
			Add("bbb", 2.0).
			Add("ccc", 3.0).
			MapValues(func(v float64) float64 {
				return v * 2
			}).Fold(0, func(acc float64, key string, value float64) float64 {
			return acc + value
		}, func(left, right string) bool {
			return left < right
		})

		assert.Equal(t, 12.0, res)
	})

	t.Run("The Map type can chain operations on a map of Option values", func(t *testing.T) {
		res := collection.NewMap[string, fp.Option[float64]]().
			Add("aaa", fp.Some(1.0)).
			Add("bbb", fp.Some(2.0)).
			Add("ccc", fp.None[float64]()).
			Add("ddd", fp.Some(3.0)).
			Add("eee", fp.None[float64]()).
			Filter(func(_ string, result fp.Option[float64]) bool {
				return result.IsDefined()
			}).
			MapValues(func(v fp.Option[float64]) fp.Option[float64] {
				return fp.Some(v.Must() * 2)
			}).Fold(fp.Some[float64](0), func(acc fp.Option[float64], key string, value fp.Option[float64]) fp.Option[float64] {
			return fp.Some(acc.Must() + value.GetOrElse(0))
		}, func(left, right string) bool {
			return left < right
		}).Must()

		assert.Equal(t, 12.0, res)

		// Doing it the Go way
		myMap := make(map[string]*float64)
		v1 := 1.0
		v2 := 2.0
		v3 := 3.0

		myMap["aaa"] = &v1
		myMap["bbb"] = &v2
		myMap["ccc"] = nil
		myMap["ddd"] = &v3
		myMap["eee"] = nil

		var sum float64
		for _, v := range myMap {
			if v == nil {
				continue
			}
			sum += *v * 2
		}

		assert.Equal(t, 12.0, sum)
	})

	t.Run("The Map type can chain operations on a map of Result values", func(t *testing.T) {
		type args struct {
			nom    int
			denom  int
			result int
		}

		errs := make([]error, 0)

		errDivisionByZero := errors.New("division by zero")

		divide := func(a args) fp.Result[args] {
			if a.denom == 0 {
				return fp.Err[args](errDivisionByZero)
			}

			return fp.OK(args{nom: a.nom, denom: a.denom, result: a.nom / a.denom})
		}

		// Instead of using Add, we pass the KV pairs directly to the NewMap constructor
		// While this method has been created for completeness, it's particularly verbose
		// and ugly, however if you have a function somewhere else that constructs a slice
		// of KV pairs, you can simply pass the results of that function to the `collection.NewMap`
		// constructor. E.g. `collection.NewMap(getKVPairs()...)`
		res := collection.NewMap(
			collection.KV[string, fp.Result[args]]{K: "aaa", V: fp.OK(args{nom: 10, denom: 2, result: 0})},
			collection.KV[string, fp.Result[args]]{K: "bbb", V: fp.OK(args{nom: 10, denom: 0, result: 0})},
			collection.KV[string, fp.Result[args]]{K: "notOK", V: fp.Err[args](errors.New("some error"))},
			collection.KV[string, fp.Result[args]]{K: "ccc", V: fp.OK(args{nom: 10, denom: 1, result: 0})},
		).Filter(
			// first we filter out any bad inputs from our map so that our mapping function
			// doesn't need to handle bad inputs
			func(_ string, result fp.Result[args]) bool {
				return result.HasValue()
			},
		).MapValues(func(v fp.Result[args]) fp.Result[args] {
			return divide(v.Must()) // We can use Must() here because we already filtered out errors
		}).ForEach(func(k string, v fp.Result[args]) {
			// Here we've defined a function that will handle each of the results that were errors
			// inline as a parameter to the `ForEach` function, but we could easily have passed a
			// predefined error handling function
			if v.IsErr() {
				errs = append(errs, v.Err())
			}
		}).Fold(
			fp.OK(args{result: 0}),
			func(acc fp.Result[args], key string, value fp.Result[args]) fp.Result[args] {
				if value.IsErr() {
					return acc
				}
				sum := acc.Must().result + value.Must().result
				return fp.OK(args{result: sum})
			},
			func(left, right string) bool {
				return left < right
			},
		).Must() // res = args{0, 0, 15}

		assert.Equal(t, 15, res.result)
		assert.Equal(t, []error{errDivisionByZero}, errs)

		// Now doing it the Go way
		errs = make([]error, 0)

		type args2 struct {
			nom   int
			denom int
			err   error
		}

		myMap := make(map[string]args2)

		myMap["aaa"] = args2{nom: 10, denom: 2}
		myMap["bbb"] = args2{nom: 10, denom: 0}
		myMap["notOK"] = args2{err: errors.New("some error")}
		myMap["ccc"] = args2{nom: 10, denom: 1}

		var sum int
		for _, v := range myMap {
			if v.err != nil {
				continue
			}

			// we cam use Must() because we already checked for error
			if v.denom == 0 {
				// this can cause a divide by zero error so we have two choices
				// - Skip it and lose the error
				// - Skip it and log the error
				// - Have our function report the error back up so it can be dealt with
				//   by the relevant party
				// - Panic because we're not expecting to encounter this error and
				//   something has gone wrong
				errs = append(errs, errDivisionByZero)
				continue
			}

			sum += (v.nom / v.denom)
		}

		assert.Equal(t, 15, sum)
	})
}
