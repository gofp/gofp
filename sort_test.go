package gofp_test

import (
	"testing"
	"time"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	fp "gitlab.com/gofp/gofp"
)

func TestQuickSort(t *testing.T) {
	t.Run("Sort ints", func(t *testing.T) {
		vals := []int{5, 4, 3, 2, 1}
		got := fp.Sort(vals, func(i, j int) bool {
			return i < j
		})
		assert.Equal(t, []int{1, 2, 3, 4, 5}, got)
		assert.Equal(t, []int{5, 4, 3, 2, 1}, vals)
	})
	t.Run("Sort int8s", func(t *testing.T) {
		vals := []int8{5, 4, 3, 2, 1}
		got := fp.Sort(vals, func(i, j int8) bool {
			return i < j
		})
		assert.Equal(t, []int8{1, 2, 3, 4, 5}, got)
		assert.Equal(t, []int8{5, 4, 3, 2, 1}, vals)
	})
	t.Run("Sort float32", func(t *testing.T) {
		vals := []float32{5, 4, 3, 2, 1}
		got := fp.Sort(vals, func(i, j float32) bool {
			return i < j
		})
		assert.Equal(t, []float32{1, 2, 3, 4, 5}, got)
		assert.Equal(t, []float32{5, 4, 3, 2, 1}, vals)
	})
	t.Run("Sort float64", func(t *testing.T) {
		vals := []float64{5, 4, 3, 2, 1}
		got := fp.Sort(vals, func(i, j float64) bool {
			return i < j
		})
		assert.Equal(t, []float64{1, 2, 3, 4, 5}, got)
		assert.Equal(t, []float64{5, 4, 3, 2, 1}, vals)
	})
	t.Run("Sort string", func(t *testing.T) {
		vals := []string{"5", "4", "3", "2", "1"}
		got := fp.Sort(vals, func(i, j string) bool {
			return i < j
		})
		assert.Equal(t, []string{"1", "2", "3", "4", "5"}, got)
		assert.Equal(t, []string{"5", "4", "3", "2", "1"}, vals)
	})
	t.Run("Sort ints even number of elements", func(t *testing.T) {
		vals := []int{6, 5, 4, 3, 2, 1}
		got := fp.Sort(vals, func(i, j int) bool {
			return i < j
		})
		assert.Equal(t, []int{1, 2, 3, 4, 5, 6}, got)
		assert.Equal(t, []int{6, 5, 4, 3, 2, 1}, vals)
	})
}

func TestDecimal(t *testing.T) {
	type args struct {
		v []decimal.Decimal
	}
	tests := []struct {
		name string
		args args
		want []decimal.Decimal
	}{
		{
			name: "sort values",
			args: args{
				v: []decimal.Decimal{
					decimal.NewFromInt(10),
					decimal.NewFromInt(7),
					decimal.NewFromInt(8),
					decimal.NewFromInt(9),
					decimal.NewFromInt(1),
					decimal.NewFromInt(5),
				},
			},
			want: []decimal.Decimal{
				decimal.NewFromInt(1),
				decimal.NewFromInt(5),
				decimal.NewFromInt(7),
				decimal.NewFromInt(8),
				decimal.NewFromInt(9),
				decimal.NewFromInt(10),
			},
		},
		{
			name: "empty list",
			args: args{
				v: nil,
			},
			want: nil,
		},
		{
			name: "fractional values",
			args: args{
				v: []decimal.Decimal{
					decimal.NewFromFloat(1.0001),
					decimal.NewFromFloat(1.00001),
					decimal.NewFromFloat(1.0000000001),
					decimal.NewFromFloat(1.00000001),
					decimal.NewFromFloat(1.0000000000001),
					decimal.NewFromFloat(1.0000001),
				},
			},
			want: []decimal.Decimal{
				decimal.NewFromFloat(1.0000000000001),
				decimal.NewFromFloat(1.0000000001),
				decimal.NewFromFloat(1.00000001),
				decimal.NewFromFloat(1.0000001),
				decimal.NewFromFloat(1.00001),
				decimal.NewFromFloat(1.0001),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := fp.Sort(tt.args.v, func(a, b decimal.Decimal) bool {
				return a.LessThan(b)
			})
			for i, v := range got {
				assert.True(t, v.Equal(tt.want[i]), "Expected %v, got %v", tt.want[i], v)
			}
		})
	}
}

func TestDates(t *testing.T) {
	type args struct {
		v []time.Time
	}
	tests := []struct {
		name string
		args args
		want []time.Time
	}{
		{
			name: "Dates",
			args: args{
				v: []time.Time{
					time.Unix(500, 0),
					time.Unix(400, 0),
					time.Unix(300, 0),
					time.Unix(200, 0),
					time.Unix(100, 0),
				},
			},
			want: []time.Time{
				time.Unix(100, 0),
				time.Unix(200, 0),
				time.Unix(300, 0),
				time.Unix(400, 0),
				time.Unix(500, 0),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := fp.Sort(tt.args.v, func(a, b time.Time) bool {
				return a.Before(b)
			})
			for i, v := range got {
				assert.True(t, v.Equal(tt.want[i]), "Expected %v, got %v", tt.want[i], v)
			}
		})
	}
}
