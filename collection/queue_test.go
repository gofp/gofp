package collection_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/collection"
)

func TestNewQueue(t *testing.T) {
	t.Run("NewQueue creates an empty queue with the given capacity", func(t *testing.T) {
		q := collection.NewQueue[int](10)
		assert.Equal(t, 0, q.Size())
		assert.Equal(t, 10, q.Cap())
	})
	t.Run("NewQueue creates a queue populated with the values provided", func(t *testing.T) {
		q := collection.NewQueue(3, 1, 2)
		assert.Equal(t, 2, q.Size())
		assert.Equal(t, 3, q.Cap())
		assert.Equal(t, 1, q.Head().Must())
		assert.Equal(t, 2, q.Last().Must())
	})
	t.Run("NewQueue creates a queue populated with the values provided when more values than capacity", func(t *testing.T) {
		q := collection.NewQueue(3, 1, 2, 3, 4)
		assert.Equal(t, 3, q.Size())
		assert.Equal(t, 3, q.Cap())
		assert.Equal(t, 2, q.Head().Must())
		assert.Equal(t, 4, q.Last().Must())
	})
}

func TestQueue_Head(t *testing.T) {
	t.Run("Head should return fp.None[T] if the queue is empty", func(t *testing.T) {
		q := collection.NewQueue[int](10)
		assert.Equal(t, fp.None[int](), q.Head())
	})

	t.Run("Head should return fp.Some[T] if the queue is not empty", func(t *testing.T) {
		q := collection.NewQueue[int](10)
		q = q.Push(1)
		assert.Equal(t, fp.Some(1), q.Head())
	})
}

func TestQueue_Last(t *testing.T) {
	t.Run("Last should return fp.None[T] if the queue is empty", func(t *testing.T) {
		q := collection.NewQueue[int](10)
		assert.Equal(t, fp.None[int](), q.Last())
	})
	t.Run("Last should return fp.Some[T] if the queue is non-empty", func(t *testing.T) {
		q := collection.NewQueue(10, 1, 2, 3)
		assert.Equal(t, fp.Some(3), q.Last())
	})
}

func TestQueue_ToList(t *testing.T) {
	t.Run("ToList returns the queue as a list", func(t *testing.T) {
		q := collection.NewQueue(3, 1, 2, 3)
		assert.Equal(t, collection.NewList(1, 2, 3), q.ToList())
	})
}

func TestQueue_ToSlice(t *testing.T) {
	t.Run("ToSlice returns the queue as a slice of T", func(t *testing.T) {
		q := collection.NewQueue(3, 1, 2, 3)
		assert.Equal(t, []int{1, 2, 3}, q.ToSlice())
	})
}

func TestQueue_Push(t *testing.T) {
	t.Run("Push should add values to the end of the queue", func(t *testing.T) {
		q := collection.NewQueue[int](3)
		q = q.Push(1)
		assert.Equal(t, 1, q.Size())
		assert.Equal(t, 3, q.Cap())
		assert.Equal(t, 1, q.Head().Must())
		assert.Equal(t, 1, q.Last().Must())

		q = q.Push(2)
		assert.Equal(t, 2, q.Size())
		assert.Equal(t, 3, q.Cap())
		assert.Equal(t, 1, q.Head().Must())
		assert.Equal(t, 2, q.Last().Must())

		q = q.Push(3)
		assert.Equal(t, 3, q.Size())
		assert.Equal(t, 3, q.Cap())
		assert.Equal(t, 1, q.Head().Must())
		assert.Equal(t, 3, q.Last().Must())
	})
	t.Run("Push should remove old items if the capacity is reached", func(t *testing.T) {
		q := collection.NewQueue(3, 1, 2, 3)
		q = q.Push(4)
		assert.Equal(t, 3, q.Size())
		assert.Equal(t, 3, q.Cap())
		assert.Equal(t, 2, q.Head().Must())
		assert.Equal(t, 4, q.Last().Must())
	})
}

func TestQueue_Pop(t *testing.T) {
	t.Run("Pop should remove items from the head of the queue", func(t *testing.T) {
		q := collection.NewQueue(3, 1, 2, 3)
		got, q := q.Pop()
		assert.Equal(t, fp.Some(1), got)
		assert.Equal(t, collection.NewQueue(3, 2, 3), q)
		got, q = q.Pop()
		assert.Equal(t, fp.Some(2), got)
		assert.Equal(t, collection.NewQueue(3, 3), q)
		got, q = q.Pop()
		assert.Equal(t, fp.Some(3), got)
		assert.Equal(t, collection.NewQueue[int](3), q)
	})
	t.Run("Pop should return fp.None[T] when the queue is empty", func(t *testing.T) {
		q := collection.NewQueue[int](3)
		got, q := q.Pop()
		assert.Equal(t, fp.None[int](), got)
		assert.Equal(t, collection.NewQueue[int](3), q)
	})
}

func TestQueue_Take(t *testing.T) {
	t.Run("Take should remove items from the end of the queue", func(t *testing.T) {
		q := collection.NewQueue(3, 1, 2, 3)
		got, q := q.Take()
		assert.Equal(t, fp.Some(3), got)
		assert.Equal(t, collection.NewQueue(3, 1, 2), q)
		got, q = q.Take()
		assert.Equal(t, fp.Some(2), got)
		assert.Equal(t, collection.NewQueue(3, 1), q)
		got, q = q.Take()
		assert.Equal(t, fp.Some(1), got)
		assert.Equal(t, collection.NewQueue[int](3), q)
	})
	t.Run("Take should return fp.None[T] when the queue is empty", func(t *testing.T) {
		q := collection.NewQueue[int](3)
		got, q := q.Take()
		assert.Equal(t, fp.None[int](), got)
		assert.Equal(t, collection.NewQueue[int](3), q)
	})
}

func TestQueue_Reverse(t *testing.T) {
	t.Run("Reverse should reverse the contents of the queue", func(t *testing.T) {
		q := collection.NewQueue(3, 1, 2, 3)
		assert.Equal(t, collection.NewQueue(3, 3, 2, 1), q.Reverse())
	})
}

func TestQueue_Filter(t *testing.T) {
	t.Run("Filter returns a queue with only members that satify the predicate in the order they were received", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		assert.Equal(t, collection.NewQueue(9, 2, 4, 6, 8), q.Filter(func(i int) bool { return i%2 == 0 }, 0))
	})
}

func TestQueue_FilterNot(t *testing.T) {
	t.Run("FilterNot returns a queue with only members that does not satisfy the predicate in the order they were received", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		assert.Equal(t, collection.NewQueue(9, 1, 3, 5, 7, 9), q.FilterNot(func(i int) bool { return i%2 == 0 }, 0))
	})
}

func TestQueue_FoldLeft(t *testing.T) {
	t.Run("FoldLeft should apply the predicate to each member of the queue, left to right and return the accumulated value", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		assert.Equal(t, 45, q.FoldLeft(0, func(acc, i int) int { return acc + i }))
		qs := collection.NewQueue(9, "a", "b", "c")
		assert.Equal(t, "abc", qs.FoldLeft("", func(acc, i string) string { return acc + i }))
	})
}

func TestQueue_FoldRight(t *testing.T) {
	t.Run("FoldRight should apply the predicate to each member of the queue, right to left and return the accumulated value", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		assert.Equal(t, 45, q.FoldRight(0, func(acc, i int) int { return acc + i }))
		qs := collection.NewQueue(9, "a", "b", "c")
		assert.Equal(t, "cba", qs.FoldRight("", func(acc, i string) string { return acc + i }))
	})
}

func TestQueue_ToStream(t *testing.T) {
	t.Run("ToStream should return a stream with the same contents as the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		s := q.ToStream()
		got := make([]int, 0, q.Size())
		for v := range s {
			got = append(got, v)
		}
		want := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
		assert.Equal(t, want, got)
	})
}

func TestQueue_ToBufferedStream(t *testing.T) {
	t.Run("ToBufferedStream should return a stream with the same contents as the queue when the buffer is smaller than the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		s := q.ToBufferedStream(5)
		got := make([]int, 0, q.Size())
		for v := range s {
			got = append(got, v)
		}
		want := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
		assert.Equal(t, want, got)
	})
	t.Run("ToBufferedStream should return a stream with the same contents as the queue when the buffer is larger than the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		s := q.ToBufferedStream(15)
		got := make([]int, 0, q.Size())
		for v := range s {
			got = append(got, v)
		}
		want := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
		assert.Equal(t, want, got)
	})
}

func TestQueue_IsEmpty(t *testing.T) {
	t.Run("IsEmpty returns true if the queue is empty", func(t *testing.T) {
		q := collection.NewQueue[int](9)
		assert.True(t, q.IsEmpty())
	})
	t.Run("IsEmpty returns false if the queue is not empty", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3)
		assert.False(t, q.IsEmpty())
	})
}

func TestQueue_NonEmpty(t *testing.T) {
	t.Run("NonEmpty returns false if the queue is empty", func(t *testing.T) {
		q := collection.NewQueue[int](9)
		assert.False(t, q.NonEmpty())
	})
	t.Run("NonEmpty returns true if the queue is not empty", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3)
		assert.True(t, q.NonEmpty())
	})
}

func TestQueue_Size(t *testing.T) {
	t.Run("Size returns the number of elements in the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3)
		assert.Equal(t, 3, q.Size())
	})
}

func TestQueue_Cap(t *testing.T) {
	t.Run("Cap returns the capacity of the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3)
		assert.Equal(t, 9, q.Cap())
	})
}

func TestQueue_SplitAt(t *testing.T) {
	t.Run("SplitAt returns 2 queue split at the given index and the index is less than the length", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		left, right := q.SplitAt(3)
		assert.Equal(t, collection.NewQueue(9, 1, 2, 3), left)
		assert.Equal(t, collection.NewQueue(9, 4, 5, 6, 7, 8, 9), right)
	})
	t.Run("SplitAt returns the queue and an empty queue when the given index is greater than the length", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		left, right := q.SplitAt(10)
		assert.Equal(t, collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9), left)
		assert.Equal(t, collection.NewQueue[int](9), right)
	})
	t.Run("SplitAt returns the queue and an empty queue when the given index is less than 0", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		left, right := q.SplitAt(-1)
		assert.Equal(t, collection.NewQueue[int](9), left)
		assert.Equal(t, collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9), right)
	})
}

func TestQueue_Partition(t *testing.T) {
	t.Run("Partition returns 2 queues with matching and non matching items", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		left, right := q.Partition(func(i int) bool { return i%2 == 0 })
		assert.Equal(t, collection.NewQueue(9, 2, 4, 6, 8), left)
		assert.Equal(t, collection.NewQueue(9, 1, 3, 5, 7, 9), right)
	})
}

func TestQueue_Slice(t *testing.T) {
	t.Run("Slice returns the given subset of values bounded by the given indexes (exclusive)", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		assert.Equal(t, collection.NewQueue(9, 4, 5, 6, 7), q.Slice(3, 7))
	})
	t.Run("Slice returns the given subset of values bounded using negative indexes (inclusive)", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		assert.Equal(t, collection.NewQueue(9, 5, 6, 7, 8, 9), q.Slice(-5, -1))
	})
}

func TestQueue_ToString(t *testing.T) {
	t.Run("ToString returns the string representation of the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		assert.Equal(t, []string{"1", "2", "3", "4", "5", "6", "7", "8", "9"}, q.ToStrings())
	})
}

func TestQueue_Map(t *testing.T) {
	t.Run("Map returns a new queue with the results of applying the given function to each element", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		assert.Equal(t, collection.NewQueue(9, 2, 4, 6, 8, 10, 12, 14, 16, 18), q.Map(func(i int) int { return i * 2 }))
	})
}

func TestQueue_ForEach(t *testing.T) {
	t.Run("ForEach executes the given function for every value in the Queue", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		var want []int
		q.ForEach(func(i int) { want = append(want, i+1) })
		assert.Equal(t, []int{2, 3, 4, 5, 6, 7, 8, 9, 10}, want)
	})
}

func TestQueue_Sum(t *testing.T) {
	t.Run("Sum provides the sum of the contents of the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		assert.Equal(t, fp.Some(45), q.Sum(fp.Add[int]))
	})
	t.Run("Sum returns fp.None[T] if the queue is empty", func(t *testing.T) {
		q := collection.NewQueue[int](9)
		assert.Equal(t, fp.None[int](), q.Sum(fp.Add[int]))
	})
}

func TestQueue_Product(t *testing.T) {
	t.Run("Product provides the product of the contents of the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		assert.Equal(t, fp.Some(362880), q.Product(fp.Multiply[int]))
	})
	t.Run("Product returns fp.None[T] if the queue is empty", func(t *testing.T) {
		q := collection.NewQueue[int](9)
		assert.Equal(t, fp.None[int](), q.Product(fp.Multiply[int]))
	})
}

func TestQueue_Min(t *testing.T) {
	t.Run("Min returns the smallest value in the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 3, 4, 2, 6, 7, 5, 8, 9)
		assert.Equal(t, fp.Some(2), q.Min(fp.LT[int]))
	})
	t.Run("Min returns fp.None[T] if the queue is empty", func(t *testing.T) {
		q := collection.NewQueue[int](9)
		assert.Equal(t, fp.None[int](), q.Min(fp.LT[int]))
	})
}

func TestQueue_Max(t *testing.T) {
	t.Run("Max returns the largest value in the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 3, 4, 2, 6, 7, 5, 8, 9)
		assert.Equal(t, fp.Some(9), q.Max(fp.GT[int]))
	})
	t.Run("Max returns fp.None[T] if the queue is empty", func(t *testing.T) {
		q := collection.NewQueue[int](9)
		assert.Equal(t, fp.None[int](), q.Max(fp.GT[int]))
	})
}

func TestQueue_Contains(t *testing.T) {
	t.Run("Contains returns true if the item exists in the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 3, 4, 2, 6, 7, 5, 8, 9)
		assert.True(t, q.Contains(7, fp.EQ[int]))
	})
	t.Run("Contains returns false if the item does not exist in the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 3, 4, 2, 6, 7, 5, 8, 9)
		assert.False(t, q.Contains(1, fp.EQ[int]))
	})
}

func TestQueue_IndexOf(t *testing.T) {
	t.Run("IndexOf returns the index of the queue if the item exists", func(t *testing.T) {
		q := collection.NewQueue(9, 3, 4, 2, 6, 7, 5, 8, 9, 2)
		assert.Equal(t, 2, q.IndexOf(2, fp.EQ[int]))
	})
	t.Run("IndexOf returns -1 if the item does not exists", func(t *testing.T) {
		q := collection.NewQueue(9, 3, 4, 2, 6, 7, 5, 8, 9, 2)
		assert.Equal(t, -1, q.IndexOf(1, fp.EQ[int]))
	})
}

func TestQueue_LastIndexOf(t *testing.T) {
	t.Run("LastIndexOf returns the last index of the queue where the item occurs", func(t *testing.T) {
		q := collection.NewQueue(9, 3, 4, 2, 6, 7, 5, 8, 9, 2)
		assert.Equal(t, 8, q.LastIndexOf(2, fp.EQ[int]))
	})
	t.Run("LastIndexOf returns -1 if item does not occur in the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 3, 4, 2, 6, 7, 5, 8, 9, 2)
		assert.Equal(t, -1, q.LastIndexOf(1, fp.EQ[int]))
	})
}

func TestQueue_Count(t *testing.T) {
	t.Run("Count returns the number of times the given predicate passes when applied to all the items in the queue", func(t *testing.T) {
		q := collection.NewQueue(9, 3, 4, 2, 6, 7, 5, 8, 9, 2)
		assert.Equal(t, 2, q.Count(func(a int) bool { return a == 2 }))
	})
}

func TestQueue_Tail(t *testing.T) {
	t.Run("Tail should return an empty queue if the queue is empty", func(t *testing.T) {
		q := collection.NewQueue[int](9)
		assert.Equal(t, collection.NewQueue[int](9), q.Tail())
	})
	t.Run("Tail should return an empty queue if the queue only has 1 element", func(t *testing.T) {
		q := collection.NewQueue(9, 1)
		assert.Equal(t, collection.NewQueue[int](9), q.Tail())
	})
	t.Run("Tail should return all the elements except the head", func(t *testing.T) {
		q := collection.NewQueue(9, 1, 2, 3, 4, 5, 6, 7, 8, 9)
		got := q.Tail()
		assert.Equal(t, collection.NewQueue(9, 2, 3, 4, 5, 6, 7, 8, 9), got)
		got = got.Tail()
		assert.Equal(t, collection.NewQueue(9, 3, 4, 5, 6, 7, 8, 9), got)
		got = got.Tail()
		assert.Equal(t, collection.NewQueue(9, 4, 5, 6, 7, 8, 9), got)
		got = got.Tail()
		assert.Equal(t, collection.NewQueue(9, 5, 6, 7, 8, 9), got)
	})
}
