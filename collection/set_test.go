package collection_test

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/collection"
)

func TestSet_NewSet(t *testing.T) {
	t.Run("NewSet should return an empty set if passed no values", func(t *testing.T) {
		s := collection.NewSet[int]()
		assert.Equal(t, 0, len(s))
	})

	t.Run("NewSet should return a set with the given values", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.Equal(t, 3, len(s))
		want := []int{1, 2, 3}
		for _, v := range want {
			assert.Contains(t, s, v)
		}
	})
}

func TestSet_Add(t *testing.T) {
	t.Run("Add should add all new values to the set", func(t *testing.T) {
		s := collection.NewSet[int]()
		s.Add(1, 2, 3)
		assert.Equal(t, 3, len(s))
		want := []int{1, 2, 3}
		for _, v := range want {
			assert.Contains(t, s, v)
		}
	})

	t.Run("Add should ignore values if they already exist in the set", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.Equal(t, 3, len(s))
		s.Add(1, 2, 4, 5)
		assert.Equal(t, 5, len(s))
		want := []int{1, 2, 3, 4, 5}
		for _, v := range want {
			assert.Contains(t, s, v)
		}
	})
}

func TestSet_Drop(t *testing.T) {
	t.Run("Drop should remove all given values from the set", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.Equal(t, 3, len(s))
		s.Drop(1, 3)
		assert.Equal(t, 1, len(s))
		want := []int{2}
		for _, v := range want {
			assert.Contains(t, s, v)
		}
	})

	t.Run("Drop should ignore values that do not exist in the set", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.Equal(t, 3, len(s))
		s.Drop(4)
		assert.Equal(t, 3, len(s))
		want := []int{1, 2, 3}
		for _, v := range want {
			assert.Contains(t, s, v)
		}
	})
}

func TestSet_Contains(t *testing.T) {
	t.Run("Contains returns true if the set contains the given value", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.True(t, s.Contains(1))
		assert.True(t, s.Contains(2))
		assert.True(t, s.Contains(3))
	})

	t.Run("Contains returns false if the set does not contain the given value", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.False(t, s.Contains(4))
	})
}

func TestSet_ContainsAll(t *testing.T) {
	t.Run("ContainsAll returns true if all given values exist in the set", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.True(t, s.ContainsAll(1, 2, 3))
	})

	t.Run("ContainsAll returns false if any of the given values do not exist in the set", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.False(t, s.ContainsAll(1, 2, 4))
	})
}

func TestSet_Map(t *testing.T) {
	t.Run("Map should return a new set with the given values transformed using the given mapping function", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		m := s.Map(func(v int) int {
			return v * 2
		})
		assert.Equal(t, 3, len(m))
		want := []int{2, 4, 6}
		for _, v := range want {
			assert.Contains(t, m, v)
		}
	})
}

func TestSet_ToSlice(t *testing.T) {
	t.Run("ToSlice should return a slice of the values in the set", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		got := s.ToSlice()
		assert.Equal(t, 3, len(got))
		want := []int{1, 2, 3}
		assert.ElementsMatch(t, want, got)
	})
	t.Run("ToSlice should order the slice ", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		got := s.ToSlice(fp.LT[int])
		assert.Equal(t, 3, len(got))
		want := []int{1, 2, 3}
		assert.Equal(t, want, got)
	})
}

func TestSet_Fold(t *testing.T) {
	t.Run("Fold should fold the set using the given initial value and the given function", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		got := s.Fold(0, func(acc, v int) int {
			return acc + v
		}, func(a, b int) bool {
			return a < b
		})
		assert.Equal(t, 6, got)
	})
	t.Run("Fold should fold the set using the given initial vlaue and the given function in the order specified", func(t *testing.T) {
		s := collection.NewSet("1", "2", "3")
		got := s.Fold("", func(acc, v string) string {
			return acc + v
		}, func(a, b string) bool {
			return a < b
		})
		assert.Equal(t, "123", got)

		got = s.Fold("", func(acc, v string) string {
			return acc + v
		}, func(a, b string) bool {
			return a > b
		})
		assert.Equal(t, "321", got)
	})
}

func TestSet_Filter(t *testing.T) {
	t.Run("Filter should return a new set with only the values that pass the given predicate", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		m := s.Filter(func(v int) bool {
			return v%2 == 0
		})
		assert.Equal(t, 1, len(m))
		want := []int{2}
		for _, v := range want {
			assert.Contains(t, m, v)
		}
	})
}

func TestSet_FilterNot(t *testing.T) {
	t.Run("FilterNot should return a new set with only the values that do not pass the given predicate", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		m := s.FilterNot(func(v int) bool {
			return v%2 == 0
		})
		assert.Equal(t, 2, len(m))
		want := []int{1, 3}
		for _, v := range want {
			assert.Contains(t, m, v)
		}
	})
}

func TestSet_Count(t *testing.T) {
	t.Run("Count should return the number of items in the set that match the given predicate", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.Equal(t, 1, s.Count(func(i int) bool {
			return i == 2
		}))
	})
}

func TestSet_Exists(t *testing.T) {
	t.Run("Exists should return true if any of the items in a set match the given predicate", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.True(t, s.Exists(func(i int) bool {
			return i == 2
		}))
	})
	t.Run("Exists should return false if any of the items in a set match the given predicate", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.False(t, s.Exists(func(i int) bool {
			return i == 4
		}))
	})
}

func TestSet_Partition(t *testing.T) {
	t.Run("Partition should split a set and return one set that matches the given predicate and one that doesn't", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		m, n := s.Partition(func(i int) bool {
			return i == 2
		})
		assert.Equal(t, 1, len(m))
		assert.Equal(t, 2, len(n))
		want := []int{2}
		for _, v := range want {
			assert.Contains(t, m, v)
		}
		want = []int{1, 3}
		for _, v := range want {
			assert.Contains(t, n, v)
		}
	})
}

func TestSet_IsEmpty(t *testing.T) {
	t.Run("IsEmpty should return true if the set is empty", func(t *testing.T) {
		s := collection.NewSet[int]()
		assert.True(t, s.IsEmpty())
	})
	t.Run("IsEmpty should return false if the set is not empty", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.False(t, s.IsEmpty())
	})
}

func TestSet_NonEmpty(t *testing.T) {
	t.Run("NonEmpty should return true if the set is not empty", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.True(t, s.NonEmpty())
	})
	t.Run("NonEmpty should return false if the set is empty", func(t *testing.T) {
		s := collection.NewSet[int]()
		assert.False(t, s.NonEmpty())
	})
}

func TestSet_Size(t *testing.T) {
	t.Run("Size should return the number of items in the set", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.Equal(t, 3, s.Size())
	})
}

func TestSet_ToStream(t *testing.T) {
	t.Run("ToStream should return a stream of values from the set", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		st := s.ToStream()
		got := make([]int, 0, len(s))
		for v := range st {
			got = append(got, v)
		}
		want := []int{1, 2, 3}
		assert.ElementsMatch(t, want, got)
	})

	t.Run("ToStream should return a stream of values from the set in the given order", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		st := s.ToStream(fp.GT[int])
		got := make([]int, 0, len(s))
		for v := range st {
			got = append(got, v)
		}
		want := []int{3, 2, 1}
		assert.Equal(t, want, got)
	})
}

func TestSet_ToBufferedStream(t *testing.T) {
	t.Run("ToBufferedStream should return a stream of values from the set", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		st := s.ToBufferedStream(2)
		got := make([]int, 0, len(s))
		for v := range st {
			got = append(got, v)
		}
		want := []int{1, 2, 3}
		assert.ElementsMatch(t, want, got)
	})

	t.Run("ToBufferedStream should return a stream of values from the set in the given order", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		st := s.ToBufferedStream(5, fp.GT[int])
		got := make([]int, 0, len(s))
		for v := range st {
			got = append(got, v)
		}
		want := []int{3, 2, 1}
		assert.Equal(t, want, got)
	})
}

func TestSet_ForEach(t *testing.T) {
	t.Run("ForEach should call the given function for each item in the set", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		var got []int
		s.ForEach(func(i int) {
			got = append(got, i)
		})
		want := []int{1, 2, 3}
		assert.ElementsMatch(t, want, got)
	})
	t.Run("ForEach should call the given function for each item in the set in the order specified by the order function", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		var got []int
		s.ForEach(func(i int) {
			got = append(got, i)
		}, fp.GT[int])
		want := []int{3, 2, 1}
		assert.Equal(t, want, got)
	})
}

func TestSet_String(t *testing.T) {
	t.Run("String should return a string representation of the set", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		assert.Equal(t, "Set{1, 2, 3}", s.String(fp.LT[int]))
	})
}

func TestMapSet(t *testing.T) {
	t.Run("MapSet should transformm the contents of the given set into a set of other data type", func(t *testing.T) {
		s := collection.NewSet(1, 2, 3)
		got := collection.MapSet(s, func(i int) string {
			return strconv.Itoa(i)
		})
		assert.Equal(t, 3, len(got))
		want := collection.Set[string]{"1": struct{}{}, "2": struct{}{}, "3": struct{}{}}
		for k := range got {
			_, ok := want[k]
			assert.True(t, ok)
		}
	})
}

func TestSet_Union(t *testing.T) {
	t.Run("Union no other sets returns the original set", func(t *testing.T) {
		a := collection.NewSet(1, 2, 3)
		want := collection.NewSet(1, 2, 3)
		got := a.Union()
		assert.Equal(t, want, got)
	})

	t.Run("Union one other set returns the union of both sets", func(t *testing.T) {
		a := collection.NewSet(1, 2, 3)
		b := collection.NewSet(3, 4, 5)
		want := collection.NewSet(1, 2, 3, 4, 5)
		got := a.Union(b)
		assert.Equal(t, want, got)
	})

	t.Run("Union multiple other sets returns the union of all sets", func(t *testing.T) {
		a := collection.NewSet(1, 2, 3)
		b := collection.NewSet(3, 4, 5)
		c := collection.NewSet(5, 6, 7)
		want := collection.NewSet(1, 2, 3, 4, 5, 6, 7)
		got := a.Union(b, c)
		assert.Equal(t, want, got)
	})
}

func TestSet_Intersection(t *testing.T) {
	t.Run("Intersection no other sets returns the original set", func(t *testing.T) {
		a := collection.NewSet(1, 2, 3)
		want := collection.NewSet(1, 2, 3)
		got := a.Intersection()
		assert.Equal(t, want, got)
	})

	t.Run("Intersection one other set returns the intersection of both sets", func(t *testing.T) {
		a := collection.NewSet(1, 2, 3)
		b := collection.NewSet(2, 3, 4)
		want := collection.NewSet(2, 3)
		got := a.Intersection(b)
		assert.Equal(t, want, got)
	})

	t.Run("Intersection multiple other sets returns the intersection of all sets", func(t *testing.T) {
		a := collection.NewSet(1, 2, 3)
		b := collection.NewSet(2, 3, 4)
		c := collection.NewSet(3, 4, 5)
		want := collection.NewSet(3)
		got := a.Intersection(b, c)
		assert.Equal(t, want, got)
	})
}

func TestSet_Difference(t *testing.T) {
	t.Run("Difference returns the values in set A that are not in set B", func(t *testing.T) {
		a := collection.NewSet(1, 2, 3)
		b := collection.NewSet(2, 3, 4)
		got := a.Difference(b)
		want := collection.NewSet(1)
		assert.Equal(t, want, got)
	})
}

func TestSet_Complement(t *testing.T) {
	t.Run("Complement returns the values in set B that are not in set A", func(t *testing.T) {
		a := collection.NewSet(1, 2, 3)
		b := collection.NewSet(2, 3, 4)
		got := a.Complement(b)
		want := collection.NewSet(4)
		assert.Equal(t, want, got)
	})
}
