package collection

import fp "gitlab.com/gofp/gofp"

// List is a generic list that implements the Traversable interface
// but also allows the list to be mapped to a different type.
// T is the originating data type for the list
// U is the resulting data type for the list when the Map function is applied.
// You can map the list and keep the originating data type and type signature
// of the list by using the MapT function until you need to map your data to the
// resulting data type.
type List2[T any, U any] List[T]

// NewList2 creates a new functional list with type specified
func NewList2[T any, U any](values ...T) List2[T, U] {
	if values == nil {
		return make(List2[T, U], 0)
	}
	return List2[T, U](NewList(values...))
}

// Push adds values to the head of the list
func (l List2[T, U]) Push(v ...T) List2[T, U] {
	return append(v, l...)
}

// Append adds a value to the end of the list
func (l List2[T, U]) Append(v ...T) List2[T, U] {
	return append(l, v...)
}

// Pop returns the value at the head of the list and the remaining values
func (l List2[T, U]) Pop() (fp.Option[T], List2[T, U]) {
	return fp.Pop(l)
}

// PopN returns the n values at the head of the list and the remaining values
func (l List2[T, U]) PopN(n int) (List2[T, U], List2[T, U]) {
	return fp.PopN(l, n)
}

// Take returns the value at the end of the list and the remaining values
func (l List2[T, U]) Take() (fp.Option[T], List2[T, U]) {
	return fp.Take(l)
}

// TakeN returns the n values at the end of the list and the remaining values at the head of the list
func (l List2[T, U]) TakeN(n int) (List2[T, U], List2[T, U]) {
	return fp.TakeN(l, n)
}

// Map returns a new list with the values mapped over, the predicate provided should map the values of the
// first declared type to the second declared type for the List2 instance.
func (l List2[T, U]) Map(pred func(T) U) List2[U, U] {
	return fp.Map(l, pred)
}

// MapT returns a new list with the values mapped over, the predicate provided should map the values of the
func (l List2[T, U]) MapT(pred func(T) T) List2[T, U] {
	return fp.Map(l, pred)
}

// Filter returns a new list with the values that match the given predicate
func (l List2[T, U]) Filter(pred func(T) bool, max int) List2[T, U] {
	return fp.Filter(l, pred, max)
}

// FilterNot returns a list with the values that don't match the given predicate
func (l List2[T, U]) FilterNot(pred func(T) bool, max int) List2[T, U] {
	return fp.FilterNot(l, pred, max)
}

// FoldLeft returns the result of folding the list from the start of the list
func (l List2[T, U]) FoldLeft(initial T, fn func(T, T) T) T {
	return fp.FoldLeft(initial, l, fn)
}

// FoldRight returns the result of folding the list from the end of the list
func (l List2[T, U]) FoldRight(initial T, fn func(T, T) T) T {
	return fp.FoldRight(initial, l, fn)
}

// Head returns the optional value at the head of the list. If the list is empty,
// the value is None
func (l List2[T, U]) Head() fp.Option[T] {
	if len(l) <= 0 {
		return fp.None[T]()
	}
	return fp.Some(l[0])
}

// Tail returns the list without the head value
func (l List2[T, U]) Tail() List2[T, U] {
	if len(l) <= 0 {
		return l
	}
	return l[1:]
}

// Last returns the last element of the list
func (l List2[T, U]) Last() fp.Option[T] {
	if len(l) <= 0 {
		return fp.None[T]()
	}

	return fp.Some(l[len(l)-1])
}

// ToString converts every element to a string and returns the slice of strings
func (l List2[T, U]) ToStrings() []string {
	return fp.ToStrings(l)
}

// SplitAt splits the list at the desired index
func (l List2[T, U]) SplitAt(index int) (List2[T, U], List2[T, U]) {
	return fp.SplitAt(l, index)
}

// Partition splits the list into 2 lists that contains either elements that match the
// given predicate, or elements that don't match the predicate
func (l List2[T, U]) Partition(pred func(T) bool) (List2[T, U], List2[T, U]) {
	return fp.Partition(l, pred)
}

// Slice returns a slice of the list between the given start and end values.
// If negative values are given, the indices will be calculated from the end
// of the list.
func (l List2[T, U]) Slice(start, end int) List2[T, U] {
	// If start or end is negative, it is relative to the end of the list
	if start < 0 {
		start = len(l) + start
	}
	if end < 0 {
		end = len(l)
	}

	if start >= len(l) || start > end {
		return NewList2[T, U]()
	}

	if end >= len(l) {
		end = len(l)
	}

	ll := make(List2[T, U], end-start)
	copy(ll, l[start:end])

	return ll
}

// IsEmpty returns true if the list is empty
func (l List2[T, U]) IsEmpty() bool {
	return len(l) == 0
}

// NonEmpty returns true if the list has values
func (l List2[T, U]) NonEmpty() bool {
	return len(l) > 0
}

// Size returns the number of items in the list
func (l List2[T, U]) Size() int {
	return len(l)
}

// Cap returns the current capacity of the list
func (l List2[T, U]) Cap() int { return cap(l) }

// Reverse returns the list in reverse order
func (l List2[T, U]) Reverse() List2[T, U] {
	return fp.Reverse(l)
}

// ToStream returns a stream of the list
func (l List2[T, U]) ToStream() chan T {
	return fp.ToStream(l)
}

// ToBufferedStream returns a buffered stream of the file
func (l List2[T, U]) ToBufferedStream(size int) chan T {
	return fp.ToBufferedStream(l, size)
}

// ForEach executes the given function for each element in the list
func (l List2[T, U]) ForEach(fn func(T)) List2[T, U] {
	return fp.ForEach(l, fn)
}

// Sum returns the sum of the list using the given function to add the values necessary to provide the sum
func (l List2[T, U]) Sum(add func(T, T) T) fp.Option[T] {
	return fp.Sum(l, add)
}

// Product returns the product of the list using the given function to multiply the values necessary to provide the product
func (l List2[T, U]) Product(mult func(T, T) T) fp.Option[T] {
	return fp.Product(l, mult)
}

// Min returns the minimum of the list using the given function to compare the values
func (l List2[T, U]) Min(compare func(T, T) bool) fp.Option[T] {
	return fp.Min(l, compare)
}

// Max returns the maximum of the list using the given function to compare the values
func (l List2[T, U]) Max(compare func(T, T) bool) fp.Option[T] {
	return fp.Max(l, compare)
}

// Contains returns true if the list contains the given value
func (l List2[T, U]) Contains(value T, compare func(T, T) bool) bool {
	return fp.Contains(l, value, compare)
}

// IndexOf returns the index of the given element if found, otherwise returns -1
func (l List2[T, U]) IndexOf(value T, compare func(T, T) bool) int {
	return fp.IndexOf(l, value, compare)
}

// LastIndexOf returns the index of the last element that matches the given predicate otherwise returns -1
func (l List2[T, U]) LastIndexOf(value T, compare func(T, T) bool) int {
	return fp.LastIndexOf(l, value, compare)
}

// Count returns the number of elements that match the given predicate
func (l List2[T, U]) Count(pred func(T) bool) int {
	return fp.Count(l, pred)
}

// Sort returns an new list sorted using the given sort order
func (l List2[T, U]) Sort(order func(T, T) bool) List2[T, U] {
	return fp.Sort(l, order)
}

// Unique returns all the unique elements in the list
func (l List2[T, U]) Unique() List2[T, U] {
	if len(l) <= 0 {
		return l
	}

	vals := make(map[any]byte, 0)

	unique := NewList2[T, U]()
	l.ForEach(func(in T) {
		if _, ok := vals[in]; !ok {
			vals[in] = 0
			unique = unique.Append(in)
		}
	})

	return unique
}

// Get provides a safe way of indexing the list.
// If the index provided exists, it will return Some(value).
// If the index provided doesn't exist, it will return None.
func (l List2[T, U]) Get(i int) fp.Option[T] {
	if i < 0 || i >= l.Size() {
		return fp.None[T]()
	}
	return fp.Some(l[i])
}

// ToList converts the List2[T, U] into a List[T]
func (l List2[T, U]) ToList() List[T] {
	results := make([]T, len(l))
	copy(results, l)
	return results
}
