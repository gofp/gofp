package collection

import (
	"fmt"
	"strings"

	fp "gitlab.com/gofp/gofp"
)

type Set[K comparable] map[K]struct{}

// NewSet creates a new set containing the given values
func NewSet[K comparable](values ...K) Set[K] {
	s := make(Set[K])

	for _, v := range values {
		s[v] = struct{}{}
	}

	return s
}

// Add inserts any new values to the set
func (s Set[K]) Add(values ...K) {
	for _, v := range values {
		s[v] = struct{}{}
	}
}

// Drop removes all given values from the set if they exist
func (s Set[K]) Drop(values ...K) {
	for _, v := range values {
		delete(s, v)
	}
}

// Contains returns true if the given value is in the set
func (s Set[K]) Contains(value K) bool {
	_, ok := s[value]
	return ok
}

// ContainsAll returns true if all the given values exist in the set
func (s Set[K]) ContainsAll(value ...K) bool {
	for _, v := range value {
		if !s.Contains(v) {
			return false
		}
	}

	return true
}

// Map returns a new set containing the values of the given map transformed using the given mapping function
func (s Set[K]) Map(f func(K) K) Set[K] {
	m := make(Set[K])

	for k := range s {
		m[f(k)] = struct{}{}
	}

	return m
}

func MapSet[K comparable, V comparable](in Set[K], fn func(K) V) Set[V] {
	out := make(Set[V])

	for k := range in {
		out[fn(k)] = struct{}{}
	}

	return out
}

// ToSlice converts the set to a slice of values. If an ordering is provided
// the values will be sorted using the given ordering function
// *WARNING* if the order function is not provided, you'll be at the mercy of the randomised ordering that Go gives
func (s Set[K]) ToSlice(order ...func(a, b K) bool) []K {
	out := make([]K, 0, len(s))

	for k := range s {
		out = append(out, k)
	}

	if len(order) > 0 {
		out = fp.Sort(out, order[0])
	}

	return out
}

// Fold applies the given function to each value in the set and returns the result of the last call
// uusing the provided ordering
// *WARNING* if the order function is left null, you'll be at the mercy of the random ordering that Go gives
func (s Set[K]) Fold(acc K, fn func(K, K) K, order func(K, K) bool) K {
	ss := s.ToSlice(order)

	for _, k := range ss {
		acc = fn(acc, k)
	}

	return acc
}

// Filter returns a new set containing only the values that match the given predicate
func (s Set[K]) Filter(pred func(K) bool) Set[K] {
	m := make(Set[K])

	for k := range s {
		if pred(k) {
			m[k] = struct{}{}
		}
	}

	return m
}

// FilterNot return a new set containing only the values that don't match the given predicate
func (s Set[K]) FilterNot(pred func(K) bool) Set[K] {
	m := make(Set[K])

	for k := range s {
		if !pred(k) {
			m[k] = struct{}{}
		}
	}

	return m
}

// Count returns the number of values that match the given predicate
func (s Set[K]) Count(pred func(K) bool) int {
	count := 0

	for k := range s {
		if pred(k) {
			count++
		}
	}

	return count
}

// Exists returns true if the set has at least one value that matches the given predicate
func (s Set[K]) Exists(pred func(K) bool) bool {
	for k := range s {
		if pred(k) {
			return true
		}
	}

	return false
}

// Partition takes a predicate and returns one set that matches the given predicate
// and one that doesn't
func (s Set[K]) Partition(pred func(K) bool) (Set[K], Set[K]) {
	a := make(Set[K])
	b := make(Set[K])

	for k := range s {
		if pred(k) {
			a[k] = struct{}{}
		} else {
			b[k] = struct{}{}
		}
	}

	return a, b
}

// IsEmpty returns true if the set is empty
func (s Set[K]) IsEmpty() bool { return len(s) <= 0 }

// NonEmpty returns true if the set is not empty
func (s Set[K]) NonEmpty() bool { return len(s) > 0 }

// Size returns the number of items in the set
func (s Set[K]) Size() int { return len(s) }

// ToStream returns a stream of values from the set
// If an order function is specified, the values will be sorted using the given function
// otherwise they will be in random order
func (s Set[K]) ToStream(order ...func(K, K) bool) chan K {
	out := make(chan K)

	go func() {
		defer close(out)

		ss := s.ToSlice(order...)

		for _, k := range ss {
			out <- k
		}
	}()

	return out
}

// ToBufferedStream returns a buffered stream of the values from the set
// If an order function is specified, the values will be sorted using the given function
// otherwise they will be in random order
func (s Set[K]) ToBufferedStream(size int, order ...func(K, K) bool) chan K {
	out := make(chan K, size)

	go func() {
		defer close(out)

		ss := s.ToSlice(order...)

		for _, k := range ss {
			out <- k
		}
	}()

	return out
}

// ForEach executes the given function for each item in the set
// If an order function is specified, the values will be sorted using the given function
// otherwise they will be in random order
func (s Set[K]) ForEach(pred func(K), order ...func(K, K) bool) {
	ss := s.ToSlice(order...)

	for _, k := range ss {
		pred(k)
	}
}

// String returns a string representation of the set
// If an order function is specified, the values will be sorted using the given function
// otherwise they will be in random order
func (s Set[K]) String(order ...func(K, K) bool) string {
	v := s.ToSlice(order...)
	buf := strings.Builder{}
	buf.WriteString("Set{")
	writeSep := false
	for _, k := range v {
		if writeSep {
			buf.WriteString(", ")
		}
		buf.WriteString(fmt.Sprintf("%v", k))
		writeSep = true
	}
	buf.WriteString("}")

	return buf.String()
}

// Union returns a new set containing all the values from all sets
func (s Set[K]) Union(others ...Set[K]) Set[K] {
	union := NewSet(s.ToSlice()...)

	for _, other := range others {
		union.Add(other.ToSlice()...)
	}

	return union
}

// Intersection returns a new set containing values that occur in all sets
func (s Set[K]) Intersection(others ...Set[K]) Set[K] {
	if len(others) == 0 {
		return s
	}

	// find the intersection between our set and the first set in the list of others
	intersection := NewSet[K]()
	for v := range others[0] {
		if s.Contains(v) {
			intersection.Add(v)
		}
	}

	// if there are no other sets to intersect, return the intersection
	if len(others) == 1 {
		return intersection
	}

	// otherwise recursively intersect the rest of the sets
	return intersection.Intersection(others[1:]...)
}

// Difference returns a new set containing values that are not contained in the other set
func (s Set[K]) Difference(other Set[K]) Set[K] {
	diff := NewSet[K]()
	for k := range s {
		if !other.Contains(k) {
			diff[k] = struct{}{}
		}
	}
	return diff
}

// Complement returns a set of values from the other set that are not in this set
func (s Set[K]) Complement(other Set[K]) Set[K] {
	comp := NewSet[K]()
	for k := range other {
		if !s.Contains(k) {
			comp[k] = struct{}{}
		}
	}
	return comp
}
