package collection

import (
	fp "gitlab.com/gofp/gofp"
)

// List is a generic list that implements the Traversable interface
type List[T any] []T

// NewList creates a new functional list with type specified
func NewList[T any](values ...T) List[T] {
	if values == nil {
		return make(List[T], 0)
	}
	return values
}

// Push adds values to the head of the list
func (l List[T]) Push(v ...T) List[T] {
	return append(v, l...)
}

// Append adds a value to the end of the list
func (l List[T]) Append(v ...T) List[T] {
	return append(l, v...)
}

// Pop returns the value at the head of the list and the remaining values
func (l List[T]) Pop() (fp.Option[T], List[T]) {
	return fp.Pop(l)
}

// PopN returns the n values at the head of the list and the remaining values
func (l List[T]) PopN(n int) (List[T], List[T]) {
	return fp.PopN(l, n)
}

// Take returns the value at the end of the list and the remaining values
func (l List[T]) Take() (fp.Option[T], List[T]) {
	return fp.Take(l)
}

// TakeN returns the n values at the end of the list and the remaining values at the head of the list
func (l List[T]) TakeN(n int) (List[T], List[T]) {
	return fp.TakeN(l, n)
}

// Map returns a new list with the values mapped over
func (l List[T]) Map(fn func(T) T) List[T] {
	return fp.Map(l, fn)
}

// Filter returns a new list with the values that match the given predicate
func (l List[T]) Filter(pred func(T) bool, max int) List[T] {
	return fp.Filter(l, pred, max)
}

// FilterNot returns a list with the values that don't match the given predicate
func (l List[T]) FilterNot(pred func(T) bool, max int) List[T] {
	return fp.FilterNot(l, pred, max)
}

// FoldLeft returns the result of folding the list from the start of the list
func (l List[T]) FoldLeft(initial T, fn func(T, T) T) T {
	return fp.FoldLeft(initial, l, fn)
}

// FoldRight returns the result of folding the list from the end of the list
func (l List[T]) FoldRight(initial T, fn func(T, T) T) T {
	return fp.FoldRight(initial, l, fn)
}

// Head returns the optional value at the head of the list. If the list is empty,
// the value is None
func (l List[T]) Head() fp.Option[T] {
	if len(l) <= 0 {
		return fp.None[T]()
	}
	return fp.Some(l[0])
}

// Tail returns the list without the head value
func (l List[T]) Tail() List[T] {
	if len(l) <= 0 {
		return l
	}
	return l[1:]
}

// Last returns the last element of the list
func (l List[T]) Last() fp.Option[T] {
	if len(l) <= 0 {
		return fp.None[T]()
	}

	return fp.Some(l[len(l)-1])
}

// ToString converts every element to a string and returns the slice of strings
func (l List[T]) ToStrings() []string {
	return fp.ToStrings(l)
}

// SplitAt splits the list at the desired index
func (l List[T]) SplitAt(index int) (List[T], List[T]) {
	return fp.SplitAt(l, index)
}

// Partition splits the list into 2 lists that contains either elements that match the
// given predicate, or elements that don't match the predicate
func (l List[T]) Partition(pred func(T) bool) (List[T], List[T]) {
	return fp.Partition(l, pred)
}

// Slice returns a slice of the list between the given start and end values.
// If negative values are given, the indices will be calculated from the end
// of the list.
func (l List[T]) Slice(start, end int) List[T] {
	// If start or end is negative, it is relative to the end of the list
	if start < 0 {
		start = len(l) + start
	}
	if end < 0 {
		end = len(l)
	}

	if start >= len(l) || start > end {
		return make(List[T], 0)
	}

	if end >= len(l) {
		end = len(l)
	}

	ll := make(List[T], end-start)
	copy(ll, l[start:end])

	return ll
}

// IsEmpty returns true if the list is empty
func (l List[T]) IsEmpty() bool {
	return len(l) == 0
}

// NonEmpty returns true if the list has values
func (l List[T]) NonEmpty() bool {
	return len(l) > 0
}

// Size returns the number of items in the list
func (l List[T]) Size() int {
	return len(l)
}

// Cap returns the current capacity of the list
func (l List[T]) Cap() int { return cap(l) }

// Reverse returns the list in reverse order
func (l List[T]) Reverse() List[T] {
	return fp.Reverse(l)
}

// ToStream returns a stream of the list
func (l List[T]) ToStream() chan T {
	return fp.ToStream(l)
}

// ToBufferedStream returns a buffered stream of the file
func (l List[T]) ToBufferedStream(size int) chan T {
	return fp.ToBufferedStream(l, size)
}

// ForEach executes the given function for each element in the list
func (l List[T]) ForEach(fn func(T)) List[T] {
	return fp.ForEach(l, fn)
}

// Sum returns the sum of the list using the given function to add the values necessary to provide the sum
func (l List[T]) Sum(add func(T, T) T) fp.Option[T] {
	return fp.Sum(l, add)
}

// Product returns the product of the list using the given function to multiply the values necessary to provide the product
func (l List[T]) Product(mult func(T, T) T) fp.Option[T] {
	return fp.Product(l, mult)
}

// Min returns the minimum of the list using the given function to compare the values
func (l List[T]) Min(compare func(T, T) bool) fp.Option[T] {
	return fp.Min(l, compare)
}

// Max returns the maximum of the list using the given function to compare the values
func (l List[T]) Max(compare func(T, T) bool) fp.Option[T] {
	return fp.Max(l, compare)
}

// Contains returns true if the list contains the given value
func (l List[T]) Contains(value T, compare func(T, T) bool) bool {
	return fp.Contains(l, value, compare)
}

// IndexOf returns the index of the given element if found, otherwise returns -1
func (l List[T]) IndexOf(value T, compare func(T, T) bool) int {
	return fp.IndexOf(l, value, compare)
}

// LastIndexOf returns the index of the last element that matches the given predicate otherwise returns -1
func (l List[T]) LastIndexOf(value T, compare func(T, T) bool) int {
	return fp.LastIndexOf(l, value, compare)
}

// Count returns the number of elements that match the given predicate
func (l List[T]) Count(pred func(T) bool) int {
	return fp.Count(l, pred)
}

// Sort returns an new list sorted using the given sort order
func (l List[T]) Sort(order func(T, T) bool) List[T] {
	return fp.Sort(l, order)
}

// Unique returns all the unique elements in the list
func (l List[T]) Unique() List[T] {
	if len(l) <= 0 {
		return l
	}

	vals := make(map[any]byte, 0)

	unique := NewList[T]()
	l.ForEach(func(in T) {
		if _, ok := vals[in]; !ok {
			vals[in] = 0
			unique = unique.Append(in)
		}
	})

	return unique
}

// Get provides a safe way of indexing the list.
// If the index provided exists, it will return Some(value).
// If the index provided doesn't exist, it will return None.
func (l List[T]) Get(i int) fp.Option[T] {
	if i < 0 || i >= l.Size() {
		return fp.None[T]()
	}
	return fp.Some(l[i])
}
