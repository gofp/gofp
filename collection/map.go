package collection

import (
	"fmt"
	"strings"

	fp "gitlab.com/gofp/gofp"
)

// Map is a generic collection of key value pairs with functional programming support
type Map[K comparable, V any] map[K]V

// KV is a generic struct of individual key value pairs
type KV[K comparable, V any] struct {
	K K
	V V
}

// NewMap creates a new Map. You can pass in as many KV pairs as you want to initialize the map
func NewMap[K comparable, V any](kvs ...KV[K, V]) Map[K, V] {
	m := make(Map[K, V])

	for _, kv := range kvs {
		m[kv.K] = kv.V
	}

	return m
}

// Add inserts a key value pair into the map if the key doesn't exist. If the key does already exist,
// the current value is replaced with the new value
func (m Map[K, V]) Add(k K, v V) Map[K, V] {
	m[k] = v
	return m
}

// Rem removes a value from the map if it has the given key, otherwise it does nothing
func (m Map[K, V]) Rem(k K) Map[K, V] {
	delete(m, k)
	return m
}

// Contains return true if the map has an item with the given key, otherwise it returns false
func (m Map[K, V]) Contains(k K) bool {
	_, ok := m[k]
	return ok
}

// Get returns the value and true if the key exists in the map, otherwise it returns the zero value
// for the value's type and false
func (m Map[K, V]) Get(k K) (V, bool) {
	v, ok := m[k]
	return v, ok
}

// GetOrElse returns the value if the key exists, otherwise it returns the provided other value
func (m Map[K, V]) GetOrElse(k K, v V) V {
	if !m.Contains(k) {
		return v
	}

	return m[k]
}

// Exists returns true if the map has at least one item that satisfies the given predicate,
// otherwise it returns false
func (m Map[K, V]) Exists(pred func(K, V) bool) bool {
	for k, v := range m {
		if pred(k, v) {
			return true
		}
	}

	return false
}

// Count returns the number of items that match the given predicate
func (m Map[K, V]) Count(pred func(K, V) bool) int {
	count := 0
	for k, v := range m {
		if pred(k, v) {
			count++
		}
	}

	return count
}

// Filter returns a new map with only the values that match the given predicate
func (m Map[K, V]) Filter(pred func(K, V) bool) Map[K, V] {
	mm := make(map[K]V)

	for k, v := range m {
		if pred(k, v) {
			mm[k] = v
		}
	}

	return mm
}

// FilterKeys returns a new map with only the values where the keys match the given predicate
func (m Map[K, V]) FilterKeys(pred func(K) bool) Map[K, V] {
	mm := make(map[K]V)
	for k, v := range m {
		if pred(k) {
			mm[k] = v
		}
	}
	return mm
}

// FilterNot returns a new map with only the values that do not satisfy the given predicate
func (m Map[K, V]) FilterNot(pred func(K, V) bool) Map[K, V] {
	mm := make(map[K]V)
	for k, v := range m {
		if pred(k, v) {
			continue
		}
		mm[k] = v
	}
	return mm
}

// Map iterates through the map and transforms the KV pair using the mapping function provided
// and returns a new map with the transformed data
func (m Map[K, V]) Map(fn func(K, V) (K, V)) Map[K, V] {
	mm := make(map[K]V)

	for k, v := range m {
		kk, vv := fn(k, v)
		mm[kk] = vv
	}

	return mm
}

// MapValues iterates through the map and transforms the values in each KV pair using the mapping
// function provided and returns a new map with the transformed data
func (m Map[K, V]) MapValues(fn func(V) V) Map[K, V] {
	mm := make(map[K]V)

	for k, v := range m {
		mm[k] = fn(v)
	}

	return mm
}

// MapKeys iterates through the map and transforms the keys in each KV pair using the mapping
// function provided and returns a new map with the transformed data
func (m Map[K, V]) MapKeys(fn func(K) K) Map[K, V] {
	mm := make(map[K]V)

	for k, v := range m {
		mm[fn(k)] = v
	}

	return mm
}

// Fold applies the given function to an initial value and every value in the map using the given ordering
func (m Map[K, V]) Fold(acc V, fn func(V, K, V) V, order func(K, K) bool) V {
	// Go Maps do not guarantee order, so we need to ensure order by sorting the keys
	keys := m.Keys(order)
	for _, k := range keys {
		acc = fn(acc, k, m[k])
	}

	return acc
}

// ForEach applies the given function to every item in the map using the given ordering.
// *WARNING* if the order function is left null, you'll be at the mercy of the random ordering that Go gives to maps
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
func (m Map[K, V]) ForEach(fn func(K, V), order ...func(K, K) bool) Map[K, V] {
	keys := m.Keys(order...)
	for _, k := range keys {
		fn(k, m[k])
	}
	return m
}

// Partition returns 2 maps, one with the key/value pairs that match the given predicate,
// and one with the key/value pairs that do not match the given predicate
func (m Map[K, V]) Partition(pred func(K, V) bool) (Map[K, V], Map[K, V]) {
	matched := make(map[K]V)
	unmatched := make(map[K]V)

	for k, v := range m {
		if pred(k, v) {
			matched[k] = v
		} else {
			unmatched[k] = v
		}
	}

	return matched, unmatched
}

// ToStream returns a stream of the key/value pairs in the map using the ordering provided
// *WARNING* if the order function is left null, you'll be at the mercy of the random ordering that Go gives to maps
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
func (m Map[K, V]) ToStream(order ...func(K, K) bool) chan KV[K, V] {
	ch := make(chan KV[K, V])
	keys := m.Keys(order...)

	go func(keys []K) {
		for _, k := range keys {
			ch <- KV[K, V]{k, m[k]}
		}
		close(ch)
	}(keys)

	return ch
}

// ToBufferedStream returns a buffered stream of the key/value pairs in the map using the ordering provided
// *WARNING* if the order function is left null, you'll be at the mercy of the random ordering that Go gives to maps
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
func (m Map[K, V]) ToBufferedStream(size int, order ...func(K, K) bool) chan KV[K, V] {
	ch := make(chan KV[K, V], size)

	keys := m.Keys(order...)

	go func(keys []K) {
		for _, k := range keys {
			ch <- KV[K, V]{k, m[k]}
		}
		close(ch)
	}(keys)

	return ch
}

// ToSlice returns a slice of the key/value pairs in the map
// If an ordering function is provided, it will order the keys according to the ordering function
// otherwise the keys will be returned in an arbitrary order
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
func (m Map[K, V]) ToSlice(order ...func(KV[K, V], KV[K, V]) bool) []KV[K, V] {
	s := make([]KV[K, V], 0, len(m))

	for k, v := range m {
		s = append(s, KV[K, V]{k, v})
	}

	if len(order) > 0 {
		s = fp.Sort(s, order[0])
	}

	return s
}

// ToSliceK returns a slice of all the keys in the map.
// If an ordering function is provided, it will order the keys according to the ordering function
// otherwise the keys will be returned in an arbitrary order
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
func (m Map[K, V]) ToSliceK(order ...func(K, K) bool) []K {
	s := make([]K, 0, len(m))

	for k := range m {
		s = append(s, k)
	}

	if len(order) > 0 {
		s = fp.Sort(s, order[0])
	}

	return s
}

// ToSliceV returns a slice of all the values in the map
// If an ordering function is specified, it will order the values according to the ordering function
// otherwise the values will be returned in an arbitrary order
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
func (m Map[K, V]) ToSliceV(order ...func(V, V) bool) []V {
	s := make([]V, 0, len(m))

	for _, v := range m {
		s = append(s, v)
	}

	if len(order) > 0 {
		s = fp.Sort(s, order[0])
	}

	return s
}

// ToList returns a List of all the key/value pairs in the map
// If an ordering function is specified, it will order the values according to the ordering function
// otherwise the values will be returned in an arbitrary order
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
func (m Map[K, V]) ToList(order ...func(KV[K, V], KV[K, V]) bool) List[KV[K, V]] {
	s := make(List[KV[K, V]], 0, len(m))

	for k, v := range m {
		s = append(s, KV[K, V]{k, v})
	}

	if len(order) > 0 {
		s = fp.Sort(s, order[0])
	}

	return s
}

// ToListK returns a list of all the keys in the map
// If an ordering function is specified, it will order the values according to the ordering function
// otherwise the values will be returned in an arbitrary order
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
func (m Map[K, V]) ToListK(order ...func(K, K) bool) List[K] {
	s := make(List[K], 0, len(m))

	for k := range m {
		s = append(s, k)
	}

	if len(order) > 0 {
		return s.Sort(order[0])
	}

	return s
}

// ToListV returns a list of all the values in the map
// If an ordering function is specified, it will order the values according to the ordering function
// otherwise the values will be returned in an arbitrary order
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
func (m Map[K, V]) ToListV(order ...func(V, V) bool) List[V] {
	s := make(List[V], 0, len(m))

	for _, v := range m {
		s = append(s, v)
	}

	if len(order) > 0 {
		return s.Sort(order[0])
	}

	return s
}

// String returns a string representation of the map using the ordering provided
// If an ordering function is specified, it will order the values according to the ordering function
// otherwise the values will be returned in an arbitrary order
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
// *WARNING* if the order function is left null, you'll be at the mercy of the random ordering that Go gives to maps
func (m Map[K, V]) String(order ...func(K, K) bool) string {
	keys := m.Keys(order...)
	sb := strings.Builder{}
	sb.WriteString("Map{")
	addSeperator := false
	for _, k := range keys {
		if addSeperator {
			sb.WriteString(", ")
		}
		sb.WriteString(fmt.Sprintf("%v: %v", k, m[k]))
		addSeperator = true
	}
	sb.WriteString("}")
	return sb.String()
}

// Keys returns a slice of the keys in the map in the order provided by the order function
// If no order function is provided, the keys are returned in a random order as determined by Go
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
func (m Map[K, V]) Keys(order ...func(K, K) bool) []K {
	return m.ToSliceK(order...)
}

// Values returns a slice of the values in the map with the keys ordered by the given ordering
// function. If no ordering is specified, the values are returned in the random order provided by Go
// While the order parameter is variadic, this is to allow 0 or 1 ordering function to be passed to the function.
// If multiple ordering functions are provided, only the first will be used, the rest will be ignored
func (m Map[K, V]) Values(order ...func(K, K) bool) []V {
	keys := m.Keys(order...)
	values := make([]V, 0, len(keys))
	for _, k := range keys {
		values = append(values, m[k])
	}
	return values
}
