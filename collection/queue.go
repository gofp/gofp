package collection

import fp "gitlab.com/gofp/gofp"

// Queue is a FIFO (First-In-First-Out) data structure of fixed capacity
// Items can be pushed on to the end of the queue, and Popped from the head
// or Taken from the tail of the queue. When the queue reaches its capacity
// it will start to drop the oldest items.
type Queue[T any] struct {
	q   List[T]
	cap int
}

// NewQueue creates a new queue with the given capacity
func NewQueue[T any](cap int, v ...T) Queue[T] {
	q := Queue[T]{
		q:   make(List[T], 0, cap),
		cap: cap,
	}

	if len(v) < cap {
		q.q = append(q.q, v...)
	} else {
		q.q = append(q.q, v[len(v)-cap:]...)
	}

	return q
}

// Head returns an optional item from the head of the queue
// If the queue is empty the optional value will be None
func (q Queue[T]) Head() fp.Option[T] {
	if len(q.q) == 0 {
		return fp.None[T]()
	}

	return fp.Some(q.q[0])
}

// Last returns an optional value from the end of the queue.
// If the queue is empty the optional value will be None
func (q Queue[T]) Last() fp.Option[T] {
	if len(q.q) == 0 {
		return fp.None[T]()
	}

	return fp.Some(q.q[len(q.q)-1])
}

// ToList returns the items in the queue as a List
func (q Queue[T]) ToList() List[T] {
	return q.q
}

// ToSlice returns the items in the queue as a slice
func (q Queue[T]) ToSlice() []T {
	return q.q
}

// Tail returns the tail of the queue without the head item
func (q Queue[T]) Tail() Queue[T] {
	return Queue[T]{
		q:   q.q.Tail(),
		cap: q.cap,
	}
}

// Push adds a value to the end of the queue
func (q Queue[T]) Push(item T) Queue[T] {
	nq := q.q.Append(item)
	if nq.Size() > q.cap {
		_, nq = nq.Pop()
	}
	return Queue[T]{
		q:   nq,
		cap: q.cap,
	}
}

// Pop returns the item at the head of the queue and the remaining items
func (q Queue[T]) Pop() (fp.Option[T], Queue[T]) {
	if q.Size() <= 0 {
		return fp.None[T](), q
	}

	return fp.Some(q.q[0]), Queue[T]{
		q:   q.q[1:],
		cap: q.cap,
	}
}

// Take returns the item at the end of the queue and the remaining items
func (q Queue[T]) Take() (fp.Option[T], Queue[T]) {
	if q.Size() <= 0 {
		return fp.None[T](), q
	}

	lastIndex := len(q.q) - 1

	return fp.Some(q.q[lastIndex]), Queue[T]{
		q:   q.q[:lastIndex],
		cap: q.cap,
	}
}

// Reverse will reverse the queue
func (q Queue[T]) Reverse() Queue[T] {
	return Queue[T]{
		q:   q.q.Reverse(),
		cap: q.cap,
	}
}

// Filter will remove all items from the queue that do not match the predicate
// The order and the capacity of the new queue will be the same as the original
func (q Queue[T]) Filter(pred func(T) bool, max int) Queue[T] {
	return Queue[T]{
		q:   q.q.Filter(pred, max),
		cap: q.cap,
	}
}

// FilterNot will remove all items from the queue that match the predicate
// The order and the capacity of the new queue will be the same as the original
func (q Queue[T]) FilterNot(pred func(T) bool, max int) Queue[T] {
	return Queue[T]{
		q:   q.q.FilterNot(pred, max),
		cap: q.cap,
	}
}

// FoldLeft will traverse the queue from the head to the tail, accumulating the value
// using the given function.
func (q Queue[T]) FoldLeft(acc T, fn func(T, T) T) T {
	return q.q.FoldLeft(acc, fn)
}

// FoldRight will traverse the queue from the tail to the head, accumulating the value
// using the given function.
func (q Queue[T]) FoldRight(acc T, fn func(T, T) T) T {
	return q.q.FoldRight(acc, fn)
}

// ToStream will return a stream of the contents of the queue
func (q Queue[T]) ToStream() chan T {
	return q.q.ToStream()
}

// ToBufferedStream will return a buffered stream of the contents of the queue
func (q Queue[T]) ToBufferedStream(size int) chan T {
	return q.q.ToBufferedStream(size)
}

// IsEmpty returns true if the queue is empty
func (q Queue[T]) IsEmpty() bool {
	return q.q.IsEmpty()
}

// NonEmpty returns true if the queue is not empty
func (q Queue[T]) NonEmpty() bool {
	return q.q.NonEmpty()
}

// Size returns the size of the queue
func (q Queue[T]) Size() int {
	return q.q.Size()
}

// Cap returns the capacity of the queue
func (q Queue[T]) Cap() int {
	return q.cap
}

// SplitAt will split the queue into 2 separate queues at the given index
// maintaining the existing order and capacity
func (q Queue[T]) SplitAt(n int) (Queue[T], Queue[T]) {
	l, r := q.q.SplitAt(n)

	return Queue[T]{
			q:   l,
			cap: q.cap,
		}, Queue[T]{
			q:   r,
			cap: q.cap,
		}
}

// Partition will split the queue into 2 separate queues based on the predicate
// The order and the capacity of the new queues will be the same as the original
func (q Queue[T]) Partition(fn func(T) bool) (Queue[T], Queue[T]) {
	l, r := q.q.Partition(fn)
	return Queue[T]{
			q:   l,
			cap: q.cap,
		}, Queue[T]{
			q:   r,
			cap: q.cap,
		}
}

// Slice will return a new queue with the given range of items (exclusive)
// The order and the capacity of the new queue will be the same as the original
func (q Queue[T]) Slice(start int, end int) Queue[T] {
	return Queue[T]{
		q:   q.q.Slice(start, end),
		cap: q.cap,
	}
}

// ToStrings converts each item in the queue to its string representation and returns a slice of those strings
func (q Queue[T]) ToStrings() []string {
	return q.q.ToStrings()
}

// Map will apply the given function to each item in the queue and return a new queue with the results
func (q Queue[T]) Map(fn func(T) T) Queue[T] {
	return Queue[T]{
		q:   q.q.Map(fn),
		cap: q.cap,
	}
}

// ForEach will apply the given function to each item in the queue
func (q Queue[T]) ForEach(fn func(T)) {
	q.q.ForEach(fn)
}

// Sum returns the optional sum of all the values in the queue using the provided add function
// If the sum cannot be calculated, the function returns an optional.None[T]
func (q Queue[T]) Sum(add func(T, T) T) fp.Option[T] {
	return q.q.Sum(add)
}

// Product returns the optional product of all the values in the queue using the provided multiply function
// If the product cannot be calculated, the function returns an optional.None[T]
func (q Queue[T]) Product(mult func(T, T) T) fp.Option[T] {
	return q.q.Product(mult)
}

// Min returns the optional minimum value in the queue using the provided compare function
func (q Queue[T]) Min(compare func(T, T) bool) fp.Option[T] {
	return q.q.Min(compare)
}

// Max returns the optional maximum value in the queue using the provided compare function
func (q Queue[T]) Max(compare func(T, T) bool) fp.Option[T] {
	return q.q.Max(compare)
}

// Contains returns true if the queue contains the given item
func (q Queue[T]) Contains(value T, compare func(T, T) bool) bool {
	return q.q.Contains(value, compare)
}

// IndexOf returns the first index of the given item in the queue
func (q Queue[T]) IndexOf(value T, compare func(T, T) bool) int {
	return q.q.IndexOf(value, compare)
}

// LastIndexOf returns the last index of the given item in the queue
func (q Queue[T]) LastIndexOf(value T, compare func(T, T) bool) int {
	return q.q.LastIndexOf(value, compare)
}

// Count returns the number of times an item that satifies the given predicate occurs in the queue
func (q Queue[T]) Count(pred func(T) bool) int {
	return q.q.Count(pred)
}
