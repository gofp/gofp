package collection_test

import (
	"testing"
	"time"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/collection"
	"gitlab.com/gofp/gofp/ranger"
)

func TestNewList(t *testing.T) {
	t.Run("NewList ints", func(t *testing.T) {
		want := collection.List[int]{1, 2, 3}
		got := collection.NewList(1, 2, 3)
		assert.Equal(t, want, got)
	})

	t.Run("NewList strings", func(t *testing.T) {
		want := collection.List[string]{"a", "b", "c"}
		got := collection.NewList("a", "b", "c")
		assert.Equal(t, want, got)
	})

	t.Run("NewList floats", func(t *testing.T) {
		want := collection.List[float64]{1.1, 2.2, 3.3}
		got := collection.NewList(1.1, 2.2, 3.3)
		assert.Equal(t, want, got)
	})

	t.Run("NewList decimals", func(t *testing.T) {
		want := collection.List[decimal.Decimal]{
			decimal.NewFromFloat(1.1),
			decimal.NewFromFloat(2.2),
			decimal.NewFromFloat(3.3),
		}
		got := collection.NewList(decimal.NewFromFloat(1.1), decimal.NewFromFloat(2.2), decimal.NewFromFloat(3.3))
		assert.Equal(t, want, got)
	})

	t.Run("NewList times", func(t *testing.T) {
		want := collection.List[time.Time]{
			time.Date(2022, time.January, 1, 0, 0, 0, 0, time.UTC),
			time.Date(2022, time.February, 1, 0, 0, 0, 0, time.UTC),
			time.Date(2022, time.March, 1, 0, 0, 0, 0, time.UTC),
		}
		got := collection.NewList(
			time.Date(2022, time.January, 1, 0, 0, 0, 0, time.UTC),
			time.Date(2022, time.February, 1, 0, 0, 0, 0, time.UTC),
			time.Date(2022, time.March, 1, 0, 0, 0, 0, time.UTC),
		)
		assert.Equal(t, want, got)
	})

	t.Run("NewList ints (empty)", func(t *testing.T) {
		want := collection.List[int]{}
		got := collection.NewList[int]()
		assert.Equal(t, want, got)
	})

	t.Run("NewList strings (empty)", func(t *testing.T) {
		want := collection.List[string]{}
		got := collection.NewList[string]()
		assert.Equal(t, want, got)
	})

	t.Run("NewList floats (empty)", func(t *testing.T) {
		want := collection.List[float64]{}
		got := collection.NewList[float64]()
		assert.Equal(t, want, got)
	})
}

func TestList_Push(t *testing.T) {
	t.Run("Push should add values to the head of the list", func(t *testing.T) {
		want := collection.List[int]{1, 2, 3, 4}
		got := collection.NewList(2, 3, 4).Push(1)
		assert.Equal(t, want, got)
	})

	t.Run("Push can be chained", func(t *testing.T) {
		want := collection.List[int]{4, 3, 2, 1}
		got := collection.NewList[int]().Push(1).Push(2).Push(3).Push(4)
		assert.Equal(t, want, got)
	})
}

func TestList_Append(t *testing.T) {
	t.Run("Append should add values to the back of the list", func(t *testing.T) {
		want := collection.List[int]{1, 2, 3, 4}
		got := collection.NewList(1, 2, 3).Append(4)
		assert.Equal(t, want, got)
	})

	t.Run("Append can be chained", func(t *testing.T) {
		want := collection.List[int]{1, 2, 3, 4}
		got := collection.NewList[int]().Append(1).Append(2).Append(3).Append(4)
		assert.Equal(t, want, got)
	})
}

func TestList_Pop(t *testing.T) {
	t.Run("Pop should remove values from the head of the list and return the value and the remaining list", func(t *testing.T) {
		ints := collection.NewList(1, 2, 3, 4)
		got, ints := ints.Pop()
		assert.Equal(t, fp.Some(1), got)
		assert.Equal(t, collection.List[int]{2, 3, 4}, ints)
		got, ints = ints.Pop()
		assert.Equal(t, fp.Some(2), got)
		assert.Equal(t, collection.List[int]{3, 4}, ints)
		got, ints = ints.Pop()
		assert.Equal(t, fp.Some(3), got)
		assert.Equal(t, collection.List[int]{4}, ints)
		got, ints = ints.Pop()
		assert.Equal(t, fp.Some(4), got)
		assert.Equal(t, collection.List[int]{}, ints)
		got, ints = ints.Pop()
		assert.Equal(t, fp.None[int](), got)
		assert.Equal(t, collection.List[int]{}, ints)
	})
}

func TestList_PopN(t *testing.T) {
	t.Run("PopN should remove N elements from the list", func(t *testing.T) {
		ints := collection.NewList(1, 2, 3, 4)
		got, ints := ints.PopN(2)
		assert.Equal(t, collection.List[int]{1, 2}, got)
		assert.Equal(t, collection.List[int]{3, 4}, ints)
	})

	t.Run("PopN should return an empty list and a copy of the original list if N is less than 1", func(t *testing.T) {
		ints := collection.NewList(1, 2, 3, 4)
		got, remaining := ints.PopN(0)
		assert.Equal(t, collection.List[int]{}, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, remaining)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, ints)
	})

	t.Run("PopN should return a copy of the list, and an empty list as remaining if N is greater than the length of the list", func(t *testing.T) {
		ints := collection.NewList(1, 2, 3, 4)
		got, remaining := ints.PopN(5)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, got)
		assert.Equal(t, collection.List[int]{}, remaining)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, ints)
	})
}

func TestList_Take(t *testing.T) {
	t.Run("Take should remove values from the end of the list and return the value and the remaining list", func(t *testing.T) {
		ints := collection.NewList(1, 2, 3, 4)
		got, ints := ints.Take()
		assert.Equal(t, fp.Some(4), got)
		assert.Equal(t, collection.List[int]{1, 2, 3}, ints)
		got, ints = ints.Take()
		assert.Equal(t, fp.Some(3), got)
		assert.Equal(t, collection.List[int]{1, 2}, ints)
		got, ints = ints.Take()
		assert.Equal(t, fp.Some(2), got)
		assert.Equal(t, collection.List[int]{1}, ints)
		got, ints = ints.Take()
		assert.Equal(t, fp.Some(1), got)
		assert.Equal(t, collection.List[int]{}, ints)
		got, ints = ints.Take()
		assert.Equal(t, fp.None[int](), got)
		assert.Equal(t, collection.List[int]{}, ints)
	})
}

func TestList_TakeN(t *testing.T) {
	t.Run("TakeN returns a copy of the last N elements and remaining elements of the given list", func(t *testing.T) {
		ints := collection.NewList(1, 2, 3, 4)
		got, remaining := ints.TakeN(2)
		assert.Equal(t, collection.List[int]{3, 4}, got)
		assert.Equal(t, collection.List[int]{1, 2}, remaining)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, ints)
	})

	t.Run("TakeN returns an empty list and a copy of the given list when N is less than 1", func(t *testing.T) {
		ints := collection.NewList(1, 2, 3, 4)
		got, remaining := ints.TakeN(0)
		assert.Equal(t, collection.List[int]{}, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, remaining)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, ints)
	})

	t.Run("TakeN returns a copy of the given list and an empty list as remaining if N is greater than the length of the list", func(t *testing.T) {
		ints := collection.NewList(1, 2, 3, 4)
		got, remaining := ints.TakeN(5)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, got)
		assert.Equal(t, collection.List[int]{}, remaining)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, ints)
	})
}

func TestList_Map(t *testing.T) {
	t.Run("Map should apply the function to each value in the list and return a new list with the results", func(t *testing.T) {
		want := collection.List[int]{2, 4, 6, 8}
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Map(func(i int) int {
			return i * 2
		})
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, myList)
	})

	t.Run("Map can be chained", func(t *testing.T) {
		want := collection.List[int]{3, 5, 7, 9}
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Map(func(i int) int {
			return i * 2
		}).Map(func(i int) int {
			return i + 1
		})
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, myList)
	})
}

func TestList_Filter(t *testing.T) {
	t.Run("Filter should return a new list with only the values that pass the predicate", func(t *testing.T) {
		want := collection.List[int]{2, 4}
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Filter(func(i int) bool {
			return i%2 == 0
		}, 0)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, myList)
	})

	t.Run("Filter can be chained", func(t *testing.T) {
		want := collection.List[int]{4}
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Filter(func(i int) bool {
			return i%2 == 0
		}, 0).Filter(func(i int) bool {
			return i%4 == 0
		}, 0)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, myList)
	})
}

func TestList_FilterNot(t *testing.T) {
	t.Run("FilterNot should return a new list with only the values that do not pass the predicate", func(t *testing.T) {
		want := collection.List[int]{1, 3}
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.FilterNot(func(i int) bool {
			return i%2 == 0
		}, 0)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, myList)
	})
}

func TestList_FoldLeft(t *testing.T) {
	t.Run("FoldLeft should apply the function to each value in the list and return the result", func(t *testing.T) {
		want := 10
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.FoldLeft(0, func(acc, i int) int {
			return acc + i
		})
		assert.Equal(t, want, got)
	})

	t.Run("FoldLeft should apply the function to each value in the list left to right", func(t *testing.T) {
		want := "abcd"
		myList := collection.NewList("a", "b", "c", "d")
		got := myList.FoldLeft("", func(acc, i string) string {
			return acc + i
		})
		assert.Equal(t, want, got)
	})
}

func TestList_FoldRight(t *testing.T) {
	t.Run("FoldRight should apply the function to each value in the list and return the result", func(t *testing.T) {
		want := 10
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.FoldRight(0, func(acc, i int) int {
			return acc + i
		})
		assert.Equal(t, want, got)
	})

	t.Run("FoldRight should apply the function to each value in the list left to right", func(t *testing.T) {
		want := "dcba"
		myList := collection.NewList("a", "b", "c", "d")
		got := myList.FoldRight("", func(acc, i string) string {
			return acc + i
		})
		assert.Equal(t, want, got)
	})
}

func TestList_Head(t *testing.T) {
	t.Run("Head should return the first value in the list without altering the list", func(t *testing.T) {
		want := fp.Some(1)
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Head()
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, myList)
	})

	t.Run("Head should return a None value if the list is empty", func(t *testing.T) {
		want := fp.None[int]()
		myList := collection.NewList[int]()
		got := myList.Head()
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{}, myList)
	})
}

func TestList_Tail(t *testing.T) {
	t.Run("Tail should return the list without the first value and without altering the list", func(t *testing.T) {
		want := collection.List[int]{2, 3, 4}
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Tail()
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4}, myList)
	})

	t.Run("Tail should return nil if the list is empty", func(t *testing.T) {
		want := collection.List[int]{}
		myList := collection.NewList[int]()
		got := myList.Tail()
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{}, myList)
	})
}

func TestList_Last(t *testing.T) {
	t.Run("Last should return the last value in the list without altering the list", func(t *testing.T) {
		want := fp.Some(4)
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Last()
		assert.Equal(t, want, got)
	})

	t.Run("Last should return none if the list is empty", func(t *testing.T) {
		want := fp.None[int]()
		myList := collection.NewList[int]()
		got := myList.Last()
		assert.Equal(t, want, got)
	})
}

func TestList_ToString(t *testing.T) {
	t.Run("ToString should return a string representation of the list", func(t *testing.T) {
		want := []string{"1", "2", "3", "4"}
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.ToStrings()
		assert.Equal(t, want, got)
	})
}

func TestList_SplitAt(t *testing.T) {
	t.Run("SplitAt should return 2 lists split at the desired index", func(t *testing.T) {
		wantLeft, wantRight := collection.List[int]{1, 2}, collection.List[int]{3, 4}
		myList := collection.NewList(1, 2, 3, 4)
		gotLeft, gotRight := myList.SplitAt(2)
		assert.Equal(t, wantLeft, gotLeft)
		assert.Equal(t, wantRight, gotRight)
	})

	t.Run("SplitAt should return 2 lists when the desired index is out of bound", func(t *testing.T) {
		wantLeft, wantRight := collection.List[int]{1, 2, 3, 4}, collection.List[int]{}
		myList := collection.NewList(1, 2, 3, 4)
		gotLeft, gotRight := myList.SplitAt(5)
		assert.Equal(t, wantLeft, gotLeft)
		assert.Equal(t, wantRight, gotRight)
	})

	t.Run("SplitAt should return 2 lists when the desired index is 0", func(t *testing.T) {
		wantLeft, wantRight := collection.List[int]{}, collection.List[int]{1, 2, 3, 4}
		myList := collection.NewList(1, 2, 3, 4)
		gotLeft, gotRight := myList.SplitAt(0)
		assert.Equal(t, wantLeft, gotLeft)
		assert.Equal(t, wantRight, gotRight)
	})
}

func TestList_Partition(t *testing.T) {
	t.Run("Partition should return 2 lists split at the desired index", func(t *testing.T) {
		wantLeft, wantRight := collection.List[int]{2, 4}, collection.List[int]{1, 3}
		myList := collection.NewList(1, 2, 3, 4)
		gotLeft, gotRight := myList.Partition(func(i int) bool {
			return i%2 == 0
		})
		assert.Equal(t, wantLeft, gotLeft)
		assert.Equal(t, wantRight, gotRight)
	})
}

func TestList_Slice(t *testing.T) {
	t.Run("Slice should return a slice using the given indexes", func(t *testing.T) {
		want := collection.List[int]{2, 3}
		myList := collection.NewList(1, 2, 3, 4, 5)
		got := myList.Slice(1, 3)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4, 5}, myList)
	})

	t.Run("Slice should return a slice given negative indexes", func(t *testing.T) {
		want := collection.List[int]{4, 5}
		myList := collection.NewList(1, 2, 3, 4, 5)
		got := myList.Slice(-2, -1)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4, 5}, myList)
	})

	t.Run("Slice should return an empty slice given indexes out of bound", func(t *testing.T) {
		want := collection.List[int]{}
		myList := collection.NewList(1, 2, 3, 4, 5)
		got := myList.Slice(5, 10)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{1, 2, 3, 4, 5}, myList)
	})
}

func TestList_IsEmpty(t *testing.T) {
	t.Run("IsEmpty should be true if the list is empty", func(t *testing.T) {
		want := true
		myList := collection.NewList[int]()
		got := myList.IsEmpty()
		assert.Equal(t, want, got)
	})

	t.Run("IsEmpty should be false if the list is not empty", func(t *testing.T) {
		want := false
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.IsEmpty()
		assert.Equal(t, want, got)
	})
}

func TestList_NonEmpty(t *testing.T) {
	t.Run("NonEmpty should be true if the list is not empty", func(t *testing.T) {
		want := true
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.NonEmpty()
		assert.Equal(t, want, got)
	})

	t.Run("NonEmpty should be false if the list is empty", func(t *testing.T) {
		want := false
		myList := collection.NewList[int]()
		got := myList.NonEmpty()
		assert.Equal(t, want, got)
	})
}

func TestList_Size(t *testing.T) {
	t.Run("Size should return the size of the list", func(t *testing.T) {
		want := 4
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Size()
		assert.Equal(t, want, got)
	})

	t.Run("Size should return 0 if the list is empty", func(t *testing.T) {
		want := 0
		myList := collection.NewList[int]()
		got := myList.Size()
		assert.Equal(t, want, got)
	})
}

func TestList_Reverse(t *testing.T) {
	t.Run("Reverse should return a reversed list", func(t *testing.T) {
		want := collection.List[int]{4, 3, 2, 1}
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Reverse()
		assert.Equal(t, want, got)
	})
}

func TestList_ToStream(t *testing.T) {
	t.Run("ToStream should return a stream of the list", func(t *testing.T) {
		want := []int{1, 2, 3, 4}
		myList := collection.NewList(1, 2, 3, 4)
		var got []int
		for v := range myList.ToStream() {
			got = append(got, v)
		}
		assert.Equal(t, want, got)
	})
}

func TestList_ToBufferedStream(t *testing.T) {
	t.Run("ToBufferedStream should return a buffered stream of the list when the buffer is smaller than the size of the list", func(t *testing.T) {
		want := []int{1, 2, 3, 4}
		myList := collection.NewList(1, 2, 3, 4)
		var got []int
		for v := range myList.ToBufferedStream(2) {
			got = append(got, v)
		}
		assert.Equal(t, want, got)
	})
	t.Run("ToBufferedStream should return a buffered stream of the list when the buffer is greater than the size of the list", func(t *testing.T) {
		want := []int{1, 2, 3, 4}
		myList := collection.NewList(1, 2, 3, 4)
		var got []int
		for v := range myList.ToBufferedStream(6) {
			got = append(got, v)
		}
		assert.Equal(t, want, got)
	})
}

func TestList_ForEach(t *testing.T) {
	t.Run("ForEach should iterate over the list and execute the given function for every value", func(t *testing.T) {
		want := collection.List[int]{2, 4, 6, 8}
		myList := collection.NewList(1, 2, 3, 4)
		var got collection.List[int]
		myList.ForEach(func(i int) {
			got = append(got, i*2)
		})
		assert.Equal(t, want, got)
	})

	t.Run("ForEach should be chainable", func(t *testing.T) {
		want := collection.List[int]{2, 4, 6, 8}
		myList := collection.NewList(1, 2, 3, 4)
		newList := collection.NewList[int]()
		got := myList.ForEach(func(i int) {
			newList = newList.Append(i * 2)
		}).FoldLeft(0, func(acc int, v int) int {
			return acc + v
		})

		assert.Equal(t, want, newList)
		assert.Equal(t, 10, got)
	})
}

func TestList_Sum(t *testing.T) {
	t.Run("Sum should return the sum of all values in the list", func(t *testing.T) {
		want := fp.Some(10)
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Sum(fp.Add[int])
		assert.Equal(t, want, got)
	})
}

func TestList_Product(t *testing.T) {
	t.Run("Product should return the product of all values in the list", func(t *testing.T) {
		want := fp.Some(24)
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Product(fp.Multiply[int])
		assert.Equal(t, want, got)
	})
}

func TestList_Min(t *testing.T) {
	t.Run("Min should return the smallest value in the list", func(t *testing.T) {
		want := fp.Some(1)
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Min(fp.LT[int])
		assert.Equal(t, want, got)
	})
}

func TestList_Max(t *testing.T) {
	t.Run("Max should return the smallest value in the list", func(t *testing.T) {
		want := fp.Some(4)
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Max(fp.GT[int])
		assert.Equal(t, want, got)
	})
}

func TestList_Contains(t *testing.T) {
	t.Run("Contains should return true if the list contains the given value", func(t *testing.T) {
		want := true
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Contains(3, fp.EQ[int])
		assert.Equal(t, want, got)
	})

	t.Run("Contains should return false if the list does not contain the given value", func(t *testing.T) {
		want := false
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Contains(5, fp.EQ[int])
		assert.Equal(t, want, got)
	})
}

func TestList_IndexOf(t *testing.T) {
	t.Run("IndexOf should return the index of the given value", func(t *testing.T) {
		want := 2
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.IndexOf(3, fp.EQ[int])
		assert.Equal(t, want, got)
	})

	t.Run("IndexOf should return none if the list does not contain the given value", func(t *testing.T) {
		want := -1
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.IndexOf(5, fp.EQ[int])
		assert.Equal(t, want, got)
	})
}

func TestList_LastIndexOf(t *testing.T) {
	t.Run("LastIndexOf should return the index of the given value", func(t *testing.T) {
		want := 4
		myList := collection.NewList(1, 2, 3, 4, 2)
		got := myList.LastIndexOf(2, fp.EQ[int])
		assert.Equal(t, want, got)
	})

	t.Run("LastIndexOf should return none if the list does not contain the given value", func(t *testing.T) {
		want := -1
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.LastIndexOf(5, fp.EQ[int])
		assert.Equal(t, want, got)
	})
}

func TestList_Count(t *testing.T) {
	t.Run("Count should return the number of times the given value occurs in the list", func(t *testing.T) {
		want := 2
		myList := collection.NewList(1, 2, 3, 4, 2)
		got := myList.Count(func(a int) bool { return a == 2 })
		assert.Equal(t, want, got)
	})

	t.Run("Count should return 0 if the list does not contain the given value", func(t *testing.T) {
		want := 0
		myList := collection.NewList(1, 2, 3, 4)
		got := myList.Count(func(a int) bool { return a == 5 })
		assert.Equal(t, want, got)
	})
}

func TestList_Traversable(t *testing.T) {
	t.Run("List should implement Traversable", func(t *testing.T) {
		myList := collection.NewList(1, 2, 3, 4)
		got := sum[int, int, collection.List[int]](0, myList)
		assert.Equal(t, 10, got)
	})
}

func sum[T fp.Addable, U any, V any](init T, l fp.Traversable[T, U, V]) T {
	return l.FoldLeft(init, fp.Add[T])
}

func TestList(t *testing.T) {
	t.Run("Calculate the sum of the first 10 natural numbers whose squares are value is divisible by 5", func(t *testing.T) {
		got := collection.NewList(ranger.Range(1, 100).Collect()...).
			Filter(func(a int) bool { return (a*a)%5 == 0 }, 10).
			Sum(fp.Add[int])
		assert.Equal(t, fp.Some(275), got)
	})
}

func TestList_Sort(t *testing.T) {
	t.Run("Sort should return a new sorted list", func(t *testing.T) {
		want := collection.NewList(1, 2, 3, 4)
		myList := collection.NewList(4, 3, 2, 1)
		got := myList.Sort(fp.LT[int])
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List[int]{4, 3, 2, 1}, myList)
	})
}

func TestList_Cap(t *testing.T) {
	t.Run("Cap should return the current capacity of the list", func(t *testing.T) {
		l := collection.NewList(1, 2, 3, 4)
		want := cap(l)
		got := l.Cap()
		assert.Equal(t, want, got)
	})
}

func TestList_Unique(t *testing.T) {
	t.Run("Unique returns an empty list if the list is empty", func(t *testing.T) {
		want := collection.NewList[int]()
		got := collection.NewList[int]().Unique()
		assert.Equal(t, want, got)
	})

	t.Run("Unique returns the unique elements in the list", func(t *testing.T) {
		want := collection.NewList(1, 2, 3, 4)
		got := collection.NewList(1, 2, 3, 4, 1, 2, 3, 4).Unique()
		assert.Equal(t, want, got)
	})
}

func TestList_Get(t *testing.T) {
	t.Run("Get should return Some(value) if the index given exists", func(t *testing.T) {
		want := fp.Some(2)
		got := collection.NewList(1, 2, 3, 4).Get(1)
		assert.Equal(t, want, got)
	})

	t.Run("Get should return None if the index given is less than 0", func(t *testing.T) {
		want := fp.None[int]()
		got := collection.NewList(1, 2, 3, 4).Get(-1)
		assert.Equal(t, want, got)
	})

	t.Run("Get should return None if the index given is out of bounds", func(t *testing.T) {
		want := fp.None[int]()
		got := collection.NewList(1, 2, 3, 4).Get(5)
		assert.Equal(t, want, got)
	})
}
