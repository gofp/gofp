package collection

import (
	fp "gitlab.com/gofp/gofp"
)

// ResultList is a generic collection of Results from operations that may or may not have failed.
type ResultList[T any] List[fp.Result[T]]

// NewResultList creates a new ResultList with type specified
func NewResultList[T any](v ...fp.Result[T]) ResultList[T] {
	return v
}

// Collect returns a list of all successful results from the ResultList
func (l ResultList[T]) Collect() List[T] {
	results := NewList[T]()
	for _, result := range l {
		if result.HasValue() {
			results = results.Append(result.Must())
		}
	}

	return results
}

// CollectErrors returns a list of all failed results from the ResultList
func (l ResultList[T]) CollectErrors() List[error] {
	errs := NewList[error]()
	for _, result := range l {
		if result.IsErr() {
			errs = errs.Append(result.Err())
		}
	}
	return errs
}
