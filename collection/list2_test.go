package collection_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/collection"
	"gitlab.com/gofp/gofp/ranger"
)

func TestNewList2(t *testing.T) {
	t.Run("NewList2[int, int][int, int]", func(t *testing.T) {
		want := collection.List2[int, int]{1, 2, 3}
		got := collection.NewList2[int, int](1, 2, 3)
		assert.Equal(t, want, got)
	})
	t.Run("NewList2[int, int][int, string]", func(t *testing.T) {
		want := collection.List2[int, string]{1, 2, 3}
		got := collection.NewList2[int, string](1, 2, 3)
		assert.Equal(t, want, got)
	})
	t.Run("NewList2[int, int][int, float64]", func(t *testing.T) {
		want := collection.List2[int, float64]{1, 2, 3}
		got := collection.NewList2[int, float64](1, 2, 3)
		assert.Equal(t, want, got)
	})
	t.Run("NewList2[int, int][string, fp.Result[int]]", func(t *testing.T) {
		want := collection.List2[string, fp.Result[int]]{"1", "2", "3"}
		got := collection.NewList2[string, fp.Result[int]]("1", "2", "3")
		assert.Equal(t, want, got)
	})
}

func TestList2_Push(t *testing.T) {
	t.Run("Push should add values to the head of the list", func(t *testing.T) {
		want := collection.List2[int, int]{1, 2, 3, 4}
		got := collection.NewList2[int, int](2, 3, 4).Push(1)
		assert.Equal(t, want, got)
	})

	t.Run("Push can be chained", func(t *testing.T) {
		want := collection.List2[int, int]{4, 3, 2, 1}
		got := collection.NewList2[int, int]().Push(1).Push(2).Push(3).Push(4)
		assert.Equal(t, want, got)
	})
}

func TestList2_Append(t *testing.T) {
	t.Run("Append should add values to the back of the list", func(t *testing.T) {
		want := collection.List2[int, int]{1, 2, 3, 4}
		got := collection.NewList2[int, int](1, 2, 3).Append(4)
		assert.Equal(t, want, got)
	})

	t.Run("Append can be chained", func(t *testing.T) {
		want := collection.List2[int, int]{1, 2, 3, 4}
		got := collection.NewList2[int, int]().Append(1).Append(2).Append(3).Append(4)
		assert.Equal(t, want, got)
	})
}

func TestList2_Pop(t *testing.T) {
	t.Run("Pop should remove values from the head of the list and return the value and the remaining list", func(t *testing.T) {
		ints := collection.NewList2[int, int](1, 2, 3, 4)
		got, ints := ints.Pop()
		assert.Equal(t, fp.Some(1), got)
		assert.Equal(t, collection.List2[int, int]{2, 3, 4}, ints)
		got, ints = ints.Pop()
		assert.Equal(t, fp.Some(2), got)
		assert.Equal(t, collection.List2[int, int]{3, 4}, ints)
		got, ints = ints.Pop()
		assert.Equal(t, fp.Some(3), got)
		assert.Equal(t, collection.List2[int, int]{4}, ints)
		got, ints = ints.Pop()
		assert.Equal(t, fp.Some(4), got)
		assert.Equal(t, collection.List2[int, int]{}, ints)
		got, ints = ints.Pop()
		assert.Equal(t, fp.None[int](), got)
		assert.Equal(t, collection.List2[int, int]{}, ints)
	})
}

func TestList2_PopN(t *testing.T) {
	t.Run("PopN should remove N elements from the list", func(t *testing.T) {
		ints := collection.NewList2[int, int](1, 2, 3, 4)
		got, ints := ints.PopN(2)
		assert.Equal(t, collection.List2[int, int]{1, 2}, got)
		assert.Equal(t, collection.List2[int, int]{3, 4}, ints)
	})

	t.Run("PopN should return an empty list and a copy of the original list if N is less than 1", func(t *testing.T) {
		ints := collection.NewList2[int, int](1, 2, 3, 4)
		got, remaining := ints.PopN(0)
		assert.Equal(t, collection.List2[int, int]{}, got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, remaining)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, ints)
	})

	t.Run("PopN should return a copy of the list, and an empty list as remaining if N is greater than the length of the list", func(t *testing.T) {
		ints := collection.NewList2[int, int](1, 2, 3, 4)
		got, remaining := ints.PopN(5)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, got)
		assert.Equal(t, collection.List2[int, int]{}, remaining)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, ints)
	})
}

func TestList2_Take(t *testing.T) {
	t.Run("Take should remove values from the end of the list and return the value and the remaining list", func(t *testing.T) {
		ints := collection.NewList2[int, int](1, 2, 3, 4)
		got, ints := ints.Take()
		assert.Equal(t, fp.Some(4), got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3}, ints)
		got, ints = ints.Take()
		assert.Equal(t, fp.Some(3), got)
		assert.Equal(t, collection.List2[int, int]{1, 2}, ints)
		got, ints = ints.Take()
		assert.Equal(t, fp.Some(2), got)
		assert.Equal(t, collection.List2[int, int]{1}, ints)
		got, ints = ints.Take()
		assert.Equal(t, fp.Some(1), got)
		assert.Equal(t, collection.List2[int, int]{}, ints)
		got, ints = ints.Take()
		assert.Equal(t, fp.None[int](), got)
		assert.Equal(t, collection.List2[int, int]{}, ints)
	})
}

func TestList2_TakeN(t *testing.T) {
	t.Run("TakeN returns a copy of the last N elements and remaining elements of the given list", func(t *testing.T) {
		ints := collection.NewList2[int, int](1, 2, 3, 4)
		got, remaining := ints.TakeN(2)
		assert.Equal(t, collection.List2[int, int]{3, 4}, got)
		assert.Equal(t, collection.List2[int, int]{1, 2}, remaining)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, ints)
	})

	t.Run("TakeN returns an empty list and a copy of the given list when N is less than 1", func(t *testing.T) {
		ints := collection.NewList2[int, int](1, 2, 3, 4)
		got, remaining := ints.TakeN(0)
		assert.Equal(t, collection.List2[int, int]{}, got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, remaining)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, ints)
	})

	t.Run("TakeN returns a copy of the given list and an empty list as remaining if N is greater than the length of the list", func(t *testing.T) {
		ints := collection.NewList2[int, int](1, 2, 3, 4)
		got, remaining := ints.TakeN(5)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, got)
		assert.Equal(t, collection.List2[int, int]{}, remaining)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, ints)
	})
}

func TestList2_Map(t *testing.T) {
	t.Run("Map should apply the function to each value in the list and return a new list with the results when both types are the same", func(t *testing.T) {
		want := collection.List2[int, int]{2, 4, 6, 8}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Map(func(i int) int {
			return i * 2
		})
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, myList)
	})

	t.Run("Map should apply the function to each value in the list and return a new list with the results when the types are different", func(t *testing.T) {
		want := collection.List2[string, string]{"2", "4", "6", "8"}
		myList := collection.NewList2[int, string](1, 2, 3, 4)
		got := myList.Map(func(i int) string {
			return fmt.Sprintf("%d", i*2)
		})
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, string]{1, 2, 3, 4}, myList)
	})

	t.Run("Map can be chained", func(t *testing.T) {
		want := collection.List2[float64, float64]{3, 5, 7, 9}
		myList := collection.NewList2[int, float64](1, 2, 3, 4)
		got := myList.Map(func(i int) float64 {
			return float64(i) * 2.0
		}).Map(func(i float64) float64 {
			return i + 1
		})
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, float64]{1, 2, 3, 4}, myList)
	})

	t.Run("MapT can map the data to a list of the same type", func(t *testing.T) {
		want := collection.List2[int, int]{2, 4, 6, 8}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.MapT(func(i int) int {
			return i * 2
		})
		assert.Equal(t, want, got)
	})

	t.Run("MapT can be chained", func(t *testing.T) {
		want := collection.List2[string, string]{"2", "4", "6", "8"}
		myList := collection.NewList2[int, string](1, 2, 3, 4)
		got := myList.MapT(func(i int) int {
			return i * 2
		}).Map(func(i int) string {
			return fmt.Sprintf("%d", i)
		})
		assert.Equal(t, want, got)
	})
}

func TestList2_Filter(t *testing.T) {
	t.Run("Filter should return a new list with only the values that pass the predicate", func(t *testing.T) {
		want := collection.List2[int, int]{2, 4}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Filter(func(i int) bool {
			return i%2 == 0
		}, 0)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, myList)
	})

	t.Run("Filter can be chained", func(t *testing.T) {
		want := collection.List2[int, int]{4}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Filter(func(i int) bool {
			return i%2 == 0
		}, 0).Filter(func(i int) bool {
			return i%4 == 0
		}, 0)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, myList)
	})
}

func TestList2_FilterNot(t *testing.T) {
	t.Run("FilterNot should return a new list with only the values that do not pass the predicate", func(t *testing.T) {
		want := collection.List2[int, int]{1, 3}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.FilterNot(func(i int) bool {
			return i%2 == 0
		}, 0)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, myList)
	})
}

func TestList2_FoldLeft(t *testing.T) {
	t.Run("FoldLeft should apply the function to each value in the list and return the result", func(t *testing.T) {
		want := 10
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.FoldLeft(0, func(acc, i int) int {
			return acc + i
		})
		assert.Equal(t, want, got)
	})

	t.Run("FoldLeft should apply the function to each value in the list left to right", func(t *testing.T) {
		want := "abcd"
		myList := collection.NewList2[string, string]("a", "b", "c", "d")
		got := myList.FoldLeft("", func(acc, i string) string {
			return acc + i
		})
		assert.Equal(t, want, got)
	})
}

func TestList2_FoldRight(t *testing.T) {
	t.Run("FoldRight should apply the function to each value in the list and return the result", func(t *testing.T) {
		want := 10
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.FoldRight(0, func(acc, i int) int {
			return acc + i
		})
		assert.Equal(t, want, got)
	})

	t.Run("FoldRight should apply the function to each value in the list left to right", func(t *testing.T) {
		want := "dcba"
		myList := collection.NewList2[string, string]("a", "b", "c", "d")
		got := myList.FoldRight("", func(acc, i string) string {
			return acc + i
		})
		assert.Equal(t, want, got)
	})
}

func TestList2_Head(t *testing.T) {
	t.Run("Head should return the first value in the list without altering the list", func(t *testing.T) {
		want := fp.Some(1)
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Head()
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, myList)
	})

	t.Run("Head should return a None value if the list is empty", func(t *testing.T) {
		want := fp.None[int]()
		myList := collection.NewList2[int, int]()
		got := myList.Head()
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{}, myList)
	})
}

func TestList2_Tail(t *testing.T) {
	t.Run("Tail should return the list without the first value and without altering the list", func(t *testing.T) {
		want := collection.List2[int, int]{2, 3, 4}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Tail()
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4}, myList)
	})

	t.Run("Tail should return nil if the list is empty", func(t *testing.T) {
		want := collection.List2[int, int]{}
		myList := collection.NewList2[int, int]()
		got := myList.Tail()
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{}, myList)
	})
}

func TestList2_Last(t *testing.T) {
	t.Run("Last should return the last value in the list without altering the list", func(t *testing.T) {
		want := fp.Some(4)
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Last()
		assert.Equal(t, want, got)
	})

	t.Run("Last should return none if the list is empty", func(t *testing.T) {
		want := fp.None[int]()
		myList := collection.NewList2[int, int]()
		got := myList.Last()
		assert.Equal(t, want, got)
	})
}

func TestList2_ToString(t *testing.T) {
	t.Run("ToString should return a string representation of the list", func(t *testing.T) {
		want := []string{"1", "2", "3", "4"}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.ToStrings()
		assert.Equal(t, want, got)
	})
}

func TestList2_SplitAt(t *testing.T) {
	t.Run("SplitAt should return 2 lists split at the desired index", func(t *testing.T) {
		wantLeft, wantRight := collection.List2[int, int]{1, 2}, collection.List2[int, int]{3, 4}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		gotLeft, gotRight := myList.SplitAt(2)
		assert.Equal(t, wantLeft, gotLeft)
		assert.Equal(t, wantRight, gotRight)
	})

	t.Run("SplitAt should return 2 lists when the desired index is out of bound", func(t *testing.T) {
		wantLeft, wantRight := collection.List2[int, int]{1, 2, 3, 4}, collection.List2[int, int]{}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		gotLeft, gotRight := myList.SplitAt(5)
		assert.Equal(t, wantLeft, gotLeft)
		assert.Equal(t, wantRight, gotRight)
	})

	t.Run("SplitAt should return 2 lists when the desired index is 0", func(t *testing.T) {
		wantLeft, wantRight := collection.List2[int, int]{}, collection.List2[int, int]{1, 2, 3, 4}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		gotLeft, gotRight := myList.SplitAt(0)
		assert.Equal(t, wantLeft, gotLeft)
		assert.Equal(t, wantRight, gotRight)
	})
}

func TestList2_Partition(t *testing.T) {
	t.Run("Partition should return 2 lists split at the desired index", func(t *testing.T) {
		wantLeft, wantRight := collection.List2[int, int]{2, 4}, collection.List2[int, int]{1, 3}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		gotLeft, gotRight := myList.Partition(func(i int) bool {
			return i%2 == 0
		})
		assert.Equal(t, wantLeft, gotLeft)
		assert.Equal(t, wantRight, gotRight)
	})
}

func TestList2_Slice(t *testing.T) {
	t.Run("Slice should return a slice using the given indexes", func(t *testing.T) {
		want := collection.List2[int, int]{2, 3}
		myList := collection.NewList2[int, int](1, 2, 3, 4, 5)
		got := myList.Slice(1, 3)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4, 5}, myList)
	})

	t.Run("Slice should return a slice given negative indexes", func(t *testing.T) {
		want := collection.List2[int, int]{4, 5}
		myList := collection.NewList2[int, int](1, 2, 3, 4, 5)
		got := myList.Slice(-2, -1)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4, 5}, myList)
	})

	t.Run("Slice should return an empty slice given indexes out of bound", func(t *testing.T) {
		want := collection.List2[int, int]{}
		myList := collection.NewList2[int, int](1, 2, 3, 4, 5)
		got := myList.Slice(5, 10)
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{1, 2, 3, 4, 5}, myList)
	})
}

func TestList2_IsEmpty(t *testing.T) {
	t.Run("IsEmpty should be true if the list is empty", func(t *testing.T) {
		want := true
		myList := collection.NewList2[int, int]()
		got := myList.IsEmpty()
		assert.Equal(t, want, got)
	})

	t.Run("IsEmpty should be false if the list is not empty", func(t *testing.T) {
		want := false
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.IsEmpty()
		assert.Equal(t, want, got)
	})
}

func TestList2_NonEmpty(t *testing.T) {
	t.Run("NonEmpty should be true if the list is not empty", func(t *testing.T) {
		want := true
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.NonEmpty()
		assert.Equal(t, want, got)
	})

	t.Run("NonEmpty should be false if the list is empty", func(t *testing.T) {
		want := false
		myList := collection.NewList2[int, int]()
		got := myList.NonEmpty()
		assert.Equal(t, want, got)
	})
}

func TestList2_Size(t *testing.T) {
	t.Run("Size should return the size of the list", func(t *testing.T) {
		want := 4
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Size()
		assert.Equal(t, want, got)
	})

	t.Run("Size should return 0 if the list is empty", func(t *testing.T) {
		want := 0
		myList := collection.NewList2[int, int]()
		got := myList.Size()
		assert.Equal(t, want, got)
	})
}

func TestList2_Reverse(t *testing.T) {
	t.Run("Reverse should return a reversed list", func(t *testing.T) {
		want := collection.List2[int, int]{4, 3, 2, 1}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Reverse()
		assert.Equal(t, want, got)
	})
}

func TestList2_ToStream(t *testing.T) {
	t.Run("ToStream should return a stream of the list", func(t *testing.T) {
		want := []int{1, 2, 3, 4}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		var got []int
		for v := range myList.ToStream() {
			got = append(got, v)
		}
		assert.Equal(t, want, got)
	})
}

func TestList2_ToBufferedStream(t *testing.T) {
	t.Run("ToBufferedStream should return a buffered stream of the list when the buffer is smaller than the size of the list", func(t *testing.T) {
		want := []int{1, 2, 3, 4}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		var got []int
		for v := range myList.ToBufferedStream(2) {
			got = append(got, v)
		}
		assert.Equal(t, want, got)
	})
	t.Run("ToBufferedStream should return a buffered stream of the list when the buffer is greater than the size of the list", func(t *testing.T) {
		want := []int{1, 2, 3, 4}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		var got []int
		for v := range myList.ToBufferedStream(6) {
			got = append(got, v)
		}
		assert.Equal(t, want, got)
	})
}

func TestList2_ForEach(t *testing.T) {
	t.Run("ForEach should iterate over the list and execute the given function for every value", func(t *testing.T) {
		want := collection.List2[int, int]{2, 4, 6, 8}
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		var got collection.List2[int, int]
		myList.ForEach(func(i int) {
			got = append(got, i*2)
		})
		assert.Equal(t, want, got)
	})
}

func TestList2_Sum(t *testing.T) {
	t.Run("Sum should return the sum of all values in the list", func(t *testing.T) {
		want := fp.Some(10)
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Sum(fp.Add[int])
		assert.Equal(t, want, got)
	})
}

func TestList2_Product(t *testing.T) {
	t.Run("Product should return the product of all values in the list", func(t *testing.T) {
		want := fp.Some(24)
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Product(fp.Multiply[int])
		assert.Equal(t, want, got)
	})
}

func TestList2_Min(t *testing.T) {
	t.Run("Min should return the smallest value in the list", func(t *testing.T) {
		want := fp.Some(1)
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Min(fp.LT[int])
		assert.Equal(t, want, got)
	})
}

func TestList2_Max(t *testing.T) {
	t.Run("Max should return the smallest value in the list", func(t *testing.T) {
		want := fp.Some(4)
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Max(fp.GT[int])
		assert.Equal(t, want, got)
	})
}

func TestList2_Contains(t *testing.T) {
	t.Run("Contains should return true if the list contains the given value", func(t *testing.T) {
		want := true
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Contains(3, fp.EQ[int])
		assert.Equal(t, want, got)
	})

	t.Run("Contains should return false if the list does not contain the given value", func(t *testing.T) {
		want := false
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Contains(5, fp.EQ[int])
		assert.Equal(t, want, got)
	})
}

func TestList2_IndexOf(t *testing.T) {
	t.Run("IndexOf should return the index of the given value", func(t *testing.T) {
		want := 2
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.IndexOf(3, fp.EQ[int])
		assert.Equal(t, want, got)
	})

	t.Run("IndexOf should return none if the list does not contain the given value", func(t *testing.T) {
		want := -1
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.IndexOf(5, fp.EQ[int])
		assert.Equal(t, want, got)
	})
}

func TestList2_LastIndexOf(t *testing.T) {
	t.Run("LastIndexOf should return the index of the given value", func(t *testing.T) {
		want := 4
		myList := collection.NewList2[int, int](1, 2, 3, 4, 2)
		got := myList.LastIndexOf(2, fp.EQ[int])
		assert.Equal(t, want, got)
	})

	t.Run("LastIndexOf should return none if the list does not contain the given value", func(t *testing.T) {
		want := -1
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.LastIndexOf(5, fp.EQ[int])
		assert.Equal(t, want, got)
	})
}

func TestList2_Count(t *testing.T) {
	t.Run("Count should return the number of times the given value occurs in the list", func(t *testing.T) {
		want := 2
		myList := collection.NewList2[int, int](1, 2, 3, 4, 2)
		got := myList.Count(func(a int) bool { return a == 2 })
		assert.Equal(t, want, got)
	})

	t.Run("Count should return 0 if the list does not contain the given value", func(t *testing.T) {
		want := 0
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := myList.Count(func(a int) bool { return a == 5 })
		assert.Equal(t, want, got)
	})
}

func TestList2_Traversable(t *testing.T) {
	t.Run("List should implement Traversable", func(t *testing.T) {
		myList := collection.NewList2[int, int](1, 2, 3, 4)
		got := sum[int, int, collection.List2[int, int]](0, myList)
		assert.Equal(t, 10, got)
	})
}

func TestList2(t *testing.T) {
	t.Run("Calculate the sum of the first 10 natural numbers whose squares are value is divisible by 5", func(t *testing.T) {
		got := collection.NewList2[int, int](ranger.Range(1, 100).Collect()...).
			Filter(func(a int) bool { return (a*a)%5 == 0 }, 10).
			Sum(fp.Add[int]).
			Must()
		assert.Equal(t, 275, got)
	})
}

func TestList2_Sort(t *testing.T) {
	t.Run("Sort should return a new sorted list", func(t *testing.T) {
		want := collection.NewList2[int, int](1, 2, 3, 4)
		myList := collection.NewList2[int, int](4, 3, 2, 1)
		got := myList.Sort(fp.LT[int])
		assert.Equal(t, want, got)
		assert.Equal(t, collection.List2[int, int]{4, 3, 2, 1}, myList)
	})
}

func TestList2_Cap(t *testing.T) {
	t.Run("Cap should return the current capacity of the list", func(t *testing.T) {
		l := collection.NewList2[int, int](1, 2, 3, 4)
		want := cap(l)
		got := l.Cap()
		assert.Equal(t, want, got)
	})
}

func TestList2_Unique(t *testing.T) {
	t.Run("Unique returns an empty list if the list is empty", func(t *testing.T) {
		want := collection.NewList2[int, int]()
		got := collection.NewList2[int, int]().Unique()
		assert.Equal(t, want, got)
	})

	t.Run("Unique returns the unique elements in the list", func(t *testing.T) {
		want := collection.NewList2[int, int](1, 2, 3, 4)
		got := collection.NewList2[int, int](1, 2, 3, 4, 1, 2, 3, 4).Unique()
		assert.Equal(t, want, got)
	})
}

func TestList2_Get(t *testing.T) {
	t.Run("Get should return Some(value) if the index given exists", func(t *testing.T) {
		want := fp.Some(2)
		got := collection.NewList2[int, int](1, 2, 3, 4).Get(1)
		assert.Equal(t, want, got)
	})

	t.Run("Get should return None if the index given is less than 0", func(t *testing.T) {
		want := fp.None[int]()
		got := collection.NewList2[int, int](1, 2, 3, 4).Get(-1)
		assert.Equal(t, want, got)
	})

	t.Run("Get should return None if the index given is out of bounds", func(t *testing.T) {
		want := fp.None[int]()
		got := collection.NewList2[int, int](1, 2, 3, 4).Get(5)
		assert.Equal(t, want, got)
	})
}

func TestList2_ToList(t *testing.T) {
	t.Run("ToList should return a new list with the same elements", func(t *testing.T) {
		want := collection.NewList(1, 2, 3, 4)
		got := collection.NewList2[int, int](1, 2, 3, 4).ToList()
		assert.Equal(t, want, got)
	})
}
