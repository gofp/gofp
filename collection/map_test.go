package collection_test

import (
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	fp "gitlab.com/gofp/gofp"
	"gitlab.com/gofp/gofp/collection"
)

func TestNewMap(t *testing.T) {
	type args struct {
		kvs []collection.KV[int, string]
	}
	tests := []struct {
		name string
		args args
		want collection.Map[int, string]
	}{
		{
			"New with no values",
			args{},
			collection.Map[int, string]{},
		},
		{
			"New with values",
			args{
				kvs: []collection.KV[int, string]{
					{1, "one"},
					{2, "two"},
					{3, "three"},
				},
			},
			collection.Map[int, string]{
				1: "one",
				2: "two",
				3: "three",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := collection.NewMap(tt.args.kvs...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMap_Add(t *testing.T) {
	t.Run("Add inserts an item into the map if it doesn't exist", func(t *testing.T) {
		m := collection.NewMap[int, string]()
		m.Add(1, "one")
		want := collection.Map[int, string]{1: "one"}
		assert.Equal(t, want, m)
	})
	t.Run("Add updates the item if the key already exists", func(t *testing.T) {
		m := collection.NewMap[int, string]()
		m.Add(1, "one")
		m.Add(1, "two")
		want := collection.Map[int, string]{1: "two"}
		assert.Equal(t, want, m)
	})
}

func TestMap_Del(t *testing.T) {
	t.Run("Del removes an item from the map if it exists", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)
		m.Rem(1)
		want := collection.Map[int, string]{2: "two", 3: "three"}
		assert.Equal(t, want, m)
	})
	t.Run("Del does nothing if the key does not exist in the map", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)
		m.Rem(4)
		want := collection.Map[int, string]{1: "one", 2: "two", 3: "three"}
		assert.Equal(t, want, m)
	})
}

func TestMap_Contains(t *testing.T) {
	t.Run("Contains returns true if the key exists", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.Contains(1)
		assert.Equal(t, true, got)
	})

	t.Run("Contains returns false if the key does not exist", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.Contains(4)
		assert.Equal(t, false, got)
	})
}

func TestMap_Get(t *testing.T) {
	t.Run("Get returns the value and true if the key exists", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		gotString, gotBool := m.Get(1)
		assert.Equal(t, "one", gotString)
		assert.Equal(t, true, gotBool)
	})

	t.Run("Get returns the value type's zero value and false if the key doesn't exist", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		gotString, gotBool := m.Get(4)
		assert.Equal(t, "", gotString)
		assert.Equal(t, false, gotBool)

	})
}

func TestMap_GetOrElse(t *testing.T) {
	t.Run("GetOrElse returns the value if the key exists", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.GetOrElse(1, "default")
		assert.Equal(t, "one", got)
	})
	t.Run("GetOrElse returns the other value if the key doesn't exist", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)
		got := m.GetOrElse(4, "default")
		assert.Equal(t, "default", got)
	})
}

func TestMap_Exists(t *testing.T) {
	t.Run("Exists returns true if the predicate returns true", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.Exists(func(k int, v string) bool {
			return k == 1
		})
		assert.True(t, got)
	})
	t.Run("Exists returns false if the predicate returns false", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.Exists(func(k int, v string) bool {
			return k == 4
		})
		assert.False(t, got)
	})
}

func TestMap_Count(t *testing.T) {
	t.Run("Count returns the number of items that satisfies the predicate", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.Count(func(_ int, v string) bool {
			return strings.Contains(v, "o")
		})
		assert.Equal(t, 2, got)
	})
}

func TestMap_Filter(t *testing.T) {
	t.Run("Filter returns a new map with the items that satisfy the predicate", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.Filter(func(k int, v string) bool {
			return strings.Contains(v, "o")
		})
		want := collection.Map[int, string]{1: "one", 2: "two"}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
}

func TestMap_FilterKeys(t *testing.T) {
	t.Run("FilterKeys returns a new map with the items where the key satisfies the predicate", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.FilterKeys(func(k int) bool {
			return k == 1
		})

		want := collection.Map[int, string]{1: "one"}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
}

func TestMap_FilterNot(t *testing.T) {
	t.Run("FilterNot returns a new map with the items that do not satisfy the predicate", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.FilterNot(func(k int, v string) bool {
			return strings.Contains(v, "o")
		})
		want := collection.Map[int, string]{3: "three"}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})

}

func TestMap_Map(t *testing.T) {
	t.Run("Map maps the key and value of each item using the given mapping function", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.Map(func(k int, v string) (int, string) {
			return k * 2, v + "!"
		})

		want := collection.Map[int, string]{2: "one!", 4: "two!", 6: "three!"}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
}

func TestMap_MapKeys(t *testing.T) {
	t.Run("MapKeys maps every key in the map using the given mapping function", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.MapKeys(func(k int) int {
			return k * 2
		})

		want := collection.Map[int, string]{2: "one", 4: "two", 6: "three"}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
}

func TestMap_MapValues(t *testing.T) {
	t.Run("MapValues maps every value in the map using the given mapping function without changing the key", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.MapValues(func(v string) string {
			return v + "!"
		})

		want := collection.Map[int, string]{1: "one!", 2: "two!", 3: "three!"}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
}

func TestMap_Fold(t *testing.T) {
	t.Run("Fold applies the given function to an initial value and every value in the map using the given ordering", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.Fold("", func(acc string, k int, v string) string {
			return acc + v
		}, fp.LT[int])

		want := "onetwothree"
		assert.Equal(t, want, got)

		got = m.Fold("", func(acc string, k int, v string) string {
			return acc + v
		}, fp.GT[int])

		want = "threetwoone"
		assert.Equal(t, want, got)
	})
}

func TestMap_ForEach(t *testing.T) {
	t.Run("ForEach applies the given function to every value in the map", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := make([]string, 0, len(m))
		m.ForEach(func(k int, v string) {
			v += "!"
			got = append(got, v)
		}, fp.LT[int])

		assert.ElementsMatch(t, []string{"one!", "two!", "three!"}, got)
	})
}

func TestMap_Partition(t *testing.T) {
	t.Run("Partition returns 2 maps with elements that match the predicate and elements that don't", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		gotMatched, gotUnmatched := m.Partition(func(k int, v string) bool {
			return strings.Contains(v, "o")
		})

		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two"}, gotMatched)
		assert.Equal(t, collection.Map[int, string]{3: "three"}, gotUnmatched)
	})
}

func TestMap_ToStream(t *testing.T) {
	t.Run("ToStream converts the map to a stream of KV values", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := make([]collection.KV[int, string], 0, len(m))
		kvCh := m.ToStream(fp.LT[int])
		for v := range kvCh {
			got = append(got, v)
		}
		want := []collection.KV[int, string]{
			{1, "one"},
			{2, "two"},
			{3, "three"},
		}
		assert.ElementsMatch(t, want, got)
	})
}

func TestMap_ToBufferedStream(t *testing.T) {
	t.Run("ToBufferedStream converts the map to a stream of KV values, buffer less than size", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := make([]collection.KV[int, string], 0, len(m))
		kvCh := m.ToBufferedStream(2, fp.LT[int])
		for v := range kvCh {
			got = append(got, v)
		}
		want := []collection.KV[int, string]{
			{1, "one"},
			{2, "two"},
			{3, "three"},
		}
		assert.ElementsMatch(t, want, got)
	})
	t.Run("ToBufferedStream converts the map to a stream of KV values, buffer greater than size", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := make([]collection.KV[int, string], 0, len(m))
		kvCh := m.ToBufferedStream(4, fp.LT[int])
		for v := range kvCh {
			got = append(got, v)
		}
		want := []collection.KV[int, string]{
			{1, "one"},
			{2, "two"},
			{3, "three"},
		}
		assert.ElementsMatch(t, want, got)
	})
}

func TestMap_ToSlice(t *testing.T) {
	t.Run("ToSlice converts the Map to a slice of KV items", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToSlice()
		want := []collection.KV[int, string]{
			{1, "one"},
			{2, "two"},
			{3, "three"},
		}
		assert.ElementsMatch(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})

	t.Run("ToSlice orders the results using the given order function", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToSlice(func(a, b collection.KV[int, string]) bool {
			return a.K < b.K
		})

		want := []collection.KV[int, string]{
			{1, "one"},
			{2, "two"},
			{3, "three"},
		}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
}

func TestMap_ToSliceK(t *testing.T) {
	t.Run("ToSliceK converts the Map to a slice of K items", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToSliceK()
		want := []int{1, 2, 3}
		assert.ElementsMatch(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})

	t.Run("ToSliceK orders the slice of K items using the given order function", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToSliceK(func(a, b int) bool {
			return a < b
		})
		want := []int{1, 2, 3}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
}

func TestMap_ToSliceV(t *testing.T) {
	t.Run("ToSliceV converts the Map to a slice of V items", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToSliceV()
		want := []string{"one", "two", "three"}
		assert.ElementsMatch(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})

	t.Run("ToSliceV orders the results using the given order function", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToSliceV(func(a, b string) bool {
			return a < b
		})
		want := []string{"one", "three", "two"}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
}

func TestMap_ToList(t *testing.T) {
	t.Run("ToList converts the Map to a List of KV items", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToList()
		want := collection.List[collection.KV[int, string]]{
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		}
		assert.ElementsMatch(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})

	t.Run("ToList converts the Map to a List of KV items", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToList(func(a, b collection.KV[int, string]) bool {
			return a.K < b.K
		})
		want := collection.List[collection.KV[int, string]]{
			{1, "one"},
			{2, "two"},
			{3, "three"},
		}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
}

func TestMap_ToListK(t *testing.T) {
	t.Run("ToListK converts the Map to a slice of K items", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToListK()
		want := collection.List[int]{1, 2, 3}
		assert.ElementsMatch(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
	t.Run("ToListK orders the results using the given order function", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToListK(func(a, b int) bool {
			return a < b
		})
		want := collection.List[int]{1, 2, 3}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
}

func TestMap_ToListV(t *testing.T) {
	t.Run("ToListV converts the Map to a slice of V items", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToListV()
		want := collection.List[string]{"one", "two", "three"}
		assert.ElementsMatch(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
	t.Run("ToListV orders the results with the given order function", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.ToListV(func(a, b string) bool {
			return a < b
		})
		want := collection.List[string]{"one", "three", "two"}
		assert.Equal(t, want, got)
		assert.Equal(t, collection.Map[int, string]{1: "one", 2: "two", 3: "three"}, m)
	})
}

func TestMap_String(t *testing.T) {
	t.Run("String converts the contents of a map to a string", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		want := "Map{1: one, 2: two, 3: three}"
		got := m.String(fp.LT[int])
		assert.Equal(t, want, got)
	})

	t.Run("String converts an empty map", func(t *testing.T) {
		m := collection.NewMap[int, string]()

		want := "Map{}"
		got := m.String(fp.LT[int])
		assert.Equal(t, want, got)
	})
}

func TestMap_Keys(t *testing.T) {
	t.Run("Keys returns a slice of the keys in the map", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.Keys()
		want := []int{1, 2, 3}
		assert.ElementsMatch(t, want, got)
	})
}

func TestMap_Values(t *testing.T) {
	t.Run("Values returns a slice of the values in the map", func(t *testing.T) {
		m := collection.NewMap(
			collection.KV[int, string]{1, "one"},
			collection.KV[int, string]{2, "two"},
			collection.KV[int, string]{3, "three"},
		)

		got := m.Values()
		want := []string{"one", "two", "three"}
		assert.ElementsMatch(t, want, got)
	})
}
